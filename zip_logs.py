#!/usr/bin/python

import fnmatch
import os
import datetime
import time
import zipfile

today_str = time.strftime('%Y%m%d')
zipname = 'Logs_' + today_str +'.zip'
zipf = zipfile.ZipFile(zipname,'w',zipfile.ZIP_DEFLATED)

for root, dirnames, filenames in os.walk('.'):
    for filename in fnmatch.filter(filenames, '*.log'):

        if not filename.startswith(today_str):
            fn = os.path.join(root,filename)
            zipf.write(fn)
            os.remove(fn)
            print ('Zipping ', fn)

zipf.close()
        

