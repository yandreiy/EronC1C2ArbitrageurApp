EXTERNALS_PATH = ../../../externals

include( build.pri )
include( dependencies.pri )

SOURCES += src/*.cpp
HEADERS += include/*.h
FORMS += ui/*.ui
