#include "ActiveOrders.h"

ActiveOrders::ActiveOrders(IBClient *ib, QObject *parent) : QObject(parent),
    m_IbClient(ib)
{
    bool ok = connect(m_IbClient, SIGNAL(orderStatusSig(OrderId,IBString,int,int,double,int,int,double,int,IBString)),
                      SLOT(orderStatus(OrderId,IBString,int,int,double,int,int,double,int,IBString)) ); Q_ASSERT(ok);

    ok = connect(m_IbClient, SIGNAL(openOrderSig(OrderId,Contract,const ::Order&,OrderState)),
                 SLOT(openOrder(OrderId,Contract,const ::Order&,OrderState)) ); Q_ASSERT(ok);
}

const QMap<OrderId, OpenOrderData>& ActiveOrders::getOpenOrders() const
{
    return m_OpenOrders;
}

void ActiveOrders::orderStatus(OrderId orderId, const IBString &status, int filled,
                    int remaining, double avgFillPrice, int /*permId*/, int /*parentId*/,
                    double /*lastFillPrice*/, int /*clientId*/, const IBString& /*whyHeld*/)
{
    if(status == "Submitted") {
        OpenOrderData& orderData = m_OpenOrders[orderId];
        orderData.filled = filled;
        orderData.avgFillPrice = avgFillPrice;
        orderData.remaining = remaining;
    }
    else {
        m_OpenOrders.remove(orderId);
    }

    emit openOrdersChanged();
}

void ActiveOrders::openOrder(OrderId orderId, const Contract& contract, const ::Order& order, const OrderState&)
{
    OpenOrderData& orderData = m_OpenOrders[orderId];
    orderData.order = order;
    orderData.contract = contract;
}

ActiveOrders::~ActiveOrders()
{

}
