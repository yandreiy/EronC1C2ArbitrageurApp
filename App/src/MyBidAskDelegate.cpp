#include "MyBidAskDelegate.h"
#include <QDoubleSpinBox>

MyBidAskDelegate::MyBidAskDelegate(QObject *parent)
    :QStyledItemDelegate(parent)
{

}

QWidget *MyBidAskDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QDoubleSpinBox* editor = new QDoubleSpinBox(parent);
    editor->setRange(-10000000.0,10000000.0);
    editor->setSingleStep(0.01);
    editor->setDecimals(2);

    return editor;
}

void MyBidAskDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    double value = index.model()->data(index, Qt::EditRole).toDouble();
    QDoubleSpinBox* spinbox = static_cast<QDoubleSpinBox*>(editor);
    spinbox->setValue(value);
}

void MyBidAskDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QDoubleSpinBox* spinbox = static_cast<QDoubleSpinBox*>(editor);
    spinbox->interpretText();
    double value = spinbox->value();
    model->setData(index,value,Qt::EditRole);
}

void MyBidAskDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}

