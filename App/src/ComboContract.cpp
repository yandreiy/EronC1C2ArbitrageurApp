#include "ComboContract.h"
#include "IBClient.h"
#include "Contract.h"
#include <QDebug>
#include "Order.h"
#include <QThread>

ComboContract::ComboContract(const Contract &contract1, const Contract &contract2, QString exchange, QString currency, IBClient *ib, QObject *parent)
    :QObject(parent)
    ,m_ib(ib)
    ,m_contract1(new Contract(contract1))
    ,m_contract2(new Contract(contract2))
    ,m_exchange(exchange)
    ,m_currency(currency)
{
    connect(m_ib, &IBClient::contractDetailsSig, this, &ComboContract::onContractDetails);
}

void ComboContract::start()
{
      m_tickerId1  = m_ib->reqContractDetails(*m_contract1);
      m_tickerId2  = m_ib->reqContractDetails(*m_contract2);

      qDebug()<<"Request contract details: " << m_tickerId1;
      qDebug()<<"Request contract details: " << m_tickerId2;
}

void ComboContract::onContractDetails(int reqId, const ContractDetails &contractDetails)
{
    qDebug() <<"onContractDetails: " << reqId;

    if (reqId == m_tickerId1 && !m_received1) {
        m_leg1 = new ComboLeg();
        m_leg1->conId = contractDetails.summary.conId;
        m_leg1->action = "SELL";
        m_leg1->exchange = m_contract1->exchange;
        m_leg1->ratio = 1;
        m_contract1->localSymbol = contractDetails.summary.localSymbol;

        m_received1 = true;
        m_tickSize = contractDetails.minTick;
    }
    else
    if (reqId == m_tickerId2 && !m_received2) {
        m_leg2 = new ComboLeg();
        m_leg2->conId = contractDetails.summary.conId;
        m_leg2->action = "BUY";
        m_leg2->exchange = m_contract1->exchange;
        m_leg2->ratio = 1;

        m_contract2->localSymbol = contractDetails.summary.localSymbol;

        m_received2 = true;
    }

    // both received
    if (m_received1 && m_received2) {
        m_received1 = m_received2 = false;
        m_comboContract = new Contract();
        m_comboContract->symbol = "USD";     // For combo order use “USD” as the symbol value all the time
        m_comboContract->secType = "BAG";   // BAG is the security type for COMBO order
        m_comboContract->exchange = m_exchange;
        m_comboContract->currency = m_currency;
        m_comboContract->comboLegs = new Contract::ComboLegList();
        m_comboContract->comboLegs->append(m_leg1);
        m_comboContract->comboLegs->append(m_leg2);
        m_comboContract->localSymbol = QString("%1-%2").arg(m_contract1->localSymbol).arg(m_contract2->localSymbol);

//        Order order;
//        order.orderType = "MKT";
//        order.totalQuantity = 1;
//        order.action = "BUY";
//        TickerId id = m_ib->placeOrder(*m_comboContract, order);
        //qDebug() <<"Placed order : " << id;
        qDebug()<<"Legs: " << m_leg1->conId <<", " << m_leg2->conId;


//        /*TickerId */id = m_ib->reqMktData(*m_comboContract, "100,101,104,105,106,107,165,221,225,233,236,258,293,294,295,318", false);
//        qDebug() <<"Requested market data id: " << id;

        emit finished(*m_comboContract);
    }
}
