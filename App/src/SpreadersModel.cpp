#include "SpreadersModel.h"
#include <QStandardItemModel>

SpreadersModel::SpreadersModel()
{

}

int SpreadersModel::rowCount(const QModelIndex &parent) const
{
    return m_rows.count();
}

int SpreadersModel::columnCount(const QModelIndex &parent) const
{
    return 5;
}

QVariant SpreadersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= m_rows.size()) {
        return QVariant();
    }
    Row row = m_rows.at(index.row());
    double value;

    switch(role){
    case Qt::EditRole:
    case Qt::DisplayRole:

        switch(index.column()) {
        case 0:
            return row.spreaderName;
        case 1:
            return QString("%1").arg(row.currentSpreadBid,0,'f',2);
        case 2:
            return QString("%1").arg(row.currentSpreadAsk,0,'f',2);
        case 3:
            return QString("%1").arg(row.minSpreadBid,0,'f',2);
        case 4:
            return QString("%1").arg(row.minSpreadAsk,0,'f',2);
        }
        break;

    case Qt::CheckStateRole:
        if(index.column() == 0) {
            if (row.isChecked) {
                return QVariant( (int)Qt::Checked);
            }
            return QVariant( (int)Qt::Unchecked);
        }
        break;

    case Qt::BackgroundColorRole:
        if (index.column() == 0) {
            return row.spreaderColor;
        }
        break;

    case Qt::TextAlignmentRole:
//        if (index.column() > 0) {
            return QVariant(Qt::AlignCenter);
//        }
        break;
    }

    return QVariant();
}

QVariant SpreadersModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal) {
        return QVariant();
    }
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    switch(section){
    case 0: return "Spreader";
    case 1: return "Market Bid";
    case 2: return "Market Ask";
    case 3: return "My Bid";
    case 4: return "My Ask";
    }

    return QVariant();
}

Qt::ItemFlags SpreadersModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags result = QAbstractTableModel::flags(index);
    if (index.column() == 0) {
        result |= Qt::ItemIsUserCheckable;
        result |= Qt::ItemIsEnabled;
    }

    if (index.column() == 3 || index.column() == 4) {
        result |= Qt::ItemIsEditable;
    }

    return result;
}

bool SpreadersModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.row() >= m_rows.size()) {
        return false;
    }

    Row& row = m_rows[index.row()];

    if (role == Qt::CheckStateRole) {
        if (index.column() == 0) {
            row.isChecked = (Qt::CheckState)value.toInt() == Qt::Checked;
        }
    }

    if (role == Qt::DisplayRole) {

        switch(index.column()) {
        case 0:
            row.spreaderName = value.toString();
            break;
        case 1:
            row.currentSpreadBid = value.toDouble();
            break;
        case 2:
            row.currentSpreadAsk = value.toDouble();
            break;
        case 3:
            if (!value.toString().isEmpty()) {
                row.minSpreadBid = value.toDouble();
            }
            break;
        case 4:
            row.minSpreadAsk = value.toDouble();
            break;
        }
    }
    if (role == Qt::EditRole) {
        bool ok = false;
        double val;
        double oldValue;
        QModelIndex name_index;
        QString name;

        switch(index.column()) {
        case 3:
            val = value.toDouble(&ok);
            if (ok) {
                oldValue = row.minSpreadBid;
                row.minSpreadBid = val;
                name_index = this->index(index.row(),0);
                name = name_index.data(Qt::DisplayRole).toString();

                emit on_mybid_changed(name, oldValue, val);
            }

            break;
        case 4:
            val = value.toDouble(&ok);
            if (ok) {
                oldValue = row.minSpreadAsk;
                row.minSpreadAsk = val;
                name_index = this->index(index.row(),0);
                name = name_index.data(Qt::DisplayRole).toString();
                emit on_myask_changed(name, oldValue, val);
            }

            break;
        }
    }

    return true;
}

bool SpreadersModel::removeRows(int row, int count, const QModelIndex &parent)
{
    m_rows.remove(row, count);
    return true;
}

bool SpreadersModel::insertRows(int row, int count, const QModelIndex &parent)
{
    m_rows.insert(row, Row());
    return true;
}

void SpreadersModel::setCurrentBidAsk(QString spreaderName, double spreadBid, double spreadAsk)
{
    for(int i=0;i<m_rows.count();++i) {
        Row& row = m_rows[i];
        if (row.spreaderName == spreaderName) {
            row.currentSpreadBid = spreadBid;
            row.currentSpreadAsk = spreadAsk;
            emit dataChanged( index(i,1), index(i,2));
            break;
        }
    }
}

void SpreadersModel::setMinSpreadsBidAsk(QString spreaderName, double spreadBid, double spreadAsk)
{
    for(int i=0;i<m_rows.count();++i) {
        Row& row = m_rows[i];
        if (row.spreaderName == spreaderName) {
            row.minSpreadBid = spreadBid;
            row.minSpreadAsk = spreadAsk;
            emit dataChanged( index(i,3), index(i,4));
            break;
        }
    }
}

void SpreadersModel::setMinSpreadsBid(QString spreaderName, double spreadBid)
{
    for(int i=0;i<m_rows.count();++i) {
        Row& row = m_rows[i];
        if (row.spreaderName == spreaderName) {
            row.minSpreadBid = spreadBid;
            emit dataChanged( index(i,3), index(i,3));
            break;
        }
    }
}

void SpreadersModel::setMinSpreadsAsk(QString spreaderName, double spreadAsk)
{
    for(int i=0;i<m_rows.count();++i) {
        Row& row = m_rows[i];
        if (row.spreaderName == spreaderName) {
            row.minSpreadAsk = spreadAsk;
            emit dataChanged( index(i,4), index(i,4));
            break;
        }
    }
}

void SpreadersModel::setSpreaderColor(QString spreaderName, QColor color)
{
    for(int i=0;i<m_rows.count();++i) {
        Row& row = m_rows[i];
        if (row.spreaderName == spreaderName) {
            row.spreaderColor = color;
            emit dataChanged( index(i,0), index(i,0));
            break;
        }
    }
}

void SpreadersModel::removeRowByName(QString spreaderName)
{
    auto it = std::find_if(m_rows.begin(),m_rows.end(),[&](Row row){ return row.spreaderName == spreaderName;});
    if (it != m_rows.end()) {
        m_rows.erase(it);
        emit dataChanged( index(0,0), index(rowCount(QModelIndex()),columnCount(QModelIndex())) );
    }
}

QVector<QString> SpreadersModel::getAllCheckedNames()
{
    QVector<QString> res;
    for( Row row: m_rows) {
        if (row.isChecked) {
            res.push_back(row.spreaderName);
        }
    }
    return res;
}
