#include <QApplication>
#include "SpreaderMainWindow.h"

#include "Settings.h"
#include "IBClient.h"
#include "Common.h"
#include "MainController.h"
#include <QSettings>
#include <QDebug>
#include <QCryptographicHash>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

//   if ( QDateTime::currentDateTimeUtc() >= QDateTime(QDate(2016,9,01)) ) {
//        QMessageBox::critical(0,"Error","Sorry, you need a licence to use this application!", QMessageBox::Ok);
//        return -1;
//   }

    IBClient* ib = IBClient::ptrInstance();

    MainController main(ib);
    main.show();

    int result = a.exec();
    return result;
}

