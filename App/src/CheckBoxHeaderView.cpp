#include "CheckBoxHeaderView.h"
#include <QDebug>

CheckBoxHeaderView::CheckBoxHeaderView(Qt::Orientation orientation, QWidget *parent)
    :QHeaderView(orientation, parent)
{
    isChecked_ = false;
}

void CheckBoxHeaderView::setIsChecked(bool val)
{
    if (isChecked_ != val) {
        isChecked_ = val;
        viewport()->update();
    }
}

void CheckBoxHeaderView::paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const
{
    painter->save();
    QHeaderView::paintSection(painter, rect, logicalIndex);
    painter->restore();
    if (logicalIndex == 0)
    {
        QStyleOptionButton option;

        option.rect = QRect(1,3,20,20);

        option.state = QStyle::State_Enabled | QStyle::State_Active;

        if (isChecked_)
            option.state |= QStyle::State_On;
        else
            option.state |= QStyle::State_Off;
        option.state |= QStyle::State_Off;

        style()->drawPrimitive(QStyle::PE_IndicatorCheckBox, &option, painter);
    }

}

void CheckBoxHeaderView::mousePressEvent(QMouseEvent *event)
{
    if (event->pos().x() > 20) {
        return;
    }
    setIsChecked(!isChecked());
    emit checkBoxClicked(isChecked());
}
