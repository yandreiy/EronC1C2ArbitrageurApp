#include "SpreadMonitor.h"
#include <QDebug>

bool SpreadMonitor::isTargetReached( double spread, const AlgoParameters &params)
{
    if (params.side == AlgoSide::Buy)  {
        double target = params.minSpread + params.delta;
        qDebug() << "Spread = " << spread <<", Target = " << target;

        /// the same as: if (spread <= target) with double precision
        if ( qFuzzyCompare(spread, target) || spread - target < 0) {
             return true;
        }

    } else {
        double target = params.minSpread - params.delta;
        qDebug() << "Spread = " << spread <<", Target = " << target;

        /// the same as: if (spread >= target) with double precision
        if ( qFuzzyCompare(spread, target) || spread - target > 0) {
            return true;
        }

    }

    return false;
}
