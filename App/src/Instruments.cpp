#include "Instruments.h"
#include "InstrumentFactory.h"
#include "InstrumentParams.h"
#include "Settings.h"
#include "log_utils.h"

using namespace IB;

Instruments::Instruments(IBClient *ib, Settings *settings, QObject *parent)
    : QObject(parent)
    , m_IbClient(ib)
    , m_Settings(settings)
{
    bool ok = connect(m_IbClient, SIGNAL(tickPriceSig(TickerId,TickType,double,int)),
                                  SLOT(tickPrice(TickerId,TickType,double,int)));
    Q_ASSERT(ok);
}

void Instruments::subscribeToMarketData()
{
    m_ReqId1 = m_IbClient->reqMktData(*m_Settings->getContract1(), "221,225,233", false);
    m_ReqId2 = m_IbClient->reqMktData(*m_Settings->getContract2(), "221,225,233", false);

    QString msg = QString("[INSTRUMENTS] Subscribed to market data with id's %1 and %2")
            .arg(m_ReqId1)
            .arg(m_ReqId2);

    qDebug() <<msg;
    //Log::info(;
}

void Instruments::tickPrice(TickerId tickerId, TickType field, double price, int canAutoExecute)
{
//    qDebug() << "price: " << price;

    TickData* td = NULL;

    if (tickerId == m_ReqId1)
        td = &m_TdContract1;
    else if (tickerId == m_ReqId2)
        td = &m_TdContract2;
    else
        return;

    if (field == ASK)
        td->askPrice = price;
    else if (field == BID)
        td->bidPrice = price;

    emit priceChanged();

    if (tickerId == m_ReqId1)
        emit contract1PriceChanged();

    emit tickPriceSig(tickerId, field, price, canAutoExecute);
}

const TickData& Instruments::getContract1TickData() const
{
    return m_TdContract1;
}

const TickData& Instruments::getContract2TickData() const
{
    return m_TdContract2;
}

Instruments::~Instruments()
{
    m_IbClient->cancelMktData(m_ReqId1);
    m_IbClient->cancelMktData(m_ReqId2);
}
