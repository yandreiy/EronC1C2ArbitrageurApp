#include "AlgoExecutionManager.h"

AlgoExecutionManager::AlgoExecutionManager(QObject *parent) : QObject(parent)
{

}

void AlgoExecutionManager::addExecution(const QString &symbol, uint qty, double price)
{
    m_map[symbol].append(qMakePair(qty,price));
}

AlgoExecutionManager::QtyPricePair AlgoExecutionManager::getExecution(const QString &symbol)
{
    AlgoExecutionManager::QtyPricePair result = qMakePair(0, 0.0);

    double sum = 0.0;

    for( auto pair: m_map.value(symbol)) {
        result.first += pair.first;
        sum += pair.first * pair.second;
    }

    if (result.first > 0) {
        result.second = sum / result.first;
    }

    return result;
}

void AlgoExecutionManager::reset()
{
    m_map.clear();
}
