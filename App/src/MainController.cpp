#include "MainController.h"
#include "ui_MainControllerWindow.h"
#include <QDebug>
#include <QPalette>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QDir>
#include "Execution.h"
#include "AddSpreaderDialog.h"
#include "CheckBoxHeaderView.h"
#include "MyBidAskDelegate.h"
#include <QToolTip>

namespace {
    const char* SETTINGS_FILE_NAME = "ControllerSavedParameters.ini";
}

MainController::MainController(IBClient *ib, QWidget *parent)
        :QMainWindow(parent)
        ,m_ib(ib)
        ,ui(new Ui::MainControllerWindow)
        ,m_logger (Logger::getLogger(QString("%1[Controller]").arg((quintptr)this).toLatin1()))
{
    ui->setupUi(this);
    restoreSettings();

    const QString appVersion = QString("Eron's Scalp Machine Controller v%1").arg(APP_VERSION);
    setWindowTitle(appVersion);

    readSettings();
    startSpreaders();
    ui->statusbar->clearMessage();

    ui->tableView->setModel(&m_model);
    CheckBoxHeaderView* headerView = new CheckBoxHeaderView(Qt::Horizontal, ui->tableView);
    ui->tableView->setHorizontalHeader( headerView );

    ui->tableView->horizontalHeader()->resizeSection(0,100);
    ui->tableView->horizontalHeader()->resizeSection(1,100);
    ui->tableView->horizontalHeader()->resizeSection(2,100);
    ui->tableView->horizontalHeader()->resizeSection(3,120);
    ui->tableView->horizontalHeader()->resizeSection(4,120);

    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    connect(headerView, &CheckBoxHeaderView::checkBoxClicked, [&](bool isChecked){

        int checkState = isChecked ? Qt::Checked : Qt::Unchecked;

        for(int i=0;i<m_model.rowCount(QModelIndex());++i) {
            auto index = m_model.index(i,0);
            m_model.setData( index, checkState, Qt::CheckStateRole );
            ui->tableView->update(index);
        }
    });

    ui->tableView->viewport()->installEventFilter(this);
    ui->tableView->viewport()->setMouseTracking(true);

    connect(ui->tableView, &QTableView::doubleClicked,[&](QModelIndex index){

        if (index.column() == 3 || index.column() == 4) {
            return;
        }

         QString spreaderName = m_model.data( m_model.index(index.row(),0),Qt::DisplayRole).toString();
         auto it = std::find_if( m_spreaders.begin(), m_spreaders.end(), [&](QSharedPointer<SpreaderMainWindow> sp){
            return QFileInfo(sp->getDirPath()).baseName() == spreaderName;
         });
         if (it != m_spreaders.end()){
             QSharedPointer<SpreaderMainWindow> sp = (*it);
             if (sp->isMinimized()) {
                 sp->showMaximized();
                 sp->restoreGeometry( m_savedGeometries[sp->getDirPath()]);

             } else {
                 m_savedGeometries[sp->getDirPath()] = sp->saveGeometry();
                 sp->showMinimized();
             }
         }
    });

    connect( &m_model, &SpreadersModel::on_mybid_changed,[&](QString name, double oldValue, double newValue){
        //qDebug() << "bid " << name <<"," << value;

        for(auto spreader: m_spreaders) {
            if ( QFileInfo(spreader->getDirPath()).baseName() == name) {
                bool ok = spreader->updateATMinSpreadBuy(newValue);
                // restore the old value in the model
                if (!ok) {
                    m_model.setMinSpreadsBid(name, oldValue);
                }
                return;
            }
        }
    });

    connect( &m_model, &SpreadersModel::on_myask_changed,[&](QString name, double oldValue, double newValue){
        //qDebug() << "ask " << name <<"," << value;
        for(auto spreader: m_spreaders) {
            if ( QFileInfo(spreader->getDirPath()).baseName() == name) {
                //qDebug() << "bid " << name <<"," << value;
                bool ok = spreader->updateATMinSpreadSell(newValue);
                if (!ok) {
                    m_model.setMinSpreadsAsk(name, oldValue);
                }

                return;
            }
        }
    });

    MyBidAskDelegate* delegate = new MyBidAskDelegate(this);
    ui->tableView->setItemDelegateForColumn(3, delegate );
    ui->tableView->setItemDelegateForColumn(4, delegate );

    configureLogger();

    m_tradesTimer = new QTimer(this);
    connect(m_tradesTimer, &QTimer::timeout,this,&MainController::onTradesTimerTimeout);
    m_tradesTimer->start(m_maxTradesInInterval.second * 1000);

    m_saveSettingsTimer = new QTimer(this);
    connect(m_saveSettingsTimer, &QTimer::timeout,this,[=](){
       saveSettings();
    });
    m_saveSettingsTimer->start(60*1000);
}

MainController::~MainController()
{
    saveSettings();
    delete ui;
}

void MainController::readSettings()
{
    QSettings mainSettings("ControllerConfig.ini", QSettings::IniFormat);
    auto ip = mainSettings.value("IP").toString().toLatin1();
    auto port = mainSettings.value("TWS_OR_GATEWAY").toString() == "TWS" ? IP_TWS : IP_GATEWAY;
    int clientId = mainSettings.value("ClientId").toInt();

    connect(m_ib, &IBClient::connectionClosedSig,[=](){
        QPalette pal = ui->statusbar->palette();
        pal.setColor(QPalette::Background, Qt::red);
        statusBar()->setPalette(pal);
        statusBar()->showMessage("Disconnected!");
        statusBar()->setAutoFillBackground(true);
    });
    connect(m_ib, &IBClient::connected,[&](){
        QPalette pal = ui->statusbar->palette();
        pal.setColor(QPalette::Background, Qt::green);
        statusBar()->setPalette(pal);
        statusBar()->showMessage("Connected!");
        statusBar()->setAutoFillBackground(true);
    });

    m_ib->connectToHost( ip, port, clientId);
    connect(m_ib, &IBClient::execDetailsSig, this, &MainController::execDetails);

    mainSettings.beginGroup("Logging");
    m_debugLevel = mainSettings.value("DebugLevel", "INFO").toString();
    m_logDirectory = mainSettings.value("LogDirectory").toString();
    mainSettings.endGroup();

    mainSettings.beginGroup("Protection");
    m_maxTradesInInterval.first = mainSettings.value("MaxSpreaderTradesInInterval").toInt();
    m_maxTradesInInterval.second =  mainSettings.value("TradesIntervalSeconds").toInt();
    mainSettings.endGroup();
    qDebug() << "m_maxTradesInInterval: " << m_maxTradesInInterval.first <<"," << m_maxTradesInInterval.second;

    mainSettings.beginGroup("Settings");
    for (int i=0;i<100;++i) {
        QTime time = QTime::fromString(mainSettings.value(QString("StartAt%1").arg(i)).toString(), "hh:mm:ss");
        if (time.isValid()) {
            m_startAllTimes << time;
        }

        time = QTime::fromString(mainSettings.value(QString("StopAt%1").arg(i)).toString(), "hh:mm:ss");
        if (time.isValid()) {
            m_stopAllTimes << time;
            qDebug() <<"Stop Ats at: " << time.toString();
        }
    }
    mainSettings.endGroup();
    qDebug() << "times to start: " << m_startAllTimes.size();

    triggerStartAtsAtTime();
    triggerStopAtsAtTime();
}

void MainController::triggerStartAtsAtTime()
{
    for(QTime time: m_startAllTimes) {

        int msec = QTime::currentTime().msecsTo(time);
        if (msec >0) {
            QTimer::singleShot(msec,Qt::PreciseTimer, this, &MainController::on_pushButton_StartATs_clicked);
        }
    }

    auto now = QDateTime::currentDateTime();
    QTimer::singleShot( now.msecsTo( QDateTime(now.date().addDays(1), QTime(0,0,1))), this, &MainController::triggerStartAtsAtTime);
}

void MainController::triggerStopAtsAtTime()
{
    for(QTime time: m_stopAllTimes) {

        int msec = QTime::currentTime().msecsTo(time);
        if (msec >0) {
            QTimer::singleShot(msec,Qt::PreciseTimer, this, &MainController::on_pushButton_StopATs_clicked);
        }
    }

    auto now = QDateTime::currentDateTime();
    QTimer::singleShot( now.msecsTo( QDateTime(now.date().addDays(1), QTime(0,0,1))), this, &MainController::triggerStopAtsAtTime);
}

void MainController::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Exit",
                                  "Are you sure you want to exit?",
                                  QMessageBox::No | QMessageBox::Yes );

    if (reply == QMessageBox::Yes) {

        Log::info(QString("Exiting All %1").arg(windowTitle()), m_logger);

        event->accept();
        m_spreaders.clear();

    } else {
        event->ignore();
    }
}

bool MainController::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseMove) {

        QMouseEvent * mouseEvent = static_cast <QMouseEvent *> (event);
        if(mouseEvent) {
            QModelIndex index = ui->tableView->indexAt(mouseEvent->pos());
            //qDebug() << "row: " << index.row() << ", col: "<<index.column();
            if (index.column() == 0) {

                QString spreaderName = m_model.data( m_model.index(index.row(),0),Qt::DisplayRole).toString();
                auto it = std::find_if(m_spreaders.begin(), m_spreaders.end(),
                               [&](QSharedPointer<SpreaderMainWindow> sp)
                { return QFileInfo(sp->getDirPath()).baseName() == spreaderName; });

                if (it!=m_spreaders.end()) {
                    QSharedPointer<SpreaderMainWindow> sp = *it;
                    auto at1 = sp->getAt1();
                    if (at1) {
                        SpreadAutoTrader::Parameters params = at1->getParams();
                        QString msg = QString("%1\n%2\n%3\n%4")
                                .arg(QString("Qty: %1 , %2").arg(params.algoParams1.qty1).arg(params.algoParams1.qty2))
                                .arg(QString("Trail: %1").arg(params.trailingDelta,0,'f',3))
                                .arg(QString("Skew: %1").arg(params.skewDelta,0,'f',3))
                                .arg(QString("MaxTimes: %1").arg(params.maxPositionsC2))
                                ;
                        QToolTip::showText( mouseEvent->globalPos(), msg);
                    }
                }
            } else {
                QToolTip::hideText();
            }
        }
    }

    return QObject::eventFilter(obj,event);
}

void MainController::addSpreader(QString spreaderPath)
{
    for(auto spreader: m_spreaders) {
        if (spreader->getDirPath() == spreaderPath) {
            ui->statusbar->showMessage( QString("Cannot add spreader. Already exists!"));
            return;
        }
    }

    Settings *spreaderSettings = new Settings( "Config.ini", m_ib, spreaderPath, this);
    if (spreaderSettings->hasError()) {
        ui->statusbar->showMessage( QString("Cannot add spreader. Error reading settings file!"));
        return;
    }

    QSharedPointer<SpreaderMainWindow> spreader( new SpreaderMainWindow(m_ib, spreaderSettings, spreaderPath));
    m_spreaders<<spreader;
    spreader->show();
    QFileInfo info(spreader->getDirPath());

    m_model.insertRow(m_spreaders.size()-1);

    QModelIndex index = m_model.index(m_spreaders.size()-1,0);
    m_model.setData(index, info.baseName(), Qt::DisplayRole);
    ui->tableView->update(index);

    connect(spreader.data(), &SpreaderMainWindow::currentSpreadBidAsk,[&](QString name, double spreadBid, double spreadAsk) {
        m_model.setCurrentBidAsk(name, spreadBid, spreadAsk);
    });

    connect(spreader.data(), &SpreaderMainWindow::autotrader1Started,[&](QString name, SpreadAutoTrader::Parameters params) {
        m_model.setMinSpreadsBidAsk(name, params.algoParams1.minSpread, params.algoParams2.minSpread);
    });

    connect(spreader.data(), &SpreaderMainWindow::autotrader1StartedNewRound,[&](QString name, AlgoParameters algoParams1, AlgoParameters algoParams2) {
        m_model.setMinSpreadsBidAsk(name, algoParams1.minSpread, algoParams2.minSpread);
    });

    connect(spreader.data(), &SpreaderMainWindow::autotrader1ChangedColor,[&](QString name, QColor color){
        m_model.setSpreaderColor(name, color);
    });

    connect(spreader.data(), &SpreaderMainWindow::beforeExit,this, [&](QString name){

        m_model.removeRowByName(name);
            /// delete it
            m_spreaders.erase(
                        std::remove_if( m_spreaders.begin(), m_spreaders.end(),
                                        [=](QSharedPointer<SpreaderMainWindow> obj){ return obj->getDirPath().endsWith(name); }),
                        m_spreaders.end()
                        );

    });

    connect(spreader.data(), &SpreaderMainWindow::autotrader1minSpreadsUpdated,[&](QString name, double minSpread1, double minSpread2){
        m_model.setMinSpreadsBidAsk(name, minSpread1, minSpread2);
    });

    connect(spreader.data(), &SpreaderMainWindow::restart,[&](SpreaderMainWindow* sender){
        if (sender) {
            auto path = sender->getDirPath();
            sender->close();
            addSpreader(path);
        }
    });
    connect(spreader.data(), &SpreaderMainWindow::autotrader1FinishedPriceDropped,[&](SpreaderMainWindow* sender){
        Log::info(QString("Price dropped %1. Will start AT in 60 sec")
                  .arg(QFileInfo(sender->getDirPath()).baseName() ), m_logger);

        QTimer::singleShot(60*1000, Qt::PreciseTimer,[=](){
           bool started = sender->startAt1();
           Log::info(QString("Started AT1: %1").arg(started),m_logger);

           if (!started) {
                QTimer::singleShot(60*10*1000, Qt::PreciseTimer,[=](){
                    bool started = sender->startAt1();
                    Log::info(QString("Started AT1: %1").arg(started),m_logger);
                });
           }
        });
    });

    ui->statusbar->showMessage( QString("Spreader added!"));
}

void MainController::saveSettings()
{
    QSettings settings( SETTINGS_FILE_NAME, QSettings::IniFormat);
    settings.setValue("geometry", saveGeometry());
}

void MainController::restoreSettings()
{
    QSettings settings( SETTINGS_FILE_NAME, QSettings::IniFormat);
    restoreGeometry( settings.value("geometry").toByteArray());
}

void MainController::configureLogger()
{
    QString debugLevel = m_debugLevel;
    QString logFile = m_logDirectory;

    const QDateTime currentDateTime = QDateTime::currentDateTimeUtc();
    logFile = logFile.replace("{$date}",currentDateTime.date().toString("yyyyMMdd"));
    logFile = logFile.replace("{$time}",currentDateTime.time().toString("hhmmss"));
    logFile = logFile.replace("{$configname}", APP_VERSION);

    qDebug() << "Logfile: " << logFile;

    LOG4CXX_DECODE_CHAR(logFileName, QString("%1").arg(logFile).toStdString());
    LOG4CXX_DECODE_CHAR(logPattern, "%d %-5p %c - %m%n");

    log4cxx::FileAppender * fileAppender = new
    log4cxx::FileAppender(log4cxx::LayoutPtr(new log4cxx::PatternLayout(logPattern)), logFileName, false);

    log4cxx::ConsoleAppender * consoleAppender = new
    log4cxx::ConsoleAppender(log4cxx::LayoutPtr(new log4cxx::PatternLayout(logPattern)));

    log4cxx::helpers::Pool p;
    fileAppender->activateOptions(p);
    consoleAppender->activateOptions(p);

    log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(consoleAppender));

    m_logger->addAppender(fileAppender);
    m_logger->setLevel(Level::toLevel(debugLevel.toStdString()));
}

void MainController::startSpreaders()
{
    QFile file("Spreaders.txt");
    if (!file.open(QFile::ReadOnly)) {
        return;
    }
    QTextStream in(&file);
    while(!in.atEnd()) {
        QString spreaderPath = in.readLine().trimmed();

        if (spreaderPath.isEmpty()){ continue; }

        addSpreader(spreaderPath);
    }
}

SpreaderMainWindow *MainController::getSpreader(QString baseDir)
{
    for (auto s: m_spreaders) {
        if (QFileInfo(s->getDirPath()).baseName() == baseDir) {
            return s.data();
        }
    }

    return nullptr;
}

void MainController::on_pushButton_StopATs_clicked()
{
    for(auto at: getAllAtsChecked()) {
        at->stop();
    }
}

void MainController::on_pushButton_StartATs_clicked()
{
    Log::info(QString("Started all Ats at: %1").arg(QTime::currentTime().toString()),m_logger);
    for(auto spreader: getAllSpreadersChecked()) {
        bool ok = spreader->startAt1();
        Log::info( QString("Started AT spreader %1: %2").arg(QFileInfo(spreader->getDirPath()).baseName()).arg(ok), m_logger );
    }
}

void MainController::execDetails(int reqId, const Contract &contract, const Execution &execution)
{
    Log::info(QString("[%0]execDetails %1: qty=%2 @ %3")
              .arg(execution.orderId)
              .arg(contract.localSymbol)
              .arg(execution.cumQty)
              .arg(execution.avgPrice)
              , m_logger);

    if (contract.localSymbol != "") {

        int& trades = m_symbolIntervalTrades[contract.localSymbol];

        if (!m_orderIds.contains(execution.orderId)) {
            ++trades;
            m_orderIds.insert(execution.orderId);
        }

        if (trades > m_maxTradesInInterval.first) {

            QString msg =QString("Stopping All ATs...Too many trades for: %1 - %2").arg(contract.localSymbol).arg(trades) ;
            Log::info(msg, m_logger);

            for(auto at: getAllAts()) {
                at->stop();
            }
        }
    }
}

void MainController::onTradesTimerTimeout()
{
    m_symbolIntervalTrades.clear();
    m_orderIds.clear();
}

void MainController::on_pushButton_AddSpreader_clicked()
{
    AddSpreaderDialog* dlg = new AddSpreaderDialog(this);

    if (dlg->exec() == QDialog::Accepted){
        for( auto line: dlg->getLines()) {
            addSpreader(line);
        }
    }
}

void MainController::on_pushButton_TightenSpreads_clicked()
{
    for(auto at: getAllAtsChecked()) {
        double value = ui->doubleSpinBox_TightWideRatio->value();
        at->tightenSpreadsBy(value);
    }
}

void MainController::on_pushButton_TightenSkewDelta_clicked()
{
    for(auto at: getAllAtsChecked()) {
        double value = ui->doubleSpinBox_TightWideRatio->value();
        at->tightenSkewDeltaBy(value);
    }
}

void MainController::on_pushButton_TightenTrailDelta_clicked()
{
    for(auto at: getAllAtsChecked()) {
        double value = ui->doubleSpinBox_TightWideRatio->value();
        at->tightenTrailDeltaBy(value);
    }
}

void MainController::on_pushButton_WidenSpreads_clicked()
{
    for(auto at: getAllAtsChecked()) {
        double value = ui->doubleSpinBox_TightWideRatio->value();
        at->widenSpreadsBy(value);
    }
}

void MainController::on_pushButton_WidenSkewDelta_clicked()
{
    for(auto at: getAllAtsChecked()) {
        double value = ui->doubleSpinBox_TightWideRatio->value();
        at->widenSkewDeltaBy(value);
    }
}

void MainController::on_pushButton_WidenTrailDelta_clicked()
{
    for(auto at: getAllAtsChecked()) {
        double value = ui->doubleSpinBox_TightWideRatio->value();
        at->widenTrailDeltaBy(value);
    }
}

std::list<std::shared_ptr<SpreadAutoTrader> > MainController::getAllAtsChecked()
{
    std::list<std::shared_ptr<SpreadAutoTrader> > list;

    auto checkedNames = m_model.getAllCheckedNames();
    for(QString name: checkedNames) {

        auto it = std::find_if(m_spreaders.begin(), m_spreaders.end(),
                               [&](QSharedPointer<SpreaderMainWindow> sp)
        { return QFileInfo(sp->getDirPath()).baseName() == name; });

        if (it!= m_spreaders.end()) {
            auto ats = (*it)->getATs();
            list.insert(list.end(), ats.begin(), ats.end() );
        }
    }
    return list;
}

std::list<std::shared_ptr<SpreadAutoTrader> > MainController::getAllAts()
{
    std::list<std::shared_ptr<SpreadAutoTrader> > list;
    for( auto sp: m_spreaders) {
        auto ats = sp->getATs();
        list.insert(list.end(), ats.begin(), ats.end() );
    }

    return list;
}

QVector<QSharedPointer<SpreaderMainWindow> > MainController::getAllSpreadersChecked()
{
    QVector<QSharedPointer<SpreaderMainWindow> > result;

    auto checkedNames = m_model.getAllCheckedNames();
    for(QString name: checkedNames) {

        auto it = std::find_if(m_spreaders.begin(), m_spreaders.end(),
                               [&](QSharedPointer<SpreaderMainWindow> sp)
        { return QFileInfo(sp->getDirPath()).baseName() == name; });
        if (it!=m_spreaders.end()) {
            result << *it;
        }
    }

    return result;
}
