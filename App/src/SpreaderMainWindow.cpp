#include "SpreaderMainWindow.h"
#include "ui_SpreaderMainWindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QSettings>
#include <QTimer>
#include <QFileInfo>
#include "log_utils.h"

namespace {

const char* START_BUYING_SPREAD = "Start Buying Spread";
const char* STOP_BUYING_SPREAD = "Stop Buying Spread";

const char* START_SELLING_SPREAD = "Start Selling Spread";
const char* STOP_SELLING_SPREAD = "Stop Selling Spread";

const char* SETTINGS_FILE_NAME = "SavedParameters.ini";

const char* START_AUTOTRADER1 = "Start AT 1";
const char* STOP_AUTOTRADER1 = "Stop AT 1";

const char* START_AUTOTRADER2 = "Start AT 2";
const char* STOP_AUTOTRADER2 = "Stop AT 2";

const char* START_AUTOTRADER3 = "Start AT 3";
const char* STOP_AUTOTRADER3 = "Stop AT 3";

const char* START_AUTOTRADER4 = "Start AT 4";
const char* STOP_AUTOTRADER4 = "Stop AT 4";

}

SpreaderMainWindow::SpreaderMainWindow(IBClient *ib, Settings *settings, QString spreaderDirPath, QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::SpreaderMainWindow),
        m_Instruments(new Instruments(ib, settings, this)),
        m_ibClient(ib),
        m_Settings(settings),
        m_logger (Logger::getLogger(QString("%1[MAIN]").arg((quintptr)this).toLatin1())),
        m_TradeLogger(Logger::getLogger(QString("%1[TRADING]").arg((quintptr)this).toLatin1())),
        m_IbErrorLogger(Logger::getLogger(QString("%1[IB_ERROR]").arg((quintptr)this).toLatin1())),
        m_ActiveOrders(new ActiveOrders(ib, this)),
        m_TradingLog(new TradingLog(ib, this)),
        m_timer(new QTimer(this)),
        m_spreaderDirPath(spreaderDirPath),
        m_fileAppender(nullptr),
        m_timerSaveSettings(new QTimer(this))

{
    ui->setupUi(this);
    this->layout()->setSizeConstraint(QLayout::SetFixedSize);

    configureLogger();

    bool ok = connect(m_Instruments, SIGNAL(priceChanged()), SLOT(onPriceChanged()) ); Q_ASSERT(ok);

    ok = connect(m_Settings, SIGNAL(contract1DetailsReceived()), SLOT(onContract1DetailsReceived()) ); Q_ASSERT(ok);
    ok = connect(m_Settings, SIGNAL(contract2DetailsReceived()), SLOT(onContract2DetailsReceived()) ); Q_ASSERT(ok);

    ok = connect(m_ActiveOrders, SIGNAL(openOrdersChanged()), SLOT(updateOpenOrdersList()) ); Q_ASSERT(ok);
    ok = connect(m_TradingLog, SIGNAL(newTradingLogItem(QString)), SLOT(onNewTradingLogItem(QString)) ); Q_ASSERT(ok);

    restoreSettings();

    connect(m_timer, &QTimer::timeout, [=](){

        if (
             (m_spreadAutoTrader1 && m_spreadAutoTrader1->hasActiveOrders()) ||
             (m_spreadAutoTrader2 && m_spreadAutoTrader2->hasActiveOrders()) ||
             (m_spreadAutoTrader3 && m_spreadAutoTrader3->hasActiveOrders()) ||
             (m_spreadAutoTrader4 && m_spreadAutoTrader4->hasActiveOrders()) ||
                    (m_algoBuy && m_algoBuy->hasActiveOrder())  ||
                    (m_algoSell && m_algoSell->hasActiveOrder())
            ) {

            ui->centralWidget->setStyleSheet("QWidget#centralWidget{border-style: solid; border-width:2px;border-color:red;}");

            if( m_spreadAutoTrader1 && m_spreadAutoTrader1->hasActiveOrders()) {
                emit autotrader1ChangedColor(QFileInfo(m_spreaderDirPath).baseName(), QColor(Qt::red));
            }
        }
        else if(
             (m_spreadAutoTrader1 && m_spreadAutoTrader1->isStarted()) ||
             (m_spreadAutoTrader2 && m_spreadAutoTrader2->isStarted()) ||
             (m_spreadAutoTrader3 && m_spreadAutoTrader3->isStarted()) ||
             (m_spreadAutoTrader4 && m_spreadAutoTrader4->isStarted())
                ) {

            ui->centralWidget->setStyleSheet("QWidget#centralWidget{border-style: solid; border-width:2px;border-color:green;}");

            if( m_spreadAutoTrader1 && m_spreadAutoTrader1->isStarted()) {
                if (m_spreadAutoTrader1->maxPositionsLimitReached()) {
                    emit autotrader1ChangedColor(QFileInfo(m_spreaderDirPath).baseName(), QColor(255,165,0)); // orange
                } else {
                    emit autotrader1ChangedColor(QFileInfo(m_spreaderDirPath).baseName(), QColor(Qt::green));
                }
            }

        } else {
            ui->centralWidget->setStyleSheet("QWidget#centralWidget{border-style: solid; border-width:1px;border-color:black;}");
            if( m_spreadAutoTrader1 ) {
                emit autotrader1ChangedColor(QFileInfo(m_spreaderDirPath).baseName(), QColor(Qt::white));
            }
        }
    });

    m_timer->start(1000);

    connect(m_timerSaveSettings, &QTimer::timeout, this, [=](){
      saveSettings();
    });
    m_timerSaveSettings->start(60*1000);

    m_algoBuyManager.reset(new AlgoManager(m_ibClient, m_fileAppender));
    m_algoSellManager.reset(new AlgoManager(m_ibClient, m_fileAppender));

    ui->checkBoxToggleAutotrader->setChecked(false);
    ui->checkBoxToggleManualTrader->setChecked(false);
    ui->checkBoxToggleTabWidget->setChecked(false);

    initializeIbConnection();

    qRegisterMetaType<SpreadAutoTrader::Parameters>("SpreadAutoTrader::Parameters");
}

void SpreaderMainWindow::saveSettings()
{
    QSettings settings( QString("%1/%2").arg(m_spreaderDirPath).arg(SETTINGS_FILE_NAME), QSettings::IniFormat);

    settings.setValue("minSpreadBuy", ui->doubleSpinBoxMinSpreadBuying->value());
    settings.setValue("minSpreadSell", ui->doubleSpinBoxMinSpreadSelling->value());
//    settings.setValue("spreadFormula", ui->comboBox_SpreadFormula->currentIndex());
    settings.setValue("orderEntryDelta", ui->doubleSpinBoxOrderEntryDelta->value());
    settings.setValue("tradingHoursStart", ui->timeEdit_Start->time().toString());
    settings.setValue("tradingHoursEnd", ui->timeEdit_Stop->time().toString());
    settings.setValue("spreadFormulaX", ui->doubleSpinBoxSpreadFormulaX->value());
    settings.setValue("spreadFormulaY", ui->doubleSpinBoxSpreadFormulaY->value());
    settings.setValue("minDeltaLimitPriceAndAskPrice", ui->doubleSpinBoxMinDeltaBidAsk->value());
    settings.setValue("geometry", saveGeometry());


    settings.beginGroup("Auto-trader");
    settings.setValue("qty1",ui->spinBoxAutoTrading_QtyC1->value());
    settings.setValue("qty2",ui->spinBoxAutoTrading_QtyC2->value());
//    settings.setValue("spreadFormula", ui->comboBox_AutoTrading_SpreadFormula->currentIndex());
    settings.setValue("minSpreadBuy",ui->doubleSpinBoxAutoTrading_MinSpreadBuy->value());
    settings.setValue("minSpreadSell",ui->doubleSpinBoxAutoTrading_MinSpreadSell->value());
    settings.setValue("trailingDelta",ui->doubleSpinBoxAutoTrading_TrailingPriceDelta->value());
    settings.setValue("skewDelta",ui->doubleSpinBoxAutoTrading_SkewPriceDelta->value());
    settings.setValue("tradeLevelMin",ui->doubleSpinBoxAutoTrading_PositionsCloseLevelMin->value());
    settings.setValue("tradeLevelMax",ui->doubleSpinBoxAutoTrading_PositionsCloseLevel_Max->value());
    settings.setValue("maxPositionsToOpenC2",ui->spinBoxAutoTrading_MaxPositionsF2->value());
    settings.setValue("orderEntryDelta", ui->doubleSpinBox_AT_OrderEntryDelta->value());
    settings.setValue("tradingHoursStart", ui->timeEdit_AT_Start->time().toString());
    settings.setValue("tradingHoursEnd", ui->timeEdit_AT_Stop->time().toString());
    settings.setValue("spreadFormulaX", ui->doubleSpinBoxSpreadFormulaX_AT->value());
    settings.setValue("spreadFormulaY", ui->doubleSpinBoxSpreadFormulaY_AT->value());
    settings.setValue("minDeltaLimitPriceAndAskPrice", ui->doubleSpinBox_AT_MinDeltaBidAsk->value());

    settings.endGroup();
    settings.sync();
}

void SpreaderMainWindow::restoreSettings()
{
    QString filename = QString("%1/%2").arg(m_spreaderDirPath).arg(SETTINGS_FILE_NAME);
    QSettings settings( filename, QSettings::IniFormat);

    qDebug()<<"SavedParams: "<< filename;

    ui->doubleSpinBoxMinSpreadBuying->setValue( settings.value("minSpreadBuy").toDouble());
    ui->doubleSpinBoxMinSpreadSelling->setValue( settings.value("minSpreadSell").toDouble());
//    ui->comboBox_SpreadFormula->setCurrentIndex( settings.value("spreadFormula").toInt());
    ui->doubleSpinBoxOrderEntryDelta->setValue( settings.value("orderEntryDelta").toDouble());
    ui->timeEdit_Start->setTime( QTime::fromString(settings.value("tradingHoursStart").toString()));
    ui->timeEdit_Stop->setTime( QTime::fromString(settings.value("tradingHoursEnd").toString()));
    ui->doubleSpinBoxSpreadFormulaX->setValue( settings.value("spreadFormulaX").toDouble());
    ui->doubleSpinBoxSpreadFormulaY->setValue( settings.value("spreadFormulaY").toDouble());
    ui->doubleSpinBoxMinDeltaBidAsk->setValue( settings.value("minDeltaLimitPriceAndAskPrice").toDouble());
    restoreGeometry( settings.value("geometry").toByteArray());

    settings.beginGroup("Auto-trader");
    ui->spinBoxAutoTrading_QtyC1->setValue( settings.value("qty1").toInt());
    ui->spinBoxAutoTrading_QtyC2->setValue( settings.value("qty2").toInt());
//    ui->comboBox_AutoTrading_SpreadFormula->setCurrentIndex(settings.value("spreadFormula").toInt());
    ui->doubleSpinBoxAutoTrading_MinSpreadBuy->setValue( settings.value("minSpreadBuy").toDouble());
    ui->doubleSpinBoxAutoTrading_MinSpreadSell->setValue( settings.value("minSpreadSell").toDouble());
    ui->doubleSpinBoxAutoTrading_TrailingPriceDelta->setValue( settings.value("trailingDelta").toDouble());
    ui->doubleSpinBoxAutoTrading_SkewPriceDelta->setValue( settings.value("skewDelta").toDouble());
    ui->doubleSpinBoxAutoTrading_PositionsCloseLevelMin->setValue( settings.value("tradeLevelMin").toDouble());
    ui->doubleSpinBoxAutoTrading_PositionsCloseLevel_Max->setValue( settings.value("tradeLevelMax").toDouble());
    ui->spinBoxAutoTrading_MaxPositionsF2->setValue( settings.value("maxPositionsToOpenC2").toInt());
    ui->doubleSpinBox_AT_OrderEntryDelta->setValue( settings.value("orderEntryDelta").toDouble());
    ui->timeEdit_AT_Start->setTime( QTime::fromString(settings.value("tradingHoursStart").toString()));
    ui->timeEdit_AT_Stop->setTime( QTime::fromString(settings.value("tradingHoursEnd").toString()));
    ui->doubleSpinBoxSpreadFormulaX_AT->setValue( settings.value("spreadFormulaX").toDouble());
    ui->doubleSpinBoxSpreadFormulaY_AT->setValue( settings.value("spreadFormulaY").toDouble());
    ui->doubleSpinBox_AT_MinDeltaBidAsk->setValue( settings.value("minDeltaLimitPriceAndAskPrice").toDouble());
    settings.endGroup();
}

SpreaderMainWindow::~SpreaderMainWindow()
{
    saveSettings();

    delete ui;
}

void SpreaderMainWindow::initializeIbConnection()
{
    bool ok = connect( m_ibClient, SIGNAL(connected()), SLOT(onIbConnected()) ); Q_ASSERT(ok);
    ok = connect( m_ibClient, SIGNAL(connectionClosedSig()), SLOT(onIbDisconnected()) ); Q_ASSERT(ok);
    ok = connect( m_ibClient, SIGNAL(errorSig(int,int,IBString)), SLOT(onIbError(int,int,IBString)) ); Q_ASSERT(ok);

    if (m_ibClient->isConnected())
    {
        onIbConnected();
    } else {
        onIbDisconnected();
    }
}

bool SpreaderMainWindow::runIfConfirmed(std::function<void ()> func)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Algo Start Confirmation",
                                  "Are you sure? Your order will get executed immediately!",
                                  QMessageBox::No | QMessageBox::Yes );

    if (reply == QMessageBox::Yes) {
        func();
        return true;
    }

    return false;
}

void SpreaderMainWindow::onIbConnected()
{
    connect(m_Settings, &Settings::contractsReady,[=](){

        if (!m_Settings->requestContractDetails()) {
            onContract1DetailsReceived();
            onContract2DetailsReceived();
        }

        m_Instruments->subscribeToMarketData();

        ui->pushBtnBuy->setEnabled(true);
        ui->pushBtnSell->setEnabled(true);

        ui->pushButtonStartAutoTrading->setEnabled(true);
        ui->pushButtonStartAutoTrader2->setEnabled(true);
        ui->pushButtonStartAutoTrader3->setEnabled(true);
        ui->pushButtonStartAutoTrader4->setEnabled(true);

        Q_ASSERT( m_fileAppender != nullptr);
        m_algoBuy.reset(new SpreadAlgo( m_ibClient, m_Settings, *m_Settings->getContract1(), *m_Settings->getContract2(), m_fileAppender));
        m_algoBuy->init();
        m_algoSell.reset(new SpreadAlgo( m_ibClient, m_Settings, *m_Settings->getContract1(), *m_Settings->getContract2(), m_fileAppender ));
        m_algoSell->init();

        logAlgoEvents(m_algoBuy, ui->listWidgetTradingLog);
        logAlgoEvents(m_algoSell, ui->listWidgetTradingLog);

        m_algoBuy->setMktDataTickerIds( m_Instruments->getTickerIds());
        m_algoSell->setMktDataTickerIds( m_Instruments->getTickerIds());
        connect(m_Instruments, &Instruments::tickPriceSig, m_algoBuy.get(), &SpreadAlgo::tickPrice);
        connect(m_Instruments, &Instruments::tickPriceSig, m_algoSell.get(), &SpreadAlgo::tickPrice);

        connect(m_algoBuyManager.get(), &AlgoManager::finished,
                [=]() {
            this->ui->pushBtnBuy->setText(START_BUYING_SPREAD);
            this->ui->labelBuyAlgoQty->setText( "0, 0, 0.0");
        });

        connect(m_algoSellManager.get(), &AlgoManager::finished,
                [=]( ) {
            this->ui->pushBtnSell->setText(START_SELLING_SPREAD);
            this->ui->labelSellAlgoQty->setText( "0, 0, 0.0");
        });

        Log::info("Connection established", m_TradeLogger);

        m_algoBuyManager->set_algo(m_algoBuy);
        m_algoSellManager->set_algo(m_algoSell);

    });

    m_Settings->readInstruments();
}

void SpreaderMainWindow::onIbDisconnected()
{
    ui->pushBtnBuy->setEnabled(false);
    ui->pushBtnSell->setEnabled(false);
    ui->pushButtonStartAutoTrading->setEnabled(false);
    ui->pushButtonStartAutoTrader2->setEnabled(false);
    ui->pushButtonStartAutoTrader3->setEnabled(false);
    ui->pushButtonStartAutoTrader4->setEnabled(false);

    Log::info("Connection lost", m_TradeLogger);
}

void SpreaderMainWindow::onIbError(const int id, const int errorCode, const IBString &errorString)
{
    QString msg = QString("id %1, code %2, %3").arg(id).arg(errorCode).arg(errorString);
    Log::error(msg, m_IbErrorLogger);
    qDebug() << msg;
}

void SpreaderMainWindow::onPriceChanged()
{
    const TickData& tdC1 = m_Instruments->getContract1TickData();
    const TickData& tdC2 = m_Instruments->getContract2TickData();

    ui->lineEditAskC1->setText( QString::number(tdC1.askPrice) );
    ui->lineEditBidC1->setText( QString::number(tdC1.bidPrice) );

    ui->lineEditAskC2->setText( QString::number(tdC2.askPrice) );
    ui->lineEditBidC2->setText( QString::number(tdC2.bidPrice) );

    double spreadBid = m_spreadFormula.calculateSpread(tdC1.bidPrice, tdC2.bidPrice);
    double spreadAsk = m_spreadFormula.calculateSpread(tdC1.askPrice, tdC2.askPrice);
    ui->lineEditSpreadBuy_General->setText( QString::number( spreadBid ) );
    ui->lineEditSpreadSell_General->setText( QString::number( spreadAsk ) );

    emit currentSpreadBidAsk(QFileInfo(m_spreaderDirPath).baseName(), spreadBid, spreadAsk );
}

void SpreaderMainWindow::onContract1DetailsReceived()
{
    const QString text = m_Settings->getContract1()->localSymbol;
    ui->labelInstr1Header->setText(text);
}

void SpreaderMainWindow::onContract2DetailsReceived()
{
    const QString text = m_Settings->getContract2()->localSymbol;
    ui->labelInstr2Header->setText(text);
}

AlgoParameters SpreaderMainWindow::getAlgoParams()
{
    AlgoParameters params;
    params.qty1 = ui->spinBoxQtyC1->value();
    params.qty2 = qAbs( ui->spinBoxQtyC2->value() );
    if (ui->spinBoxQtyC2->value() < 0 ) {
        params.qty2_Negative = true;
    }
    params.delta = ui->doubleSpinBoxOrderEntryDelta->value();
//    params.spreadFormulaOld = (AlgoParameters::SpreadFormulaOld)ui->comboBox_SpreadFormula->currentIndex();
    params.tradingHours.start = ui->timeEdit_Start->time();
    params.tradingHours.end = ui->timeEdit_Stop->time();
    params.protectionPriceDrop = m_Settings->getProtectionPriceDropValues();
    params.spreadFormula = m_spreadFormula;
    params.minDeltaBidAsk = ui->doubleSpinBoxMinDeltaBidAsk->value();

    return params;
}

void SpreaderMainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Exit",
                                  "Are you sure you want to exit?",
                                  QMessageBox::No | QMessageBox::Yes );

    if (reply == QMessageBox::Yes) {

        Log::info(QString("Exiting %1").arg(windowTitle()));

        auto closeAppenders = [=](log4cxx::AppenderList list){
        for(int i = 0; i < list.size(); ++i) {
            log4cxx::Appender *appender = list[i];
            appender->close();
        }
        };

        closeAppenders( log4cxx::Logger::getRootLogger()->getAllAppenders());
        closeAppenders(m_logger->getAllAppenders());
        closeAppenders(m_TradeLogger->getAllAppenders());
        closeAppenders(m_IbErrorLogger->getAllAppenders());

        emit beforeExit(QFileInfo(m_spreaderDirPath).baseName());

        event->accept();
    } else {
        event->ignore();
    }
}

void SpreaderMainWindow::on_pushBtnBuy_clicked()
{
    if (ui->pushBtnBuy->text().startsWith(START_BUYING_SPREAD)) {
        ui->pushBtnBuy->setDisabled(true);

        /// start the algo
        AlgoParameters params = getAlgoParams();
        params.side = AlgoSide::Buy;
        params.minSpread = ui->doubleSpinBoxMinSpreadBuying->value();

        auto startFn = [=](){
            m_algoBuyManager->start(params);
                ui->pushBtnBuy->setText( STOP_BUYING_SPREAD);
                ui->labelBuyAlgoQty->setText( QString("%1, %2, %3").arg(params.qty1).arg(params.qty2).arg(params.minSpread));
        };

        if (m_algoBuy->willTradeImmediately(AlgoSide::Buy, params.minSpread, m_spreadFormula)) {
            runIfConfirmed( startFn );
        } else {
            startFn();
        }

        ui->pushBtnBuy->setEnabled(true);
    }
    else {
        ui->pushBtnBuy->setDisabled(true);
        m_algoBuyManager->stop();
        ui->pushBtnBuy->setText( START_BUYING_SPREAD);
        ui->pushBtnBuy->setEnabled(true);
    }
}

void SpreaderMainWindow::on_pushBtnSell_clicked()
{
    if (ui->pushBtnSell->text().startsWith(START_SELLING_SPREAD)) {
        ui->pushBtnSell->setDisabled(true);

        /// start the algo
        AlgoParameters params = getAlgoParams();
        params.side = AlgoSide::Sell;
        params.minSpread = ui->doubleSpinBoxMinSpreadSelling->value();

        auto startFn = [=](){
            m_algoSellManager->start(params);
                ui->pushBtnSell->setText( STOP_SELLING_SPREAD);
                ui->labelSellAlgoQty->setText( QString("%1, %2, %3").arg(params.qty1).arg(params.qty2).arg(params.minSpread));
        };

        if (m_algoSell->willTradeImmediately(AlgoSide::Sell, params.minSpread, m_spreadFormula)) {
            runIfConfirmed( startFn );
        } else {
            startFn();
        }

        ui->pushBtnSell->setEnabled(true);
    }
    else {
        ui->pushBtnSell->setDisabled(true);
        m_algoSellManager->stop();
        ui->pushBtnSell->setText(START_SELLING_SPREAD);
        ui->pushBtnSell->setEnabled(true);
    }
}

void SpreaderMainWindow::updateOpenOrdersList()
{
    ui->listWidgetActiveOrders->clear();

    foreach(const OpenOrderData& orderData, m_ActiveOrders->getOpenOrders())
    {
        const QString text = QString("%1 %2 %3 @ %4").arg(orderData.order.action)
                        .arg(orderData.order.totalQuantity).arg(orderData.contract.localSymbol)
                        .arg(orderData.order.lmtPrice);

        ui->listWidgetActiveOrders->addItem(text);
    }
}

void SpreaderMainWindow::onNewTradingLogItem(const QString &tradingItem)
{
    ui->listWidgetTradingLog->addItem(tradingItem);
}

SpreadAutoTrader::Parameters SpreaderMainWindow::getAutoTraderParams()
{
    SpreadAutoTrader::Parameters params;
    params.algoParams1.qty1 = ui->spinBoxAutoTrading_QtyC1->value();
    params.algoParams1.qty2 = qAbs(ui->spinBoxAutoTrading_QtyC2->value());
    if (ui->spinBoxAutoTrading_QtyC2->value() < 0) {
        params.algoParams1.qty2_Negative = true;
    }

    params.algoParams1.minSpread = ui->doubleSpinBoxAutoTrading_MinSpreadBuy->value();
    params.algoParams1.side = AlgoSide::Buy;
    params.algoParams1.delta = ui->doubleSpinBox_AT_OrderEntryDelta->value();
    params.algoParams1.protectionPriceDrop = m_Settings->getProtectionPriceDropValues();
    params.algoParams1.tradingHours = TradingHours( ui->timeEdit_AT_Start->time(), ui->timeEdit_AT_Stop->time());
    params.algoParams1.spreadFormula = SpreadFormula( ui->doubleSpinBoxSpreadFormulaX_AT->value(), ui->doubleSpinBoxSpreadFormulaY_AT->value());
    params.algoParams1.minDeltaBidAsk = ui->doubleSpinBox_AT_MinDeltaBidAsk->value();

    params.algoParams2.qty1 = params.algoParams1.qty1;
    params.algoParams2.qty2 = params.algoParams1.qty2;
    params.algoParams2.qty2_Negative = params.algoParams1.qty2_Negative;

    params.algoParams2.minSpread = ui->doubleSpinBoxAutoTrading_MinSpreadSell->value();
    params.algoParams2.side = AlgoSide::Sell;
    params.algoParams2.delta = params.algoParams1.delta;
    params.algoParams2.protectionPriceDrop = params.algoParams1.protectionPriceDrop;
    params.algoParams2.tradingHours = params.algoParams1.tradingHours;
    params.algoParams2.spreadFormula = params.algoParams1.spreadFormula;
    params.algoParams2.minDeltaBidAsk = params.algoParams1.minDeltaBidAsk;

    params.maxPositionsC2 = ui->spinBoxAutoTrading_MaxPositionsF2->value();
    params.trailingDelta = ui->doubleSpinBoxAutoTrading_TrailingPriceDelta->value();
    params.closeALlOutsideSpreadInterval =
                    qMakePair(  ui->doubleSpinBoxAutoTrading_PositionsCloseLevelMin->value(), ui->doubleSpinBoxAutoTrading_PositionsCloseLevel_Max->value());
    params.freezeTimeMsec = ui->spinBoxAutoTrading_FreezeTimeMsec->value();

    params.maxExecutionsUnder1min = m_Settings->getProtectionMaxATExecutionsUnder1min();

    params.type = SpreadAutoTrader::Parameters::typeFromString(ui->comboBox_AT_algotype->currentText());
    params.skewDelta = ui->doubleSpinBoxAutoTrading_SkewPriceDelta->value();

    return params;
}

void SpreaderMainWindow::on_doubleSpinBoxAutoTrading_PositionsCloseLevelMin_valueChanged(double arg1)
{
    if (ui->doubleSpinBoxAutoTrading_PositionsCloseLevel_Max->value() < arg1) {
        ui->doubleSpinBoxAutoTrading_PositionsCloseLevel_Max->setValue(arg1);
    }
}


void SpreaderMainWindow::startAutoTrader(UiContext uiContext)
{
    SpreadAutoTrader::Parameters params = getAutoTraderParams();

    auto startFn = [=](){
        uiContext.button->setDisabled(true);

        std::shared_ptr<SpreadAlgo> algo1(new SpreadAlgo( m_ibClient, m_Settings, *m_Settings->getContract1(), *m_Settings->getContract2(), m_fileAppender));
        algo1->init();
        std::shared_ptr<SpreadAlgo> algo2(new SpreadAlgo( m_ibClient, m_Settings, *m_Settings->getContract1(), *m_Settings->getContract2(), m_fileAppender));
        algo2->init();

        logAlgoEvents(algo1, ui->listWidgetTradingLog);
        logAlgoEvents(algo2, ui->listWidgetTradingLog);

        algo1->setMktDataTickerIds( m_Instruments->getTickerIds());
        algo2->setMktDataTickerIds( m_Instruments->getTickerIds());
        connect(m_Instruments, &Instruments::tickPriceSig, algo1.get(), &SpreadAlgo::tickPrice);
        connect(m_Instruments, &Instruments::tickPriceSig, algo2.get(), &SpreadAlgo::tickPrice);

        uiContext.autotrader.reset( new SpreadAutoTrader( algo1, algo2, m_fileAppender));

        // if it's the first autotrader, emit minSpreadBid/Ask params
        if(uiContext.startTxt == START_AUTOTRADER1) {
            QString name = QFileInfo(m_spreaderDirPath).baseName();
            emit autotrader1Started(name, params);
            connect( uiContext.autotrader.get(), &SpreadAutoTrader::startedNewRound, [=](AlgoParameters algo1Params, AlgoParameters algo2Params){
                emit autotrader1StartedNewRound(name, algo1Params, algo2Params);

                ui->doubleSpinBoxAutoTrading_MinSpreadBuy->setValue(algo1Params.minSpread);
                ui->doubleSpinBoxAutoTrading_MinSpreadSell->setValue(algo2Params.minSpread);
            });

            connect( uiContext.autotrader.get(), &SpreadAutoTrader::updatedMinSpreads, [=](double minSpread1, double minSpread2){
                emit autotrader1minSpreadsUpdated(name, minSpread1, minSpread2);

                ui->doubleSpinBoxAutoTrading_MinSpreadBuy->setValue(minSpread1);
                ui->doubleSpinBoxAutoTrading_MinSpreadSell->setValue(minSpread2);
            });
        }

        auto colorFn = [=]() {
            if (uiContext.autotrader->hasActiveOrders()) {
                uiContext.listWidget->setStyleSheet("border-style: solid; border-width:2px;border-color:red");
            }else if (uiContext.autotrader->isStarted()) {
                uiContext.listWidget->setStyleSheet("border-style: solid; border-width:2px;border-color:green");
            }
            else {
                uiContext.listWidget->setStyleSheet("border-style: solid; border-width:1px;border-color:black");
            }
        };

        QTimer *timer = new QTimer(uiContext.autotrader.get());
        connect(timer, &QTimer::timeout, [=](){
            colorFn();
        });
        timer->start(1000);

        connect(uiContext.autotrader.get(), &SpreadAutoTrader::logAction,
                [=](QString msg) {
            uiContext.listWidget->addItem(msg);
        }
        );

        connect(uiContext.autotrader.get(), &SpreadAutoTrader::finished,
                [=]( ) {
            uiContext.button->setText(uiContext.startTxt);
            if (uiContext.autotrader.get() == m_spreadAutoTrader1.get()) {
                if (uiContext.autotrader->wasPriceDropp()) {
                    emit autotrader1FinishedPriceDropped(this);
                }
            }
        });
        uiContext.autotrader->start(params);

        uiContext.button->setText( uiContext.stopTxt);
        uiContext.button->setEnabled(true);
    };

    bool ok = true;
    if (params.skewDelta < (params.trailingDelta/2)) {

        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Algo Start Confirmation",
                                      "Skew Delta Seems Low, Are You Sure?",
                                      QMessageBox::No | QMessageBox::Yes );

        if (reply == QMessageBox::No) {
            ok = false;
        }
    }
    if (!ok) {
        return;
    }


    if ( m_algoBuy->willTradeImmediately(AlgoSide::Buy, params.algoParams1.minSpread, params.algoParams1.spreadFormula) ||
                    m_algoSell->willTradeImmediately(AlgoSide::Sell, params.algoParams2.minSpread, params.algoParams2.spreadFormula)
                    ) {
        runIfConfirmed( startFn);
    }
    else {
        startFn();
    }
}


void SpreaderMainWindow::startStopAutoTrader(UiContext uiContext)
{
    if ( uiContext.button->text().startsWith( uiContext.startTxt )) {
        startAutoTrader(uiContext);
    }
    else {
        uiContext.button->setText( uiContext.stopTxt);

        uiContext.button->setDisabled(true);
        uiContext.autotrader->stop();
        uiContext.button->setEnabled(true);
    }
}

bool SpreaderMainWindow::startAt1()
{
    if (m_spreadAutoTrader1 && m_spreadAutoTrader1->isStarted()) {
        return false;
    }

    SpreadAutoTrader::Parameters params = getAutoTraderParams();
    if ( m_algoBuy->willTradeImmediately
         (AlgoSide::Buy, params.algoParams1.minSpread, params.algoParams1.spreadFormula) ||
                    m_algoSell->willTradeImmediately(AlgoSide::Sell, params.algoParams2.minSpread, params.algoParams2.spreadFormula)
                    ) {
        return false;
    }

    on_pushButtonStartAutoTrading_clicked();
    return true;
}

std::list<std::shared_ptr<SpreadAutoTrader> > SpreaderMainWindow::getATs()
{
    std::list<std::shared_ptr<SpreadAutoTrader>> atList;
    if( m_spreadAutoTrader1 && m_spreadAutoTrader1->isStarted()) {
        atList.push_back(m_spreadAutoTrader1);
    }
    if( m_spreadAutoTrader2 && m_spreadAutoTrader2->isStarted()) {
        atList.push_back(m_spreadAutoTrader2);
    }
    if( m_spreadAutoTrader3 && m_spreadAutoTrader3->isStarted()) {
        atList.push_back(m_spreadAutoTrader3);
    }
    if( m_spreadAutoTrader4 && m_spreadAutoTrader4->isStarted()) {
        atList.push_back(m_spreadAutoTrader4);
    }

    return atList;
}

std::shared_ptr<SpreadAutoTrader> SpreaderMainWindow::getAt1()
{
    if( m_spreadAutoTrader1 && m_spreadAutoTrader1->isStarted()) {
        return m_spreadAutoTrader1;
    }
    return std::shared_ptr<SpreadAutoTrader>();
}

bool SpreaderMainWindow::updateATMinSpreadBuy(double value)
{

    if( !m_spreadAutoTrader1 || !m_spreadAutoTrader1->isStarted()) {
        ui->doubleSpinBoxAutoTrading_MinSpreadBuy->setValue(value);
    }

    if( m_spreadAutoTrader1 && m_spreadAutoTrader1->isStarted()) {

        auto action = [=](){
            m_spreadAutoTrader1->updateMinSpreadBuy(value);
            ui->doubleSpinBoxAutoTrading_MinSpreadBuy->setValue(value);
        };

        if (m_spreadAutoTrader1->willTradeImmediatelyBuy(value)) {
            bool ok = runIfConfirmed(action);
            if (!ok) {
                return false;
            }
        }  else {
            action();
        }
    }
    return true;
}

bool SpreaderMainWindow::updateATMinSpreadSell(double value)
{
    if( !m_spreadAutoTrader1 || !m_spreadAutoTrader1->isStarted()) {
        ui->doubleSpinBoxAutoTrading_MinSpreadSell->setValue(value);
    }

    if( m_spreadAutoTrader1 && m_spreadAutoTrader1->isStarted()) {

        auto action = [=](){
            m_spreadAutoTrader1->updateMinSpreadSell(value);
            ui->doubleSpinBoxAutoTrading_MinSpreadSell->setValue(value);
        };

        if (m_spreadAutoTrader1->willTradeImmediatelySell(value)) {
            bool ok = runIfConfirmed(action);
            if (!ok) {
                return false;
            }
        }  else {
            action();
        }
    }
    return true;
}

void SpreaderMainWindow::configureLogger()
{
    QString debugLevel = m_Settings->getDebugLevel();
    QString logFile = m_Settings->getLogDirectory();

    const QString appVersion = QString("Eron's Scalp Machine v%1").arg(APP_VERSION);
    setWindowTitle(appVersion);

    // make the logFile absolute path
    if (!m_spreaderDirPath.isEmpty()) {
        logFile = QString("%1/%2").arg(m_spreaderDirPath).arg(logFile);
    }

    const QDateTime currentDateTime = QDateTime::currentDateTimeUtc();
    logFile = logFile.replace("{$date}",currentDateTime.date().toString("yyyyMMdd"));
    logFile = logFile.replace("{$time}",currentDateTime.time().toString("hhmmss"));
    logFile = logFile.replace("{$configname}", APP_VERSION);

    qDebug() << "Logfile: " << logFile;

    LOG4CXX_DECODE_CHAR(logFileName, QString("%1").arg(logFile).toStdString());
    LOG4CXX_DECODE_CHAR(logPattern, "%d %-5p %c - %m%n");

    log4cxx::FileAppender * fileAppender = new
    log4cxx::FileAppender(log4cxx::LayoutPtr(new log4cxx::PatternLayout(logPattern)), logFileName, false);

    log4cxx::ConsoleAppender * consoleAppender = new
    log4cxx::ConsoleAppender(log4cxx::LayoutPtr(new log4cxx::PatternLayout(logPattern)));

    log4cxx::helpers::Pool p;
    fileAppender->activateOptions(p);
    consoleAppender->activateOptions(p);

    log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(consoleAppender));

    m_logger->addAppender(fileAppender);
    m_logger->setLevel(Level::toLevel(debugLevel.toStdString()));
    m_TradeLogger->addAppender(fileAppender);
    m_TradeLogger->setLevel(Level::toLevel(debugLevel.toStdString()));
    m_IbErrorLogger->addAppender(fileAppender);
    m_IbErrorLogger->setLevel(Level::toLevel(debugLevel.toStdString()));

    Log::info(QString("Starting %1").arg(appVersion), m_logger);

    m_fileAppender = fileAppender;
}

void SpreaderMainWindow::on_pushButtonStartAutoTrading_clicked()
{
    UiContext context(m_spreadAutoTrader1, ui->pushButtonStartAutoTrading, ui->listWidgetAutoTraderLog, START_AUTOTRADER1, STOP_AUTOTRADER1);
    startStopAutoTrader(context);
}

void SpreaderMainWindow::on_pushButtonStartAutoTrader2_clicked()
{
    UiContext context(m_spreadAutoTrader2, ui->pushButtonStartAutoTrader2, ui->listWidgetAutoTraderLog_2, START_AUTOTRADER2, STOP_AUTOTRADER2);
    startStopAutoTrader(context);
}

void SpreaderMainWindow::on_pushButtonStartAutoTrader3_clicked()
{
    UiContext context(m_spreadAutoTrader3, ui->pushButtonStartAutoTrader3, ui->listWidgetAutoTraderLog_3, START_AUTOTRADER3, STOP_AUTOTRADER3);
    startStopAutoTrader(context);
}

void SpreaderMainWindow::on_pushButtonStartAutoTrader4_clicked()
{
    UiContext context(m_spreadAutoTrader4, ui->pushButtonStartAutoTrader4, ui->listWidgetAutoTraderLog_4, START_AUTOTRADER4, STOP_AUTOTRADER4);
    startStopAutoTrader(context);
}

void SpreaderMainWindow::on_doubleSpinBoxSpreadFormulaX_valueChanged(double arg1)
{
    m_spreadFormula.x = arg1;
}

void SpreaderMainWindow::on_doubleSpinBoxSpreadFormulaY_valueChanged(double arg1)
{
    m_spreadFormula.y = arg1;
}

void SpreaderMainWindow::logAlgoEvents(std::shared_ptr<SpreadAlgo> algo, QListWidget *widget)
{
    connect(algo.get(), &SpreadAlgo::limitOrderPlaced,[=](Order order, QString symbol){
        widget->addItem( QString("%0 [%1] Placed %2 %3 LMT %5 @ %6")
                                           .arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"))
                                           .arg(order.orderId)
                                           .arg(symbol)
                                           .arg(order.action)
                                           .arg(order.totalQuantity)
                                           .arg(order.lmtPrice));
    });
    connect(algo.get(), &SpreadAlgo::orderCancelSent,[=](OrderId id){
        widget->addItem( QString("%0 [%1] Order Cancel Sent")
                                           .arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"))
                                           .arg(id));
    });
    connect(algo.get(), &SpreadAlgo::orderCancelConfirmed,[=](OrderId id){
        widget->addItem( QString("%0 [%1] Order Cancel Confirmed")
                                           .arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"))
                                           .arg(id));
    });

    connect(algo.get(), &SpreadAlgo::limitOrderFilled, [=](OrderId id, int qty, double price){
        widget->addItem( QString("%0 [%1] LMT Order Filled %2 @ %3")
                                           .arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"))
                                           .arg(id)
                                           .arg(qty)
                                           .arg(price));
    });

    connect(algo.get(), &SpreadAlgo::mktOrderFilled, [=](OrderId id, int qty, double price){
        widget->addItem( QString("%0 [%1] MKT Order Filled %2 @ %3")
                                           .arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"))
                                           .arg(id)
                                           .arg(qty)
                                           .arg(price));
    });

    connect(algo.get(), &SpreadAlgo::marketOrderPlaced,[=](Order order, QString symbol){
        widget->addItem( QString("%0 [%1] Placed %2 %3 MKT %4")
                                           .arg(QDateTime::currentDateTime().toString("hh:mm:ss.zzz"))
                                           .arg(order.orderId)
                                           .arg(symbol)
                                           .arg(order.action)
                                           .arg(order.totalQuantity)
                                           );
    });
}

void SpreaderMainWindow::on_pushButton_restart_clicked()
{
    emit restart(this);
}
