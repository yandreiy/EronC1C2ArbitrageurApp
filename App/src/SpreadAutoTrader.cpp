#include "SpreadAutoTrader.h"
#include "log_utils.h"
#include <QDateTime>
#include <QUuid>
#include <QTimer>
#include "Order.h"

SpreadAutoTrader::SpreadAutoTrader(std::shared_ptr<SpreadAlgo> algo1, std::shared_ptr<SpreadAlgo> algo2, FileAppender *appender, QObject *parent)
        : QObject(parent)
        , m_openedPositionsC2(0)
        , m_algo1(algo1)
        , m_algo2(algo2)
        , m_state(State::Idle)
        , m_ibClient( IBClient::ptrInstance())
        , m_closeOrderIdC1(0)
        , m_closeOrderIdC2(0)
        , m_appender(appender)
{
    connect( m_algo1.get(), &SpreadAlgo::closeSpreadLevelReached, this, &SpreadAutoTrader::closeAllOpenedPositions);
    connect( m_algo2.get(), &SpreadAlgo::closeSpreadLevelReached, this, &SpreadAutoTrader::closeAllOpenedPositions);

    connect( m_algo1.get(), &SpreadAlgo::spreadPriceDropped, this, &SpreadAutoTrader::onSpreadPriceDropped);
    connect( m_algo2.get(), &SpreadAlgo::spreadPriceDropped, this, &SpreadAutoTrader::onSpreadPriceDropped);

    m_algoManager1.reset(new AlgoManager(m_ibClient));
    m_algoManager2.reset(new AlgoManager(m_ibClient));

    m_algoManager1->set_algo(m_algo1);
    m_algoManager2->set_algo(m_algo2);

    connect(m_algoManager1.get(), SIGNAL(finished(SpreadAlgo::TradeExecutionResult)),
            this, SLOT(onAlgo1Finished(SpreadAlgo::TradeExecutionResult)));

    connect(m_algoManager2.get(), SIGNAL(finished(SpreadAlgo::TradeExecutionResult)),
            this, SLOT(onAlgo2Finished(SpreadAlgo::TradeExecutionResult)));


    connect(m_algoManager1.get(), &AlgoManager::paused,[=](int sec){
        QString msg = QString("Paused algo1 for: %1 sec.").arg(sec);
        Log::info(msg, m_autoTraderLogger);
        emit logAction( msg );
    });
    connect(m_algoManager2.get(), &AlgoManager::paused,[=](int sec){
        QString msg = QString("Paused algo2 for: %1 sec.").arg(sec);
        Log::info(msg, m_autoTraderLogger);
        emit logAction( msg );
    });

    qRegisterMetaType<AlgoParameters>("AlgoParameters");
}

void SpreadAutoTrader::start(const SpreadAutoTrader::Parameters &params)
{
    m_state = State::Started;
    logStartInfo(params);

    m_params = params;
    m_openedPositionsC2 = 0;
    m_openedPositionsC1 = 0;
    m_timesExecuted = 0;

    if (params.algoParams1.qty2 == 0) {
        m_maxAllowedQtyC2 = params.algoParams1.qty1 * params.maxPositionsC2;
    } else {
        m_maxAllowedQtyC2 = params.algoParams1.qty2 * params.maxPositionsC2;
    }

    m_isSkewed1 = m_isSkewed2 = false;

    m_algo1->setSpreadLevelNotifier( m_params.closeALlOutsideSpreadInterval);
    m_algo2->setSpreadLevelNotifier( m_params.closeALlOutsideSpreadInterval);

    m_algoManager1->start(params.algoParams1);
    m_algoManager2->start(params.algoParams2);

    QString msg = QString("%1, Started Auto-trader!\n%2\n%3\n%4\n%5\n%6\n%7\n")
                    .arg(QDateTime::currentDateTime().toString("yy/MM/dd hh:mm:ss"))
                    .arg("Type: " + Parameters::typeToString(m_params.type))
                    .arg(QString("TrailingDelta: %1").arg(m_params.trailingDelta))
                    .arg(QString("SkewDelta: %1").arg(m_params.skewDelta))
                    .arg(QString("minDeltaDiffLimitAndAsk: %1").arg(m_params.algoParams1.minDeltaBidAsk,0,'f',2))
                    .arg(QString("Buying spread (%1, %2) @ %3").arg(m_params.algoParams1.qty1).arg(m_params.algoParams1.qty2).arg(m_params.algoParams1.minSpread,0,'f',2))
                    .arg(QString("Selling spread (%1, %2) @ %3").arg(m_params.algoParams2.qty1).arg(m_params.algoParams2.qty2).arg(m_params.algoParams2.minSpread,0,'f',2))
                    ;

    emit logAction(msg);
}

void SpreadAutoTrader::stop()
{
    m_state = State::StoppedByUser;

    m_algoManager1->stop();
    m_algoManager2->stop();
}

void SpreadAutoTrader::widenSpreadsBy(double value)
{
    if ( qFuzzyCompare( value, 1.0)) {
        return;
    }

    double s1 = m_params.algoParams1.minSpread;
    double s2 = m_params.algoParams2.minSpread;
    double delta = qAbs(s1-s2) * (value-1)/2;

//    if (delta > qAbs(m_params.algoParams1.minSpread)) {
//        return;
//    }

    m_params.algoParams1.minSpread -= delta;
    m_params.algoParams2.minSpread += delta;

    m_algo1->updateMinSpread( m_params.algoParams1.minSpread);
    m_algo2->updateMinSpread( m_params.algoParams2.minSpread);

    emit updatedMinSpreads( m_params.algoParams1.minSpread, m_params.algoParams2.minSpread);


    QString msg = QString("%1\n%2\n%3")
                    .arg(QString("Widened by %1").arg(value))
                    .arg(QString("Buying spread (%1, %2) @ %3").arg(m_params.algoParams1.qty1).arg(m_params.algoParams1.qty2).arg(m_params.algoParams1.minSpread,0,'f',3))
                    .arg(QString("Selling spread (%1, %2) @ %3").arg(m_params.algoParams2.qty1).arg(m_params.algoParams2.qty2).arg(m_params.algoParams2.minSpread,0,'f',3))
                    ;

    logAction(msg);
}

void SpreadAutoTrader::widenSkewDeltaBy(double value)
{
    m_params.skewDelta *= 2;

    QString msg = QString("%1\n%2\n")
                    .arg(QString("Widened by %1").arg(value))
                    .arg(QString("SkewDelta: %1").arg(m_params.skewDelta))
                    ;
    logAction(msg);
}

void SpreadAutoTrader::widenTrailDeltaBy(double value)
{
    m_params.trailingDelta *= value;

    QString msg = QString("%1\n%2\n")
                    .arg(QString("Widened by %1").arg(value))
                    .arg(QString("TrailingDelta: %1").arg(m_params.trailingDelta))
                    ;
    logAction(msg);
}

void SpreadAutoTrader::tightenSpreadsBy(double value)
{
    if ( qFuzzyCompare( value, 1.0)) {
        return;
    }

    double s1 = m_params.algoParams1.minSpread;
    double s2 = m_params.algoParams2.minSpread;

    double t = (value-1)/2;
    m_params.algoParams1.minSpread = (s1*(1+t) + s2*t) / (1+2*t);
    m_params.algoParams2.minSpread = s1 + s2 - m_params.algoParams1.minSpread;

    m_algo1->updateMinSpread( m_params.algoParams1.minSpread);
    m_algo2->updateMinSpread( m_params.algoParams2.minSpread);
    emit updatedMinSpreads( m_params.algoParams1.minSpread, m_params.algoParams2.minSpread);

    QString msg = QString("%1\n%2\n%3\n")
                    .arg(QString("Tightened by %1").arg(value))
                    .arg(QString("Buying spread (%1, %2) @ %3").arg(m_params.algoParams1.qty1).arg(m_params.algoParams1.qty2).arg(m_params.algoParams1.minSpread,0,'f',3))
                    .arg(QString("Selling spread (%1, %2) @ %3").arg(m_params.algoParams2.qty1).arg(m_params.algoParams2.qty2).arg(m_params.algoParams2.minSpread,0,'f',3))
                    ;

    logAction(msg);
}

void SpreadAutoTrader::tightenSkewDeltaBy(double value)
{
    if ( qFuzzyCompare( value, 1.0)) {
        return;
    }

    m_params.skewDelta /= value;

    QString msg = QString("%1\n%2\n")
                    .arg(QString("Tightened by %1").arg(value))
                    .arg(QString("SkewDelta: %1").arg(m_params.skewDelta))
                    ;
    logAction(msg);
}

void SpreadAutoTrader::tightenTrailDeltaBy(double value)
{
    if ( qFuzzyCompare( value, 1.0)) {
        return;
    }

    m_params.trailingDelta /= value;

    QString msg = QString("%1\n%2\n")
                    .arg(QString("Tightened by %1").arg(value))
                    .arg(QString("TrailingDelta: %1").arg(m_params.trailingDelta))
                    ;
    logAction(msg);
}

bool SpreadAutoTrader::wasPriceDropp()
{
    return m_wasPriceDrop;
}

void SpreadAutoTrader::updateMinSpreadBuy(double value)
{
    if (!qFuzzyCompare(value, m_params.algoParams1.minSpread)) {
        m_algo1->updateMinSpread(value);
        m_params.algoParams1.minSpread = value;
        QString msg = QString("Updated Min Spread Buy: %1").arg(value);
        Log::info(msg, m_autoTraderLogger);
        emit logAction(msg);
    }
}

void SpreadAutoTrader::updateMinSpreadSell(double value)
{
    if (!qFuzzyCompare(value, m_params.algoParams2.minSpread)) {
        m_algo2->updateMinSpread(value);
        m_params.algoParams2.minSpread = value;
        QString msg = QString("Updated Min Spread Sell: %1").arg(value);
        Log::info(msg, m_autoTraderLogger);
        emit logAction(msg);
    }
}

bool SpreadAutoTrader::willTradeImmediatelyBuy(double minSpread)
{
    if (m_algo1->willTradeImmediately(AlgoSide::Buy,
                                      minSpread,
                                      m_params.algoParams1.spreadFormula))
    {
        return true;
    }
    return false;
}

bool SpreadAutoTrader::willTradeImmediatelySell(double minSpread)
{
    if (m_algo2->willTradeImmediately(AlgoSide::Sell,
                                      minSpread,
                                      m_params.algoParams2.spreadFormula))
    {
        return true;
    }
    return false;

}

void SpreadAutoTrader::onAlgo1Finished(SpreadAlgo::TradeExecutionResult result)
{
    Log::info( QString("Algo1 finished with result: %1").arg(result.toString()), m_autoTraderLogger);

    m_algo1Result = result;

    m_openedPositionsC1 += result.qty1;
    m_openedPositionsC2 -= result.qty2;

    if (m_algo2->isIdle()) {
        onBothAlgosFinished();
    } else {
        Log::info( "Stopping Algo2", m_autoTraderLogger);
        m_algoManager2->stop();
    }
}

void SpreadAutoTrader::onAlgo2Finished(SpreadAlgo::TradeExecutionResult result)
{
    Log::info( QString("Algo2 finished with result: %1").arg(result.toString()), m_autoTraderLogger);

    m_algo2Result = result;

    m_openedPositionsC1 -= result.qty1;
    m_openedPositionsC2 += result.qty2;

    if (m_algo1->isIdle()) {
        onBothAlgosFinished();
    } else {
        Log::info( "Stopping Algo1", m_autoTraderLogger);
        m_algoManager1->stop();
    }
}

void SpreadAutoTrader::closeAllOpenedPositions()
{
    if (m_state == State::CloseAllOpened || m_state == State::StoppedBySpreadDropp) {
        return;
    }
    m_state = State::CloseAllOpened;

    m_algoManager1->stop();
}

void SpreadAutoTrader::onSpreadPriceDropped(double spread)
{
    m_wasPriceDrop = true;
    if (m_state == State::StoppedBySpreadDropp || m_state == State::CloseAllOpened) {
        return;
    }
    m_state = State::StoppedBySpreadDropp;

    m_algoManager1->stop();
}

void SpreadAutoTrader::doStopAndNotify(QString reason)
{
    QString msg = QString("Stopped. Reason: %1").arg(reason);
    Log::info(msg, m_autoTraderLogger);
    emit logAction( msg );

    m_state = State::Idle;
    emit finished();
}

void SpreadAutoTrader::onBothAlgosFinished()
{
    if (m_state == State::StoppedBySpreadDropp) {
        doStopAndNotify("Spread value dropped too quickly!");
        return;
    }

    if (m_state == State::CloseAllOpened) {
        sendCloseMktOrders();
        doStopAndNotify("Closed All. Spread value outside interval!");
        return;
    }

    if (m_state == State::StoppedByUser) {
        doStopAndNotify("Stopped By User!");
        return;
    }

    if (m_state == State::Idle) {
        return;
    }

    Log::info( "Both algo finished. Checking next round!", m_autoTraderLogger);

    QTimer::singleShot( m_params.freezeTimeMsec ,Qt::PreciseTimer, [=]() {

        if (m_state != State::Started) {
            Log::info("Can't continue round, the trader was stopped!", m_autoTraderLogger);
            emit logAction(QString("Stopped before new round!"));
            return;
        }

        double spread = updateTimesAndgetTradedSpread();
        logRoundFinished(spread);

        if (!maxPositionsLimitReached() ) {
            startAlgosNewRound(spread);
        }
        else {
            handlePositionsLimitReached(spread);
        }
    });
}

void SpreadAutoTrader::handlePositionsLimitReached(double spread)
{
    if (m_timesExecuted > 0) {
        Log::info("Positions Limit Reached BUY! Starting SELL algo!");

        auto params = getNewParametersAlgo2(spread);
        m_algoManager2->start(params);

        emit logAction( QString("BUY Limit Reached! Start Sell Spread (%1,%2) @ %3")
                        .arg(params.qty1)
                        .arg(params.qty2)
                        .arg(params.minSpread));

    } else if (m_timesExecuted < 0) {
        Log::info("Positions Limit Reached SELL! Starting BUY algo!");
        auto params = getNewParametersAlgo1(spread);

        m_algoManager1->start(params);
        emit logAction( QString("SELL Limit Reached! Start Buy Spread (%1,%2) @ %3")
                        .arg(params.qty1)
                        .arg(params.qty2)
                        .arg(params.minSpread));
    }
    else {
        Log::info(QString("Limits reached, but timesExecuted = %1").arg(m_timesExecuted), m_autoTraderLogger);
    }
}

void SpreadAutoTrader::sendCloseMktOrders()
{
    Log::info(QString("Opened positions: %1, %2. Closing all!")
              .arg(m_openedPositionsC1)
              .arg(m_openedPositionsC2),
              m_autoTraderLogger);

    if ( m_openedPositionsC1 != 0) {
        auto c1 = m_algo1->getContract1();

        Order order;
        order.action = ( m_openedPositionsC1 < 0 ? "BUY" : "SELL");
        order.orderType = "MKT";
        order.totalQuantity = qAbs(m_openedPositionsC1);
        m_closeOrderIdC1 = m_ibClient->placeOrder(c1, order);

        auto symbol = c1.symbol;
        if (symbol == "USD") {
            symbol = c1.localSymbol;
        }
        Log::info(QString("[%1]Placed MKT order %2, %3, for qty %4")
                  .arg(m_closeOrderIdC1)
                  .arg(order.action)
                  .arg(symbol)
                  .arg(order.totalQuantity)
                  ,m_autoTraderLogger);
    }

    if ( m_openedPositionsC2 != 0) {
        auto contract = m_algo1->getContract2();

        auto symbol = contract.symbol;
        if (symbol == "USD") {
            symbol = contract.localSymbol;
        }

        Order order;
        order.action = ( m_openedPositionsC2 < 0 ? "BUY" : "SELL");
        order.orderType = "MKT";
        order.totalQuantity = qAbs(m_openedPositionsC2);

        m_closeOrderIdC2 = m_ibClient->placeOrder(contract, order);

        Log::info(QString("[%1]Placed MKT order %2, %3, for qty %4")
                  .arg(m_closeOrderIdC2)
                  .arg(order.action)
                  .arg(symbol)
                  .arg(order.totalQuantity)
                  ,m_autoTraderLogger);
    }
}


bool SpreadAutoTrader::maxPositionsLimitReached()
{
    if ( m_params.algoParams1.qty2 == 0) {
        return qAbs( m_openedPositionsC1 ) >= m_maxAllowedQtyC2; // to do: refactor
    }

    return qAbs( m_openedPositionsC2 ) >= m_maxAllowedQtyC2;
}

AlgoParameters SpreadAutoTrader::getNewParametersAlgo2(double tradedSpreadLevel)
{
    AlgoParameters paramsAlgo2 = m_params.algoParams2;
    double delta = m_isSkewed2 ? m_params.skewDelta : m_params.trailingDelta;
    paramsAlgo2.minSpread = tradedSpreadLevel + delta;

    if (m_params.type == Parameters::Type::Aggressive) {
        if (m_timesExecuted > 0) {
            paramsAlgo2.qty1 += m_params.algoParams2.qty1 * qAbs(m_timesExecuted );
            paramsAlgo2.qty2 += m_params.algoParams2.qty2 * qAbs( m_timesExecuted );
        }
    }

    if (m_params.type == Parameters::Type::Neutral) {
        if (m_timesExecuted > 0) {
            paramsAlgo2.qty1 =  m_params.algoParams2.qty1 * qAbs(m_timesExecuted );
            paramsAlgo2.qty2 =  m_params.algoParams2.qty2 * qAbs(m_timesExecuted );
        }
    }

    return paramsAlgo2;
}

AlgoParameters SpreadAutoTrader::getNewParametersAlgo1(double tradedSpreadLevel)
{
    AlgoParameters paramsAlgo1 = m_params.algoParams1;
    double delta = m_isSkewed1 ? m_params.skewDelta : m_params.trailingDelta;
    paramsAlgo1.minSpread = tradedSpreadLevel - delta;

    if (m_params.type == Parameters::Type::Aggressive) {
        if (m_timesExecuted < 0) {
            paramsAlgo1.qty1 += m_params.algoParams1.qty1 * qAbs(m_timesExecuted);
            paramsAlgo1.qty2 += m_params.algoParams1.qty2 * qAbs(m_timesExecuted);
        }
    }

    if (m_params.type == Parameters::Type::Neutral) {
        if (m_timesExecuted < 0) {
            paramsAlgo1.qty1 = m_params.algoParams1.qty1 * qAbs(m_timesExecuted );
            paramsAlgo1.qty2 = m_params.algoParams1.qty2 * qAbs(m_timesExecuted );
        }
    }

    return paramsAlgo1;
}

void SpreadAutoTrader::startAlgosNewRound(double tradedSpreadLevel)
{
    AlgoParameters paramsAlgo1 = getNewParametersAlgo1(tradedSpreadLevel);
    AlgoParameters paramsAlgo2 = getNewParametersAlgo2(tradedSpreadLevel);

    logStartNextRound(paramsAlgo1, paramsAlgo2);

    m_algoManager1->start(paramsAlgo1);
    m_algoManager2->start(paramsAlgo2);

    // update original params
    m_params.algoParams1 = paramsAlgo1;
    m_params.algoParams2 = paramsAlgo2;

    emit startedNewRound(paramsAlgo1, paramsAlgo2);
}

double SpreadAutoTrader::updateTimesAndgetTradedSpread()
{
    double spread = 0;

    if ( m_algo1Result.hasTraded() && m_algo2Result.hasTraded()) {
        spread = qMin(
                                m_params.algoParams1.spreadFormula.calculateSpread(m_algo1Result.price1, m_algo1Result.price2),
                                m_params.algoParams1.spreadFormula.calculateSpread(m_algo2Result.price1, m_algo2Result.price2)
                                );
        Log::info( QString("Both algo have traded, continue with min spread: %1").arg(spread), m_autoTraderLogger);

        m_isSkewed1 = m_isSkewed2 = false;
        m_timesExecuted += (m_algo1Result.qty1) / m_params.algoParams1.qty1;
        m_timesExecuted -= (m_algo2Result.qty1) / m_params.algoParams2.qty1;
    }
    else if (m_algo1Result.hasTraded()) {

        if (m_algo1Result.qty2 == 0) {
            spread = m_algo1Result.price1;
        } else {
            spread = m_params.algoParams1.spreadFormula.calculateSpread(m_algo1Result.price1, m_algo1Result.price2);
        }

        Log::info( QString("Algo1 traded @ %1").arg(spread,0,'f',2), m_autoTraderLogger);
        m_timesExecuted += (m_algo1Result.qty1) / m_params.algoParams1.qty1;

        m_isSkewed1 = false;
        m_isSkewed2 = true;

    }
    else if (m_algo2Result.hasTraded()) {

        if (m_algo2Result.qty2 == 0) {
            spread = m_algo2Result.price1;
        } else {
            spread = m_params.algoParams1.spreadFormula.calculateSpread(m_algo2Result.price1, m_algo2Result.price2);
        }

        Log::info( QString("Algo2 traded @ %1").arg(spread,0,'f',2), m_autoTraderLogger);

        m_timesExecuted -= (m_algo2Result.qty1) / m_params.algoParams2.qty1;

//        if (m_isSkewed2) {
//            m_isSkewed2 = false;
//        }
//        else {
//            m_isSkewed1 = true;
//        }
        m_isSkewed1 = true;
        m_isSkewed2 = false;

    }
    else {
        Q_ASSERT ( false );
    }

    return spread;
}

void SpreadAutoTrader::logStartInfo(const SpreadAutoTrader::Parameters &params)
{
    QString id = QUuid::createUuid().toString();
    m_autoTraderLogger = log4cxx::Logger::getLogger( QString("[Auto-Trader_%1]").arg(id).toStdString());
    m_autoTraderLogger->addAppender(m_appender);

    Log::info( "Auto-trader started!", m_autoTraderLogger);
    QString desc = QString("Algo1 Contract: %1 - %2\nAlgo2 Contract: %3 - %4")
                    .arg(m_algo1->getContract1().symbol)
                    .arg(m_algo1->getContract2().symbol)
                    .arg(m_algo2->getContract1().symbol)
                    .arg(m_algo2->getContract2().symbol)
                    ;

    Log::info(desc, m_autoTraderLogger);
    Log::info( QString("Params: %1").arg(params.toString()), m_autoTraderLogger);
}

void SpreadAutoTrader::logRoundFinished(double spread)
{
    QString msg = QString("%1, Round finished!\nTraded spread: %2\nTotalOpenedPositions C1: %3, C2: %4")
                    .arg(QDateTime::currentDateTime().toString("yy/MM/dd hh:mm:ss"))
                    .arg(spread)
                    .arg(m_openedPositionsC1)
                    .arg(m_openedPositionsC2);

    emit logAction(msg);
}

void SpreadAutoTrader::logStartNextRound(AlgoParameters params1, AlgoParameters params2)
{
    QString msg = QString("Starting next round, Buy spread(%1,%2) @ %3, Sell spread (%4,%5) @ %6")
                    .arg(params1.qty1)
                    .arg(params1.qty2)
                    .arg(params1.minSpread)
                    .arg(params2.qty1)
                    .arg(params2.qty2)
                    .arg(params2.minSpread);
    Log::info( msg, m_autoTraderLogger);
    emit logAction(msg);
}
