#include "AddSpreaderDialog.h"
#include "ui_AddSpreaderDialog.h"

AddSpreaderDialog::AddSpreaderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddSpreaderDialog)
{
    ui->setupUi(this);
}

AddSpreaderDialog::~AddSpreaderDialog()
{
    delete ui;
}

QStringList AddSpreaderDialog::getLines()
{
   return ui->textEdit->toPlainText().split(QRegExp("\n|\r\n|\r"), QString::SkipEmptyParts);
}
