#include "SpreadAlgo.h"
#include "log_utils.h"
#include "Execution.h"
#include "OrderState.h"
#include <QDateTime>
#include <QThread>
#include <QUuid>

SpreadAlgo::SpreadAlgo(IBClient *ib, Settings *settings, const Contract &c1, const Contract &c2, FileAppender *appender, QObject *parent)
    : QObject(parent)
    , m_ibClient(ib)
    , m_tickerId1(-1)
    , m_tickerId2(-1)
    , m_c1(c1)
    , m_c2(c2)
    , m_orderIdC1(-1)
    , m_state(State::Idle)
    , m_hasSpreadLevelProtection(false)
    , m_tickSizeC1(0.0)
    , m_tickSizeC2(0.0)
    , m_settings(settings)
    , m_appender(appender)
{
    qRegisterMetaType<Order>("Order");
    qRegisterMetaType<OrderId>("OrderId");
    qRegisterMetaType<SpreadAlgo::TradeExecutionResult>("SpreadAlgo::TradeExecutionResult");
}

void SpreadAlgo::init()
{
    subscribeToMktData();
    setupIB();
}

void SpreadAlgo::subscribeToMktData()
{
    m_tickerId1 = m_ibClient->reqMktData( m_c1, "221", false);
    m_tickerId2 = m_ibClient->reqMktData( m_c2, "221", false);

    Log::info(QString("[SpreadAlgo] Subscribed to market data with id's %1 and %2")
              .arg(m_tickerId1)
              .arg(m_tickerId2));
    Log::info(m_c1.localSymbol +"," + m_c2.localSymbol);
    Log::info(m_c1.symbol +"," + m_c2.symbol);
}

void SpreadAlgo::setupIB()
{
//    connect(m_ibClient, SIGNAL(tickPriceSig(TickerId,TickType,double,int)),
//                                  SLOT(tickPrice(TickerId,TickType,double,int)));

    connect(m_ibClient, SIGNAL(execDetailsSig(int,Contract,Execution)), SLOT(execDetails(int,Contract,Execution)), Qt::UniqueConnection );

    connect(m_ibClient, SIGNAL(orderStatusSig(OrderId,IBString,int,int,double,int,int,double,int,IBString)),
                 SLOT(orderStatus(OrderId,IBString,int,int,double,int,int,double,int,IBString)) , Qt::UniqueConnection);
}

double SpreadAlgo::getTickSizeC1()
{
    return m_settings->getTickSizeContract1();
}

double SpreadAlgo::getTickSizeC2()
{
    return m_settings->getTickSizeContract2();
}

QString SpreadAlgo::getTradingAccount()
{
    return m_settings->tradingAccount();
}

bool SpreadAlgo::start(const AlgoParameters &parameters)
{
    if (m_state != State::Idle) {
        return false;
    }

    m_state = State::Started;
    m_params = parameters;

    m_executions.reset();
    m_totalFilledC1 = m_totalFilledC2 = 0;

    m_spreadBuffer.reset(new CircularBuffer<double>(m_params.protectionPriceDrop.first));
    m_tickSizeC1 = getTickSizeC1();
    m_tickSizeC2 = getTickSizeC2();

    m_algoId = QUuid::createUuid().toString();
    m_algoLogger = log4cxx::Logger::getLogger(QString("[SpreadAlgo_%1]").arg(m_algoId).toStdString());
    m_algoLogger->addAppender(m_appender);

    Log::info("Trading started", m_algoLogger);
    Log::info(QString("Params: %1; TickSize1=%2; TickSize2=%3")
              .arg(m_params.toString())
              .arg(m_tickSizeC1)
              .arg(m_tickSizeC2)
              , m_algoLogger);

    auto symbol1 = m_c1.symbol;
    if (symbol1 == "USD") {
        symbol1 = m_c1.localSymbol;
    }
    auto symbol2 = m_c2.symbol;
    if (symbol2 == "USD") {
        symbol2 = m_c2.localSymbol;
    }

    Log::info( QString("Contracts: %1, %2").arg(symbol1).arg(symbol2), m_algoLogger);

    if (m_params.side == AlgoSide::Buy) {
        Log::info(QString("[Current Price] %1,%2,%3").arg("F1").arg("BID").arg(m_tickC1.bidPrice), m_algoLogger);
        if (!m_params.qty2_Negative) {
            Log::info(QString("[Current Price] %1,%2,%3").arg("F2").arg("BID").arg(m_tickC2.bidPrice), m_algoLogger);
        } else {
            Log::info(QString("[Current Price] %1,%2,%3").arg("F2").arg("ASK").arg(m_tickC2.askPrice), m_algoLogger);
        }
    }
    else {
        Log::info(QString("[Current Price] %1,%2,%3").arg("F1").arg("ASK").arg(m_tickC1.askPrice), m_algoLogger);
        if (!m_params.qty2_Negative) {
            Log::info(QString("[Current Price] %1,%2,%3").arg("F2").arg("ASK").arg(m_tickC2.askPrice), m_algoLogger);
        } else {
            Log::info(QString("[Current Price] %1,%2,%3").arg("F2").arg("BID").arg(m_tickC2.bidPrice), m_algoLogger);
        }
    }

    evaluateCurrentPrices();
    return true;
}

void SpreadAlgo::evaluateCurrentPrices()
{
    double price1 = m_params.side == AlgoSide::Buy ? m_tickC1.bidPrice : m_tickC1.askPrice;

    if ( qFuzzyCompare(price1, 0.0)) {
        return;
    }

    double spread;
    if ( hasSpread( m_tickerId1, price1, spread)) {
        processNewPrice(m_tickerId1, spread);
    }
}

void SpreadAlgo::updateTick(TickerId tickerId, TickType field, double price)
{
    if (m_tickerId1 == tickerId) {
        if (field == BID) {
            m_tickC1.bidPrice = price;
        }
        if (field == ASK) {
            m_tickC1.askPrice = price;
        }
    } else if (m_tickerId2 == tickerId) {
        if (field == BID) {
            m_tickC2.bidPrice = price;
        }
        if (field == ASK) {
            m_tickC2.askPrice = price;
        }
    }
}

bool SpreadAlgo::checkSpreadDropConditions(double spread)
{
    bool spreadDropped = false;

    if (!m_spreadBuffer->isEmpty()) {

        double diffPrev = qAbs(  spread - m_spreadBuffer->tail()) / m_tickSizeC1;
        double diff = qAbs( spread - m_spreadBuffer->head() ) / m_tickSizeC1;

        if ( diffPrev > m_params.protectionPriceDrop.second  || diff > m_params.protectionPriceDrop.second ) {
           spreadDropped = true;
        }
    }

    m_spreadBuffer->add(spread);

    return spreadDropped;
}

double SpreadAlgo::getNormalizedPrice(double tickSize, double price)
{
    return qRound(price / tickSize + 10*FLT_EPSILON) * tickSize;
}

bool SpreadAlgo::hasActiveOrder() const
{
    if( m_state == State::Started || m_state == State::Idle) {
        return false;
    }
    return true;
}

void SpreadAlgo::tickPrice(TickerId tickerId, TickType field, double price, int /*canAutoExecute*/)
{
    /// Ignore what we don't need
    if (m_tickerId1 != tickerId && m_tickerId2 != tickerId) {
        return;
    }

    if (field != BID && field != ASK) {
        return;
    }

    updateTick(tickerId, field, price);

    if (m_state == State::Idle) {
        return;
    }

    if (m_params.qty2_Negative) {

        // need to use Bid1, Ask2 OR Ask1, Bid2
        if (m_params.side == AlgoSide::Buy) {
            if (tickerId == m_tickerId1 && field != BID) {
                return;
            }
            if (tickerId == m_tickerId2 && field != ASK) {
                return;
            }
        }
        if (m_params.side == AlgoSide::Sell) {
            if (tickerId == m_tickerId1 && field != ASK) {
                return;
            }
            if (tickerId == m_tickerId2 && field != BID) {
                return;
            }
        }

    }  else  {

        if (m_params.side == AlgoSide::Buy && field != BID) {
            return;
        }
        if (m_params.side == AlgoSide::Sell && field != ASK) {
            return;
        }
    }

    if (m_params.qty2 == 0 && m_tickerId2 == tickerId){
        return;
    }

    logPrice(tickerId, field, price );

    double spread=0.0;
    if (!hasSpread(tickerId, price, spread)) {
        return;
    }

    // For all except BAG, if any price = -1, don't process!
    if ( m_c1.secType != "BAG" ) {
        bool isMinusOne =  (
            qFuzzyCompare(m_tickC1.bidPrice,-1.0) || qFuzzyCompare(m_tickC1.askPrice, -1.0) ||
             qFuzzyCompare(m_tickC2.bidPrice,-1.0) || qFuzzyCompare(m_tickC2.askPrice, -1.0)
         );
        if (isMinusOne) {
            Log::info(QString("Skipping. Bid/Ask = -1. Invalid price!"), m_algoLogger);
            return;
        }

        bool isZero =  (
            qFuzzyCompare(m_tickC1.bidPrice,0.0) || qFuzzyCompare(m_tickC1.askPrice, 0.0) ||
             qFuzzyCompare(m_tickC2.bidPrice,0.0) || qFuzzyCompare(m_tickC2.askPrice, 0.0)
        );
        if (isZero) {
            Log::info(QString("Skipping. Bid/Ask = 0. Invalid price!"), m_algoLogger);
            return;
        }
    }

    if (checkSpreadDropConditions(spread)) {
        Log::info(QString("Spread dropped signal activated. Stopping! (Spread: %1)").arg(spread), m_algoLogger);
        emit spreadPriceDropped(spread);
        return;
    }

    if (m_hasSpreadLevelProtection) {
        if ( spread < m_closeOutsideSpreadLevel.first || spread > m_closeOutsideSpreadLevel.second) {
            Log::info( QString("Spread=%1 is in the close interval. Stopping trading! Send signal!").arg(spread), m_algoLogger);
            emit closeSpreadLevelReached(spread);
            return;  /// don't allow opening new trades
        }
    }

    processNewPrice(tickerId, spread);
}

void SpreadAlgo::processNewPrice(TickerId tickerId, double spread)
{
    switch(m_state) {
        case State::Started:
            checkEntrySignal(spread);
            break;
        case State::LimitOrderPlacedPending:
        case State::LimitOrderSubmitted:
            checkBeforeFillConditions(tickerId, spread);
            break;
    }
}

void SpreadAlgo::checkEntrySignal(double spread)
{
    bool shouldOpen = SpreadMonitor::isTargetReached(spread, m_params);

    if (shouldOpen) {
        tryPlaceLimitOrder();
    }
}

bool SpreadAlgo::hasSpread(TickerId tickerId, double price, double &spread)
{
    if (m_tickerId1 == tickerId) {
        double prevPrice2 = m_params.side == AlgoSide::Buy ? m_tickC2.bidPrice : m_tickC2.askPrice;

        if (!m_params.qty2_Negative) {
            // use reverse
            prevPrice2 = m_params.side == AlgoSide::Buy ? m_tickC2.askPrice : m_tickC2.bidPrice;
        }

        /// if prev price was not initialized yet
        if ( std::isnan(prevPrice2)) {
            Log::info("Skipping. prev price = 0");
            return false;
        }

        spread = m_params.spreadFormula.calculateSpread( price, prevPrice2);
    }
    else if (m_tickerId2 == tickerId) {
        double prevPrice1 = m_params.side == AlgoSide::Buy ? m_tickC1.bidPrice : m_tickC1.askPrice;

        /// if prev price was not initialized yet
        if (std::isnan(prevPrice1)) {
            Log::info("Skipping. prev price = 0");
            return false;
        }

        spread = m_params.spreadFormula.calculateSpread(prevPrice1, price);
    }

    return true;
}

double SpreadAlgo::getLimitPrice()
{
    /// formula is: minspread = x*C1 - y*C2
    /// limit price = (y*C2 + minSpread)/x

    double c2_price = m_params.side == AlgoSide::Buy ? m_tickC2.bidPrice : m_tickC2.askPrice;
    if (m_params.qty2_Negative) {
        c2_price = m_params.side == AlgoSide::Buy ? m_tickC2.askPrice : m_tickC2.bidPrice;
    }

    double price = m_params.spreadFormula.calculateLimitPrice( m_params.minSpread, c2_price);

    double priceNormalized = getNormalizedPrice(m_tickSizeC1, price);
    return priceNormalized;
}

void SpreadAlgo::sendCancelOrderToIB()
{
    m_ibClient->cancelOrder(m_orderIdC1);
}

void SpreadAlgo::checkBeforeFillConditions(TickerId tickerId, double spread)
{
    if (tickerId == m_tickerId1) {
        // do nothing for C1 price changes
        return;
    }

    if (tickerId == m_tickerId2) {

        // Q_ASSERT( m_state == State::LimitOrderSubmitted);

        bool isInTarget = SpreadMonitor::isTargetReached(spread, m_params);

        // Don't cancel if tickSizes are different, and the target limit price is the same as previous
        if ( !qFuzzyCompare(m_tickSizeC1, m_tickSizeC2)) {
            double price = getLimitPrice();
            if ( qFuzzyCompare(price, m_prevLimitPrice)) {
                Log::info("Prices are the same. Not cancelling!", m_algoLogger);
                return;
            }
        }

        if (isInTarget) {
            m_state = State::CancellingLimitAndPlaceNew;
            Log::info("State: CancellingLimitAndPlaceNew", m_algoLogger);
        } else {
            m_state = State::CancellingLimitAndRestart;
            Log::info("State: CancellingLimitAndRestart", m_algoLogger);
        }

        Log::info(QString("[OnPrice][%1]Cancel LMT order").arg(m_orderIdC1), m_algoLogger);
        sendCancelOrderToIB();
        emit orderCancelSent(m_orderIdC1);
    }
}

void SpreadAlgo::placeMktOrder(uint qty)
{
    Order order;
    order.action = (m_params.side == AlgoSide::Buy ? "SELL" : "BUY");

    if (m_params.qty2_Negative) {
        order.action = (m_params.side == AlgoSide::Buy ? "BUY" : "SELL");
    }


    order.orderType = "MKT";
    order.totalQuantity = qty;

    order.account = getTradingAccount();
    order.orderId = sendMktOrderToIB(order);

    Log::info(QString("[%1]Placed MKT order %2, %3, for qty %4").arg(order.orderId).arg(order.action).arg("F2").arg(order.totalQuantity), m_algoLogger);
    m_orderIdsC2.insert(order.orderId);

    auto symbol = m_c2.symbol;
    if (symbol == "USD") {
        symbol = m_c2.localSymbol;
    }
    emit marketOrderPlaced(order, symbol );
}

OrderId SpreadAlgo::sendMktOrderToIB(Order order)
{
    return m_ibClient->placeOrder(m_c2, order);
}

void SpreadAlgo::execDetails(int reqId, const Contract &contract, const Execution &execution)
{
    //    Log::info(QString("%1 execDetails: %2: %3 ")
    //              .arg(execution.orderId)
    //              .arg(contract.symbol + "_" + contract.localSymbol + "_" + contract.secType)
    //              .arg(execution.avgPrice)
    //              , m_algoLogger);

    if (m_c1.secType == "BAG" && contract.secType != "BAG") {
        Log::info("Ignored execution for C1 leg: " + contract.localSymbol, m_algoLogger);
        return;
    }

    if (m_c2.secType == "BAG" && contract.secType != "BAG") {
        Log::info("Ignored execution for C2 leg: " + contract.localSymbol, m_algoLogger);
        return;
    }

    if( execution.orderId == m_orderIdC1) {
        handleLimitOrderExecution(execution);
    }
    else if( m_orderIdsC2.contains(execution.orderId) ) {
        QString info = QString("[%1]Received execution for MKT Order: shares = %2, price = %3")
                        .arg(execution.orderId)
                        .arg(execution.shares)
                        .arg(execution.price)
                        ;
        Log::info(info, m_algoLogger);
        m_executions.addExecution("c2_" + m_c2.symbol+m_c2.localSymbol, execution.shares, execution.price);
    }
}

void SpreadAlgo::handleLimitOrderExecution(const Execution &execution)
{
    Log::info(QString("[%1]Execution for Lmt Received: shares = %2, price = %3")
              .arg(m_orderIdC1)
              .arg(execution.shares)
              .arg(execution.price),
              m_algoLogger);

    if (m_totalFilledC1 == m_params.qty1) {

        Log::info(QString("[%0] Already filled total qty1").arg(execution.orderId), m_algoLogger);
        return;
    }

    m_totalFilledC1 += execution.shares;

    emit limitOrderFilled(m_orderIdC1, execution.shares, execution.price);

    m_executions.addExecution( "c1_" + m_c1.symbol + m_c1.localSymbol, execution.shares, execution.price);

    if ( m_totalFilledC1 == m_params.qty1) {
        // Finished with C1 orders
        m_state = State::LimitOrderFilled;

        uint qty2 = getTradeQty2();

        if (qty2 > 0) {
            placeMktOrder(qty2);

        } else {

            if (m_orderIdsC2.empty()) {
                /// all done, algo can stop now
                m_state = State::MktOrderLastFilled;
                stop();
            }
            // or wait for MKT to finish
        }
    }
    else {
        handleLimitOrderExecutionPartialFill(execution);
    }
}

void SpreadAlgo::handleLimitOrderExecutionPartialFill(const Execution& execution)
{
    /// send the corresponding Mkt order
    uint qty2 = getTradeQty2();
    if (qty2 > 0) {
        placeMktOrder(qty2);
    }

    /// If I'm in the cancelling state, need to update the quantities and restart monitoring
    if (m_state == State::CancellingLimitAndPlaceNew || m_state == State::CancellingLimitAndRestart) {

        /// If no MKT order was sent, and we are in the cancelling state...leave the state unchanged
        if (qty2 > 0) {
            m_state = State::LimitOrderPartialFillWhileCanceled;
        }

        Log::info(QString("[%1]Partial Fill while cancelling!").arg(m_orderIdC1), m_algoLogger);
    }
    else {
        m_state = State::LimitOrderPartialFill;
        Log::info(QString("[%1]Partial Fill!").arg(m_orderIdC1), m_algoLogger);

        if (qty2 == 0 ) {
            m_state = State::LimitOrderSubmitted;
        }
    }
}

void SpreadAlgo::orderStatus(OrderId orderId, const IBString &status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId, const IBString &whyHeld)
{
    //Log::info(QString("orderStatus %1 avgprice=%2 lastfill=%3").arg(orderId).arg(avgFillPrice).arg(lastFillPrice),m_algoLogger);

    if ( orderId == m_orderIdC1 && status.contains("Submitted")) {
        if (m_state == State::LimitOrderPlacedPending) {
            Log::info(QString("[%1]Limit Order Submitted!").arg(m_orderIdC1), m_algoLogger);
            m_state = State::LimitOrderSubmitted;

            evaluateCurrentPrices(); /// check if any changes for prices in the meantime
        }
    }
    else
        if ( orderId == m_orderIdC1 ) {
            if (status.contains("Cancelled")) {
                onCancelledConfirmed();
                qDebug() << "Cancelled: " << status;
            }
        }
        else if( m_orderIdsC2.contains(orderId) && status == "Filled" && remaining == 0) {

            emit mktOrderFilled(orderId, filled, avgFillPrice);

            m_orderIdsC2.remove(orderId);
            if (!m_orderIdsC2.empty()) { /// waiting for other orders
                qDebug()<<"waiting for other MKT orders";
                return;
            }

            if (m_state == State::LimitOrderFilled) {

                QString info = QString("[%1]MKT Order Last filled @ %2").arg(orderId).arg(avgFillPrice);
                Log::info(info, m_algoLogger);

                m_state = State::MktOrderLastFilled;
                stop();
            }
            else if (m_state == State::LimitOrderPartialFillWhileCanceled) {
                restartAfterPartialFillAndCancel();
            }
            else if (m_state == State::LimitOrderPartialFill) {
                m_state = State::LimitOrderSubmitted;
            }
        }
}

void SpreadAlgo::onError(const int id, const int errorCode, const IBString &errorString)
{
}

void SpreadAlgo::onConnected()
{
    subscribeToMktData();
}

void SpreadAlgo::restartAfterPartialFillAndCancel()
{
    Log::info(QString("[%1] LMT Cancelled after Partial Fill, Continuing trading!")
              .arg(m_orderIdC1)
              , m_algoLogger);

    m_state = State::Started;
    evaluateCurrentPrices();
}

void SpreadAlgo::onCancelledConfirmed()
{
    QString msg = QString("[%1]LMT Order Cancelled!").arg(m_orderIdC1);
    Log::info(msg, m_algoLogger);

    emit orderCancelConfirmed(m_orderIdC1);

    switch(m_state) {
    case State::CancellingLimitAndRestart:
        Log::info(QString("[%1] LMT Cancelled, restart monitoring!").arg(m_orderIdC1), m_algoLogger);
        m_state = State::Started;
        break;
    case State::CancellingLimitAndPlaceNew:
        Log::info(QString("[%1] LMT Cancelled, placing new one!").arg(m_orderIdC1), m_algoLogger);
        tryPlaceLimitOrder();
        break;
    case State::StoppingTrading:
        m_state = State::Idle;
        stop();
        break;

    case State::LimitOrderPartialFill:
        /// A partial fill happened, so we need to restart trading with updated quantities
        restartAfterPartialFillAndCancel();
        break;
    }
}

void SpreadAlgo::tryPlaceLimitOrder()
{
    double deltaBidAsk = m_tickC1.askPrice - m_tickC1.bidPrice;
    if (  deltaBidAsk > m_params.minDeltaBidAsk ) {
        QString msg = QString("Not Placing Lmt order, Difference between Bid & Ask (%1) is bigger then minDelta(%2)").arg(deltaBidAsk).arg(m_params.minDeltaBidAsk);
        Log::info( msg, m_algoLogger);
        m_state = State::Started;
        return;
    }

    placeLimitOrder();
}

void SpreadAlgo::placeLimitOrder()
{
    double lmtPrice = getLimitPrice();

    Order order;
    order.action = (m_params.side == AlgoSide::Buy ? "BUY" : "SELL");
    order.orderType = "LMT";
    order.lmtPrice = lmtPrice;
    if (m_c1.secType != "BAG") {
        order.minQty = 1;
    }
    order.totalQuantity = m_params.qty1 - m_totalFilledC1;

    order.account = getTradingAccount();
    m_orderIdC1 = sendLmtOrderToIB(order);
    order.orderId = m_orderIdC1;

    QString msg = QString("[%1] Placed LMT Order %2 @ %3, for qty %4")
                    .arg(m_orderIdC1)
                    .arg(order.action)
                    .arg(order.lmtPrice)
                    .arg(order.totalQuantity);
    Log::info( msg, m_algoLogger);

    /// Update state
    m_state = State::LimitOrderPlacedPending;

    auto symbol = m_c1.symbol;
    if (symbol == "USD") {
        symbol = m_c1.localSymbol;
    }
    emit limitOrderPlaced(order, symbol);

    m_prevLimitPrice = order.lmtPrice;
}

void SpreadAlgo::setMktDataTickerIds(std::pair<TickerId, TickerId> pairdIds)
{
    m_tickerId1 = pairdIds.first;
    m_tickerId2 = pairdIds.second;
}

void SpreadAlgo::updateMinSpread(double minSpread)
{
    m_params.minSpread = minSpread;
}

OrderId SpreadAlgo::sendLmtOrderToIB(Order order)
{
    return m_ibClient->placeOrder(m_c1, order);
}

void SpreadAlgo::stopAndNotifyFinished()
{
    Log::info("Trading stopped!", m_algoLogger);
    m_state = State::Idle;

    auto executionC1 = m_executions.getExecution("c1_" + m_c1.symbol + m_c1.localSymbol);
    auto executionC2 = m_executions.getExecution("c2_" + m_c2.symbol + m_c2.localSymbol);

    TradeExecutionResult result;
    result.qty1 = executionC1.first;
    result.price1 = executionC1.second;
    result.qty2 = executionC2.first;
    result.price2 = executionC2.second;

    Log::info(QString("Finished with result: %1").arg(result.toString()), m_algoLogger);
    emit finished( result );
}

void SpreadAlgo::stop()
{
    Log::info( QString("Stopping trading initiated!"), m_algoLogger);

    switch(m_state) {

    case State::Idle:
    case State::Started:
        stopAndNotifyFinished();
        break;

    case State::LimitOrderPlacedPending:
    case State::LimitOrderSubmitted:
        Log::info(QString("[STOP][%1]Cancel LMT order").arg(m_orderIdC1), m_algoLogger);
        m_ibClient->cancelOrder(m_orderIdC1);
        m_state = State::StoppingTrading;
        break;

    case State::CancellingLimitAndRestart:
    case State::CancellingLimitAndPlaceNew:
        m_state = State::StoppingTrading;
        break;

    case State::LimitOrderFilled:
    case State::LimitOrderPartialFill:
    case State::MktOrderLastPlaced:
        /// wait to finish
        break;

    case State::MktOrderLastFilled:
        stopAndNotifyFinished();
        break;
    }
}

bool SpreadAlgo::willTradeImmediately(AlgoSide side, double minSpread, SpreadFormula formula) const
{
    if ( side == AlgoSide::Buy) {
        double spread = formula.calculateSpread(m_tickC1.bidPrice, m_tickC2.bidPrice);

        qDebug() <<"Current spread: " << spread <<", minSpread: " << minSpread;
        if (spread <= minSpread) {
            return true;
        }

    } else {

        double spread = formula.calculateSpread(m_tickC1.askPrice, m_tickC2.askPrice);

        qDebug() <<"Current spread: " << spread <<", minSpread: " << minSpread;
        if (spread >= minSpread) {
            return true;
        }
    }

    return false;
}

void SpreadAlgo::setSpreadLevelNotifier(QPair<double, double> spreadInterval)
{
    m_hasSpreadLevelProtection = true;
    m_closeOutsideSpreadLevel = spreadInterval;
}

int SpreadAlgo::getTradeQty2()
{
    // safer way for last qty ( avoiding rounding errors)
    if (m_totalFilledC1 == m_params.qty1) {
        return  m_params.qty2 - m_totalFilledC2;
    }

    double ratio = m_params.qty1 * 1.0 / m_params.qty2;
    uint qty2 = qRound(m_totalFilledC1 / ratio) - m_totalFilledC2;

    m_totalFilledC2 += qty2; // Assume MKT order will be filled

    return qty2;
}

void SpreadAlgo::logPrice(TickerId tickerId, TickType field, double price)
{
    QString id = m_tickerId1 == tickerId ? "F1" : "F2";
    QString type = field == BID ? "BID" : "ASK";
    Log::info(QString("[Price] %1,%2,%3").arg(id).arg(type).arg(price), m_algoLogger);
}
