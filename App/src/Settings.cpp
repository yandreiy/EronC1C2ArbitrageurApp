#include "Settings.h"

#include <QFile>
#include "log_utils.h"
#include "InstrumentFactory.h"
#include "InstrumentParams.h"
#include "ComboContract.h"

#include <QSharedPointer>
#include <QVariant>

using namespace IB;

Settings::Settings(QString configFileName, IBClient *ib, QString dirPath, QObject *parent)
        : QObject(parent),
          m_IbClient(ib),
          m_TickSizeContract1(0.01),
          m_TickSizeContract2(0.01),
          m_dirPath(dirPath)
{
    bool ok = connect(m_IbClient,SIGNAL(contractDetailsSig(int,ContractDetails)),
                      SLOT(onContractDetails(int,ContractDetails))); Q_ASSERT(ok);

    if (!m_dirPath.isEmpty()){
        configFileName = QString("%1\\%2").arg(m_dirPath).arg(configFileName);
        qDebug() <<"configFileName : "<< configFileName;
    }

    if (!QFile(configFileName).exists())
    {
        m_hasError = true;
        return;
    }

    m_Settings = new QSettings(configFileName, QSettings::IniFormat,this);

    readSettings();
    readLogSettings();
}

void Settings::readSettings()
{
    const QString port = m_Settings->value("TWS_OR_GATEWAY", IP_GATEWAY).toString();
    m_Ip = m_Settings->value("IP", "127.0.0.1").toString();
    m_Port = (port.toUpper() == "TWS" ? IP_TWS : IP_GATEWAY);
    m_TradingAccount = m_Settings->value("TradingAccount").toString();
    m_clientId = m_Settings->value("ClientId").toInt();

    m_Settings->beginGroup("TradingHoursUTC");
    const QString startTime = m_Settings->value("StartTime", "09:00").toString();
    const QString endTime = m_Settings->value("EndTime", "17:00").toString();
    m_TradingHoursStart = QTime::fromString(startTime, "hh:mm");
    m_TradingHoursEnd = QTime::fromString(endTime, "hh:mm");
    m_Settings->endGroup();

    m_Settings->beginGroup("Protection");
    m_protectionPriceDrop.first = m_Settings->value("NumberOfUpdatesToConsider").toUInt();
    m_protectionPriceDrop.second = m_Settings->value("MaxNumberOfPipsDroppedAllowed").toUInt();
    m_maxATExecutionsUnder1min = m_Settings->value("MaxAutotraderExecutionsUnder1min").toUInt();
    m_Settings->endGroup();
}

void Settings::readInstruments(QString filename)
{
    CVMap vmap;
    bool ok;
    ok = CSVParser::parseFile(filename, vmap, m_headers,',');

    if (!ok || vmap.size() < 2) {
        m_hasError = true;
        return;
    }

    int rowIndex = 0;
    const ORDMap& map1 = vmap.at(rowIndex);
    InstrumentParams params;
    params.symbol = map1["Symbol"].toString();
    params.secType = map1["SecType"].toString().trimmed();
    if (params.secType.isEmpty()) {
        params.secType = "FUT";
    }
    params.expiry = map1["Maturity"].toString();
    params.currency = map1["Currency"].toString();
    params.exchange = map1["Exchange"].toString();
    params.multiplier = map1["Multiplier"].toString().trimmed();
    params.right = map1["Right"].toString();
    params.strike = map1["Strike"].toDouble();

    if (params.symbol == "USD") {
        // combo contract
        m_combo1Ready = false;

        if (!map1["TickSize"].toString().isEmpty()){
            m_TickSizeContract1 = map1["TickSize"].toDouble();
        }

        Contract c1;
        {
            const ORDMap& row = vmap.at(++rowIndex);
            c1.symbol = row["Symbol"].toString().trimmed();
            c1.secType = row["SecType"].toString().trimmed();
            c1.expiry = row["Maturity"].toString().trimmed();
            c1.currency = row["Currency"].toString().trimmed();
            c1.exchange = row["Exchange"].toString().trimmed();
            c1.right = row["Right"].toString().trimmed();
            c1.multiplier = row["Multiplier"].toString().trimmed();
            c1.strike = row["Strike"].toDouble();
        }

        Contract c2;
        {
            const ORDMap& row = vmap.at(++rowIndex);
            c2.symbol = row["Symbol"].toString().trimmed();
            c2.secType = row["SecType"].toString().trimmed();
            c2.expiry = row["Maturity"].toString().trimmed();
            c2.currency = row["Currency"].toString().trimmed();
            c2.exchange = row["Exchange"].toString().trimmed();
            c2.right = row["Right"].toString().trimmed();
            c2.multiplier = row["Multiplier"].toString().trimmed();
            c2.strike = row["Strike"].toDouble();
        }

        m_combo1.reset( new ComboContract(c1,c2,params.exchange,params.currency,m_IbClient));

        connect(m_combo1.data(), &ComboContract::finished,[=](const Contract& comboContract){
            m_contract1 = new Contract(comboContract);
            m_combo1Ready = true;

            if (m_combo2Ready) {
                emit contractsReady();
            }
        });
        m_combo1->start();
    }
    else {
        m_contract1 = InstrumentFactory::instance()->getInstrument(params);
    }

    const ORDMap& row4 = vmap.at(++rowIndex);
    params.symbol = row4["Symbol"].toString().trimmed();
    params.expiry = row4["Maturity"].toString().trimmed();
    params.currency = row4["Currency"].toString().trimmed();
    params.exchange = row4["Exchange"].toString().trimmed();
    params.multiplier = row4["Multiplier"].toString().trimmed();
    params.secType = row4["SecType"].toString().trimmed();
    if (params.secType.isEmpty()) {
        params.secType = "FUT";
    }
    params.multiplier = row4["Multiplier"].toString().trimmed();
    params.right = row4["Right"].toString().trimmed();
    params.strike = row4["Strike"].toDouble();

    if (params.symbol == "USD") {
        // combo contract

        m_combo2Ready = false;
        if (!row4["TickSize"].toString().isEmpty()){
            m_TickSizeContract2 = row4["TickSize"].toDouble();
        }

        Contract c1;
        {
            const ORDMap& row = vmap.at(++rowIndex);
            c1.symbol = row["Symbol"].toString().trimmed();
            c1.secType = row["SecType"].toString().trimmed();
            c1.expiry = row["Maturity"].toString().trimmed();
            c1.currency = row["Currency"].toString().trimmed();
            c1.exchange = row["Exchange"].toString().trimmed();
            c1.right = row["Right"].toString().trimmed();
            c1.multiplier = row["Multiplier"].toString().trimmed();
            c1.strike = row["Strike"].toDouble();
        }

        Contract c2;
        {
            const ORDMap& row = vmap.at(++rowIndex);
            c2.symbol = row["Symbol"].toString().trimmed();
            c2.secType = row["SecType"].toString().trimmed();
            c2.expiry = row["Maturity"].toString().trimmed();
            c2.currency = row["Currency"].toString().trimmed();
            c2.exchange = row["Exchange"].toString().trimmed();
            c2.right = row["Right"].toString().trimmed();
            c2.multiplier = row["Multiplier"].toString().trimmed();
            c2.strike = row["Strike"].toDouble();
        }

        m_combo2.reset( new ComboContract(c1,c2,params.exchange,params.currency,m_IbClient));
        connect(m_combo2.data(), &ComboContract::finished,[=](const Contract& comboContract){
            m_contract2 = new Contract(comboContract);
            m_combo2Ready = true;
            if (m_combo1Ready) {
                emit contractsReady();
            }
        });
        m_combo2->start();

    } else {
        m_contract2 = InstrumentFactory::instance()->getInstrument(params);
        emit contractsReady();
    }
}

void Settings::readLogSettings()
{
    m_Settings->beginGroup("Logging");
    m_DebugLevel = m_Settings->value("DebugLevel", "INFO").toString();
    m_LogDirectory = m_Settings->value("LogDirectory").toString();
    m_Settings->endGroup();
}

bool Settings::requestContractDetails()
{
    if ( m_contract1->secType == "BAG") {
        return false;
    }

    reqId1 = m_IbClient->reqContractDetails(*m_contract1);
    reqId2 = m_IbClient->reqContractDetails(*m_contract2);

    return true;
}

QString Settings::getIbIp() const
{
    return m_Ip;
}

void Settings::onContractDetails(int id, const ContractDetails& details)
{
    Contract* contract = NULL;
    if(id == reqId1)
        contract = const_cast<Contract*>(m_contract1);
    else if(id == reqId2)
        contract = const_cast<Contract*>(m_contract2);
    else
        return;

    Q_ASSERT(contract != NULL);
    *contract = details.summary;

    if(id == reqId1) {
        m_TickSizeContract1 = details.minTick;
        emit contract1DetailsReceived();
    }
    else if(id == reqId2) {
        m_TickSizeContract2 = details.minTick;
        emit contract2DetailsReceived();
    }
}

IPs Settings::getIbPort() const
{
    return m_Port;
}

QTime Settings::getStartTimeTradingHours() const
{
    return m_TradingHoursStart;
}

QTime Settings::getEndTimeTradingHours() const
{
    return m_TradingHoursEnd;
}

const Contract* Settings::getContract1() const
{
    return m_contract1;
}

const Contract* Settings::getContract2() const
{
    return m_contract2;
}

double Settings::getTickSizeContract1() const
{
    return m_TickSizeContract1;
}

double Settings::getTickSizeContract2() const
{
    return m_TickSizeContract2;
}

QString Settings::getDebugLevel() const
{
    return m_DebugLevel;
}

QString Settings::getLogDirectory() const
{
    return m_LogDirectory;
}

bool Settings::isCurrentTimeInsideTradingHours() const
{
    const QTime currentTime = QDateTime::currentDateTimeUtc().time();
    return (m_TradingHoursStart <= currentTime && currentTime <= m_TradingHoursEnd );
}

QString Settings::tradingAccount() const
{
    return m_TradingAccount;
}

void Settings::readInstruments()
{
    QString instrumentsFile = QString("Instruments_%1.csv").arg(m_clientId);
    if (!m_dirPath.isEmpty()){
        instrumentsFile = QString("%1/%2").arg(m_dirPath).arg(instrumentsFile);
        qDebug() <<"instrumentsFile: "<< instrumentsFile;
    }

    readInstruments(instrumentsFile);
}

Settings::~Settings()
{
    delete m_Settings;
}
