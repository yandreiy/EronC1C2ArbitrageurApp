#include "log_utils.h"

#include <QString>
#include <QDebug>

namespace Log {

void trace(const QString& msg, log4cxx::LoggerPtr logger)
{
    if(logger == NULL)
        logger = log4cxx::Logger::getRootLogger();

    LOG4CXX_TRACE(logger, msg.toStdString());
}

void debug(const QString& msg, log4cxx::LoggerPtr logger)
{
    if(logger == NULL)
        logger = log4cxx::Logger::getRootLogger();

    LOG4CXX_DEBUG(logger, msg.toStdString());
}

void info(const QString& msg, log4cxx::LoggerPtr logger)
{
    if(logger == NULL)
        logger = log4cxx::Logger::getRootLogger();

    LOG4CXX_INFO(logger, msg.toStdString());
}

void warn(const QString& msg, log4cxx::LoggerPtr logger)
{
    if(logger == NULL)
        logger = log4cxx::Logger::getRootLogger();

    LOG4CXX_WARN(logger, msg.toStdString());
}

void error(const QString& msg, log4cxx::LoggerPtr logger)
{
    if(logger == NULL)
        logger = log4cxx::Logger::getRootLogger();

    LOG4CXX_ERROR(logger, msg.toStdString());
}

void fatal(const QString& msg, log4cxx::LoggerPtr logger)
{
    if(logger == NULL)
        logger = log4cxx::Logger::getRootLogger();

    LOG4CXX_FATAL(logger, msg.toStdString());
}

}
