#include "AlgoManager.h"
#include <QUuid>

AlgoManager::AlgoManager(IBClient *ib, FileAppender *appender, QObject *parent)
    : QObject(parent)
    , m_ib(ib)
    , m_isStopped(false)
    , m_appender(appender)
{
    m_timer.setTimerType(Qt::PreciseTimer);
}

void AlgoManager::set_algo(std::shared_ptr<SpreadAlgo> algo)
{
    m_algo = algo;

    // stop once algo finished and did some trades
    connect(m_algo.get(), &SpreadAlgo::finished,
            this,[=]( SpreadAlgo::TradeExecutionResult result) {
        if (m_isStopped || result.hasTraded()) {
            m_timer.stop();
            emit finished(result);
            qDebug() <<"stopped! traded!";
        }
    }, Qt::UniqueConnection);

    // protection
    connect(m_algo.get(), &SpreadAlgo::spreadPriceDropped,
            [=]( double ) {
        stop();
    });
}

void AlgoManager::start(const AlgoParameters &params)
{
    m_isStopped = false;
    m_params = params;
    disconnect(m_connection);

    QString id = QUuid::createUuid().toString();
    m_logger = log4cxx::Logger::getLogger( QString("[algo-manager%1]").arg(id).toStdString());
    m_logger->addAppender(m_appender);

    m_connection = connect(m_ib, &IBClient::currentTimeSig,this, [=](const QDateTime& dt){
        disconnect(m_connection);

        const auto& now = dt.time();
        qDebug() <<"now: " << now.toString();

        if (now < m_params.tradingHours.start) {
            int msec = now.msecsTo(m_params.tradingHours.start);
            startTimer(msec);
        }
        else if (now > m_params.tradingHours.end){
            auto nextDayStart = QDateTime(QDate::currentDate().addDays(1), m_params.tradingHours.start);
            int msec = dt.msecsTo( nextDayStart) ;
            startTimer(msec);
        }
        else {
            do_start();
        }
    }, Qt::UniqueConnection);

    m_ib->reqCurrentTime();
}

void AlgoManager::stop()
{
    m_isStopped = true;
    m_timer.stop();
    m_algo->stop();
    qDebug() <<"Stopped!";
}

void AlgoManager::do_start()
{
    qDebug() << "starting now!";
    m_algo->start(m_params);

    disconnect(m_connection);
    m_connection = connect(m_ib, &IBClient::currentTimeSig,this, [=](const QDateTime& dt){
        disconnect(m_connection);
        int msec = dt.time().msecsTo( m_params.tradingHours.end) + 2000;
        qDebug() << m_algo->getAlgoId() << " stopping in: " << msec/1000;

        m_timer.setSingleShot(true);
        connect(&m_timer, &QTimer::timeout,[=](){
            m_algo->stop();
            start(m_params);
        });
        m_timer.start(msec);

    }, Qt::UniqueConnection);

    m_ib->reqCurrentTime();
}

void AlgoManager::startTimer(int msec)
{
   qDebug() << "waiting for(s): " << msec/1000;
    m_timer.setSingleShot(true);
    connect(&m_timer,&QTimer::timeout,this,&AlgoManager::do_start);
    m_timer.start(msec);
    emit paused(msec/1000);
    Log::info( QString("[%1] Paused algo for %2 sec").arg(m_algo->getAlgoId()).arg(msec/1000), m_logger);
}
