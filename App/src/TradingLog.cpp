#include "TradingLog.h"
#include "Execution.h"
#include <QDateTime>

TradingLog::TradingLog(IBClient *ib, QObject *parent) : QObject(parent),
    m_IbClient(ib)
{
//    bool ok = connect(m_IbClient, SIGNAL(orderStatusSig(OrderId,IBString,int,int,double,int,int,double,int,IBString)),
//                      SLOT(orderStatus(OrderId,IBString,int,int,double,int,int,double,int,IBString)) ); Q_ASSERT(ok);

//    ok = connect(m_IbClient, SIGNAL(openOrderSig(OrderId,Contract,const ::Order&,OrderState)),
//                 SLOT(openOrder(OrderId,Contract,const ::Order&,OrderState)) ); Q_ASSERT(ok);

//    ok = connect(m_IbClient, SIGNAL(execDetailsSig(int,Contract,Execution)),
//                 SLOT(execDetails(int,Contract,Execution)) ); Q_ASSERT(ok);

//    ok = connect(m_IbClient, SIGNAL(errorSig(int,int,IBString)),
//                 SLOT(error(int,int,IBString)) ); Q_ASSERT(ok);
}

void TradingLog::openOrder(OrderId orderId, const Contract& c, const ::Order& order, const OrderState&)
{
    if(m_OpenOrderIds.contains(orderId))
        return;

    QString price = QString::number(order.lmtPrice);

    if(order.orderType == "MKT")
        price = "MKT";

    const QString text = QString("%1, Placed a %2 %3 %4 @ %5; Id = %6").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"))
            .arg(order.action).arg(order.totalQuantity).arg(c.localSymbol).arg(price).arg(orderId);

    m_OpenOrderIds.insert(orderId);

    emit newTradingLogItem(text);
}

void TradingLog::execDetails(int /*reqId*/, const Contract& c, const Execution& execution)
{
    const QString text = QString("%1, %2 %3 %4 @ %5; Id = %6").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"))
            .arg(execution.side).arg(execution.shares).arg(c.localSymbol).arg(execution.price).arg(execution.orderId);

    emit newTradingLogItem(text);
}

void TradingLog::orderStatus(OrderId orderId, const IBString &status, int /*filled*/,
                    int /*remaining*/, double /*avgFillPrice*/, int /*permId*/, int /*parentId*/,
                    double /*lastFillPrice*/, int /*clientId*/, const IBString& /*whyHeld*/)
{
    if(status == "Cancelled") {
        const QString text = QString("%1, Order with Id = %2 canceled").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"))
                .arg(orderId);

        emit newTradingLogItem(text);
    }
}

void TradingLog::error(const int id, const int errorCode, const IBString& errorString)
{
    if(id > 0) {
        const QString text = QString("%1, Error id = %2, code = %3. %4").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"))
                .arg(id).arg(errorCode).arg(errorString);
        emit newTradingLogItem(text);
    }
}

TradingLog::~TradingLog()
{

}
