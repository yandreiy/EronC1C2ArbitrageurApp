#ifndef COMMON
#define COMMON

#include <QString>
#include <QDateTime>

#include "log_utils.h"
#include "log4cxx/logger.h"
#include "log4cxx/propertyconfigurator.h"
#include "log4cxx/helpers/exception.h"
#include "log4cxx/fileappender.h"
#include "log4cxx/consoleappender.h"
#include "log4cxx/helpers/exception.h"
#include "log4cxx/helpers/transcoder.h"
#include "log4cxx/patternlayout.h"
#include "log4cxx/basicconfigurator.h"

enum class AlgoSide {
    Buy,
    Sell
};

struct TradingHours
{
    QTime start;
    QTime end;

    TradingHours() {}

    TradingHours(QTime s, QTime e)
        :start(s)
        ,end(e)
    {}
};

struct SpreadFormula
{
    /// Formula: minspread = x*C1 - y*C2
    double x;
    double y;

    SpreadFormula()
            :x(1.0)
            ,y(1.0)
    { }

    SpreadFormula(double _x, double _y)
        :x(_x)
        ,y(_y)
    { }

    double calculateLimitPrice( double minSpread, double priceC2) const
    {
        double lmt = (y * priceC2  + minSpread) / x;
        return lmt;
    }

    double calculateSpread( double price1, double price2) const
    {
        return price1 * x - price2 * y;
    }

};

struct AlgoParameters
{
    AlgoSide side;
    double minSpread;
    double delta;								///< Delta to enter the market with LMT
    uint qty1, qty2;							///< The quantities for each contract
    bool qty2_Negative = false; 					///< Mark the qty2 as negative, meaning it will do the opposite order side (Buy->Sell)

    TradingHours tradingHours;  				///< The hours to do trading, ignore others

    double minDeltaBidAsk;		///< Min Delta between limit price and ask price, so that only within this delta, limit orders will get placed

    /// Pair of: NumberOfUpdatesToConsider, MaxnNumberOfPipsDroppedAllowed
    QPair<uint, uint> protectionPriceDrop;

    enum class SpreadFormulaOld {
        C1_minus_C2,			// C1 - C2
        C1_minus_2C2,			// C1 - 2*C2
        C1_3times_minus_2C2,	// 3*C1 - 2*C2
        C1_minus_1p25_C2,		// C1 - 1.25*C2
        C1_p2_minus_0p5_C2		// 0.2*C1 - 0.5*C2
    };

    SpreadFormula spreadFormula;

    QString toString() const
    {
        return QString("%1,%2,%3,%4,%5,%6")
                        .arg(side == AlgoSide::Buy ? "Buy" : "Sell")
                        .arg(QString("minSpread=%1").arg(minSpread))
                        .arg(QString("delta=%1").arg(delta))
                        .arg(QString("qty1=%1").arg(qty1))
                        .arg(QString("qty2=%1").arg(qty2))
                        .arg(QString("minDeltaLmtAndAsk=%1").arg(minDeltaBidAsk))
                        ;
    }
};

class LogConfig {
public:

static void configureLogger(QString debugLevel, QString logFile, LoggerPtr logger)
{
    const QDateTime currentDateTime = QDateTime::currentDateTimeUtc();
    logFile = logFile.replace("{$date}",currentDateTime.date().toString("yyyyMMdd"));
    logFile = logFile.replace("{$time}",currentDateTime.time().toString("hhmmss"));
    logFile = logFile.replace("{$configname}", APP_VERSION);

    LOG4CXX_DECODE_CHAR(logFileName, QString("%1").arg(logFile).toStdString());
    LOG4CXX_DECODE_CHAR(logPattern, "%d %-5p %c - %m%n");

    log4cxx::FileAppender * fileAppender = new
    log4cxx::FileAppender(log4cxx::LayoutPtr(new log4cxx::PatternLayout(logPattern)), logFileName, false);

    log4cxx::ConsoleAppender * consoleAppender = new
    log4cxx::ConsoleAppender(log4cxx::LayoutPtr(new log4cxx::PatternLayout(logPattern)));

    log4cxx::helpers::Pool p;
    fileAppender->activateOptions(p);
    consoleAppender->activateOptions(p);

//    log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(fileAppender));
    log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(consoleAppender));
//    log4cxx::Logger::getRootLogger()->setLevel(Level::toLevel(debugLevel.toStdString()));

    logger->addAppender(log4cxx::AppenderPtr(fileAppender));
    Logger::getLogger("[TRADING]")->addAppender(log4cxx::AppenderPtr(fileAppender));
}

};
#endif // COMMON

