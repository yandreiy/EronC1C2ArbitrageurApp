#ifndef ADDSPREADERDIALOG_H
#define ADDSPREADERDIALOG_H

#include <QDialog>

namespace Ui {
class AddSpreaderDialog;
}

class AddSpreaderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddSpreaderDialog(QWidget *parent = 0);
    ~AddSpreaderDialog();

    QStringList getLines();

private:
    Ui::AddSpreaderDialog *ui;
};

#endif // ADDSPREADERDIALOG_H
