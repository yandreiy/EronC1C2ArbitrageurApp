#ifndef SPREADAUTOTRADER_H
#define SPREADAUTOTRADER_H

#include <QObject>
#include "SpreadAlgo.h"
#include "log4cxx/logger.h"
#include <memory>
#include "IBClient.h"
#include "AlgoManager.h"

class SpreadAutoTrader : public QObject
{
    Q_OBJECT

    enum class State
    {
       Idle,
       Started,
       CloseAllOpened,
       StoppedBySpreadDropp,
       StoppedByUser
    };

public:

    struct Parameters
    {
        enum class Type {
            Passive,
            Neutral,
            Aggressive
        };

        static Type typeFromString(QString str) {
            if (str.startsWith("Passive",Qt::CaseInsensitive)) {
                return Type::Passive;
            }
            if (str.startsWith("Neutral",Qt::CaseInsensitive)) {
                return Type::Neutral;
            }
            if (str.startsWith("Aggressive",Qt::CaseInsensitive)) {
                return Type::Aggressive;
            }

            return Type::Passive;
        }

        static QString typeToString(Type type) {
            switch(type) {
            case Type::Passive:
                return "Passive";
            case Type::Neutral:
                return "Neutral";
            case Type::Aggressive:
                return "Aggressive";
            default:
                return "Passive";
            }
        }

        AlgoParameters algoParams1;
        AlgoParameters algoParams2;

        double trailingDelta;								///< The amount to substract from previous traded spread level
        double skewDelta;									///< Same as trailing delta, but for opposite side
        int maxPositionsC2;									///< The max positions allowed to open for 2nd contract
        QPair<double,double> closeALlOutsideSpreadInterval;	///< If spread goes outside this interval, we close ALL Opened positions

        uint freezeTimeMsec;								///< Wait this amount of msec before starting next algos
        uint maxExecutionsUnder1min;						///< Max Number of Algo executions under 1min allowed

        Parameters::Type type;							    ///< Determines the way we set the sizes.

        QString toString() const
        {
            return QString("Algo1Params: %1, \nAlgo2Params: %2, \nTrailingDelta: %3, MaxPositionsC2: %4, PositionsCloseOutsideInterval: %5, %6, FreezeTime: %7, Type: %8")
                    .arg(algoParams1.toString())
                    .arg(algoParams2.toString())
                    .arg(trailingDelta)
                    .arg(maxPositionsC2)
                    .arg(closeALlOutsideSpreadInterval.first)
                    .arg(closeALlOutsideSpreadInterval.second)
                    .arg(freezeTimeMsec)
                    .arg( Parameters::typeToString(type))
                    ;
        }
    };

    explicit SpreadAutoTrader(
            std::shared_ptr<SpreadAlgo> algo1,
            std::shared_ptr<SpreadAlgo> algo2,
            log4cxx::FileAppender* appender = nullptr,
            QObject *parent = 0);

    void start(const Parameters& params);

    void stop();

    bool hasActiveOrders() const {
        return m_algo1->hasActiveOrder() || m_algo2->hasActiveOrder();
    }

    bool isStarted() const {
        return m_state != State::Idle;
    }

    SpreadAutoTrader::Parameters getParams() { return m_params;}

    void widenSpreadsBy(double value);
    void widenSkewDeltaBy(double value);
    void widenTrailDeltaBy(double value);

    void tightenSpreadsBy(double value);
    void tightenSkewDeltaBy(double value);
    void tightenTrailDeltaBy(double value);

    bool wasPriceDropp();

    void updateMinSpreadBuy(double value);
    void updateMinSpreadSell(double value);

    bool willTradeImmediatelyBuy(double minSpread);
    bool willTradeImmediatelySell(double minSpread);

    bool maxPositionsLimitReached();

signals:
    void logAction(QString);
    void finished();

    void startedNewRound(AlgoParameters aglo1Params, AlgoParameters aglo2Params);
    void updatedMinSpreads(double minSpread1, double minSpread2);

private slots:
    void onAlgo1Finished( SpreadAlgo::TradeExecutionResult result );
    void onAlgo2Finished( SpreadAlgo::TradeExecutionResult result );

    void closeAllOpenedPositions();

    void onSpreadPriceDropped(double);

private:
    void onBothAlgosFinished();
    void startAlgosNewRound(double tradedSpreadLevel);

    double updateTimesAndgetTradedSpread();
    void logStartInfo(const SpreadAutoTrader::Parameters &params);

    void doStopAndNotify(QString reason);
    void logRoundFinished(double spread);
    void logStartNextRound(AlgoParameters buySpread, AlgoParameters sellSpread);

    void sendCloseMktOrders();
    void multiplyAlgoQty(AlgoParameters &paramsAlgo);
    AlgoParameters getNewParametersAlgo2(double tradedSpreadLevel);
    AlgoParameters getNewParametersAlgo1(double tradedSpreadLevel);
    void handlePositionsLimitReached(double spread);

private:
    Parameters m_params;

    std::shared_ptr<SpreadAlgo> m_algo1;
    std::shared_ptr<SpreadAlgo> m_algo2;

    int m_openedPositionsC2;	    	///< Opened positions so far for 2nd Contract
    int m_maxAllowedQtyC2;			///< We are allowed to be in the range [-max, +max] positions

    int m_openedPositionsC1;		///!< Opened positions so far for first Contract

    SpreadAlgo::TradeExecutionResult m_algo1Result;
    SpreadAlgo::TradeExecutionResult m_algo2Result;

    log4cxx::FileAppender* m_appender;
    log4cxx::LoggerPtr m_autoTraderLogger;
    State m_state;

    IBClient *m_ibClient;
    OrderId m_closeOrderIdC1;
    OrderId m_closeOrderIdC2;

    int m_timesExecuted;	/// +1 means one time bought, -1 means one time sold

    bool m_isSkewed1, m_isSkewed2;

    std::shared_ptr<AlgoManager> m_algoManager1;
    std::shared_ptr<AlgoManager> m_algoManager2;

    bool m_wasPriceDrop = false;
};

#endif // SPREADAUTOTRADER_H
