#ifndef ERONC1C2ARBITRAGEURAPP_H
#define ERONC1C2ARBITRAGEURAPP_H

#include <QMainWindow>
#include <functional>
#include "IBClient.h"
#include "Instruments.h"
#include "Settings.h"
#include "log4cxx/logger.h"
#include "SpreadAlgo.h"
#include "ActiveOrders.h"
#include "TradingLog.h"
#include "SpreadAutoTrader.h"

#include <QCloseEvent>
#include <QPushButton>
#include <QListWidget>
#include <QTimer>
#include "AlgoManager.h"

namespace Ui {
class SpreaderMainWindow;
}

class SpreaderMainWindow: public QMainWindow
{
    Q_OBJECT

    struct UiContext
    {
        UiContext(std::shared_ptr<SpreadAutoTrader>& _autotrader, QPushButton* _button, QListWidget* _listWidget, const char* _startTxt, const char* _stopTxt)
            :autotrader(_autotrader)
            ,button(_button)
            ,listWidget(_listWidget)
            ,startTxt(_startTxt)
            ,stopTxt(_stopTxt)
        {
        }

        std::shared_ptr<SpreadAutoTrader> & autotrader;
        QPushButton* button;
        QListWidget* listWidget;
        const char* startTxt, *stopTxt;
    };

public:
    explicit SpreaderMainWindow(IBClient* ib, Settings* settings, QString spreaderDirPath="", QWidget *parent = 0);
    ~SpreaderMainWindow();

    AlgoParameters getAlgoParams();

    void closeEvent(QCloseEvent* event);

    void startStopAutoTrader(UiContext uiContext);

    QString getDirPath() { return m_spreaderDirPath; }

    void setTighter(double value);
    void setWider(double value);

    bool startAt1();

    std::list<std::shared_ptr<SpreadAutoTrader>> getATs();
    std::shared_ptr<SpreadAutoTrader> getAt1();

    bool updateATMinSpreadBuy(double value);
    bool updateATMinSpreadSell(double value);


signals:
    void beforeExit(QString name);
    void restart(SpreaderMainWindow* sender);

    void currentSpreadBidAsk(QString name, double spreadBid, double spreadAsk);

    void autotrader1Started(QString name, SpreadAutoTrader::Parameters params);
    void autotrader1StartedNewRound(QString name, AlgoParameters aglo1Params, AlgoParameters aglo2Params);
    void autotrader1minSpreadsUpdated(QString name, double minSpread1, double minSpread2);

    void autotrader1ChangedColor(QString name, QColor color);

    void autotrader1FinishedPriceDropped(SpreaderMainWindow* sender);

private:
    void configureLogger();
    void initializeIbConnection();
    bool runIfConfirmed( std::function<void()> func);

private slots:
    void onIbConnected();
    void onIbDisconnected();
    void onIbError(const int id, const int errorCode, const IBString& errorString);
    void onPriceChanged();

    void onContract1DetailsReceived();
    void onContract2DetailsReceived();

    void on_pushBtnBuy_clicked();
    void on_pushBtnSell_clicked();

    void updateOpenOrdersList();
    void onNewTradingLogItem(const QString& tradingItem);

    void on_pushButtonStartAutoTrading_clicked();
    void on_doubleSpinBoxAutoTrading_PositionsCloseLevelMin_valueChanged(double arg1);
    void on_pushButtonStartAutoTrader2_clicked();

    void saveSettings();
    void restoreSettings();

    void startAutoTrader(UiContext uiContext);

    SpreadAutoTrader::Parameters getAutoTraderParams();

    void on_pushButtonStartAutoTrader3_clicked();

    void on_pushButtonStartAutoTrader4_clicked();

    void on_doubleSpinBoxSpreadFormulaX_valueChanged(double arg1);
    void on_doubleSpinBoxSpreadFormulaY_valueChanged(double arg1);

    void logAlgoEvents( std::shared_ptr<SpreadAlgo> algo, QListWidget* widget);

    void on_pushButton_restart_clicked();

private:
    Ui::SpreaderMainWindow *ui;
    IBClient * m_ibClient;

    Instruments* m_Instruments;
    Settings* m_Settings;

    log4cxx::LoggerPtr m_TradeLogger;
    log4cxx::LoggerPtr m_IbErrorLogger;

    std::shared_ptr<SpreadAlgo> m_algoBuy, m_algoSell;

    std::shared_ptr<SpreadAutoTrader> m_spreadAutoTrader1;
    std::shared_ptr<SpreadAutoTrader> m_spreadAutoTrader2;
    std::shared_ptr<SpreadAutoTrader> m_spreadAutoTrader3;
    std::shared_ptr<SpreadAutoTrader> m_spreadAutoTrader4;

    ActiveOrders* m_ActiveOrders;
    TradingLog* m_TradingLog;
    QTimer* m_timer;

    std::shared_ptr<AlgoManager> m_algoBuyManager;
    std::shared_ptr<AlgoManager> m_algoSellManager;

    SpreadFormula m_spreadFormula;

    QString m_spreaderDirPath; // the dirPath where the spreader settings are (bin directory)
    LoggerPtr m_logger; // the main logger
    log4cxx::FileAppender * m_fileAppender;

    QTimer* m_timerSaveSettings;
};

#endif // ERONC1C2ARBITRAGEURAPP_H

