#ifndef TRADINGLOG_H
#define TRADINGLOG_H

#include <QObject>
#include <QSet>
#include "IBClient.h"
#include "Contract.h"
#include "Order.h"
#include "OrderState.h"

class TradingLog : public QObject
{
    Q_OBJECT

public:
    TradingLog(IBClient* ib, QObject* parent = 0);
    ~TradingLog();

signals:
    void newTradingLogItem(const QString&);

private slots:
    void openOrder(OrderId orderId, const Contract&, const ::Order&, const OrderState&);
    void execDetails(int reqId, const Contract& contract, const Execution& execution);
    void orderStatus(OrderId orderId, const IBString &status, int filled,
                        int remaining, double avgFillPrice, int permId, int parentId,
                        double lastFillPrice, int clientId, const IBString& whyHeld);
    void error(const int id, const int errorCode, const IBString& errorString);

private:
    IBClient* m_IbClient;
    QSet<OrderId> m_OpenOrderIds;
};

#endif // TRADINGLOG_H
