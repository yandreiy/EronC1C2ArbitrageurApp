#ifndef SPREADERSMODEL_H
#define SPREADERSMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include "SpreaderMainWindow.h"

struct Row
{
    QString spreaderName;
    bool isChecked = false;
    double  minSpreadBid = NAN;
    double  minSpreadAsk = NAN;
    double  currentSpreadBid = NAN;
    double  currentSpreadAsk = NAN;

    QColor spreaderColor = QColor(Qt::white);
};



class SpreadersModel: public QAbstractTableModel
{
    Q_OBJECT

public:
    SpreadersModel();

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);

    virtual bool removeRows(int row, int count, const QModelIndex &parent);
    virtual bool insertRows(int row, int count, const QModelIndex &parent);

    void setCurrentBidAsk(QString spreaderName, double spreadBid, double spreadAsk);

    void setMinSpreadsBidAsk(QString spreaderName, double spreadBid, double spreadAsk);
    void setMinSpreadsBid(QString spreaderName, double spreadBid);
    void setMinSpreadsAsk(QString spreaderName, double spreadAsk);


    void setSpreaderColor(QString spreaderName, QColor color);

    void removeRowByName(QString spreaderName);

    QVector<QString> getAllCheckedNames();

signals:
    void on_mybid_changed(QString name, double oldValue, double newValue);
    void on_myask_changed(QString name, double oldValue, double newValue);

private:
    bool m_checked = false;
    QVector<Row> m_rows;
};

#endif // SPREADERSMODEL_H
