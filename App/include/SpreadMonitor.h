#ifndef SPREADMONITOR_H
#define SPREADMONITOR_H

#include "Common.h"

class SpreadMonitor
{
public:
    static bool isTargetReached(double spread, const AlgoParameters& params);
};

#endif // SPREADMONITOR_H
