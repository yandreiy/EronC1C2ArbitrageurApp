#ifndef TICKDATAFEED
#define TICKDATAFEED

#include <QPair>
#include <QVariantMap>
#include <memory>


// IBFeed
#include "IBClient.h"
#include "InstrumentParams.h"

namespace FEED
{

typedef long RequestId;

struct TickData
{
    typedef QPair<double, uint> PriceQty;

    RequestId id;
    PriceQty bid;
    PriceQty ask;
};

class TickDataFeedProvider
{
public:
    virtual ~TickDataFeed() {}
    virtual RequestId subscribe(const QVariantMap& map, TickDataFeedListenerWeakPtr ptr ) = 0;
};

class TickDataFeedListener
{
public:
    virtual ~TickDataFeed() {}
    virtual void onTickData() = 0;
};

typedef std::weak_ptr<TickDataFeedListener> TickDataFeedListenerWeakPtr;

class IBFeed: public TickDataFeedProvider
{
public:
    virtual RequestId subscribe(const QVariantMap &map, TickDataFeedListenerWeakPtr ptr)
    {
    }

private:
    IBClient* m_ibClient;
};

}
#endif // TICKDATAFEED

