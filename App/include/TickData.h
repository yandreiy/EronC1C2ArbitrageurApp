#ifndef TICKDATA_H
#define TICKDATA_H

#include <QtGlobal>
#include <cmath>

namespace DataFeed {

struct TickData
{
   double bidPrice, askPrice;
   uint bidQty, askQty;

   TickData()
       :bidPrice(NAN)
       ,askPrice(NAN)
       ,bidQty(NAN)
       ,askQty(NAN)
   { }

};

}
#endif // TICKDATA_H

