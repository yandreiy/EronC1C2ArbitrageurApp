#ifndef MYBIDASKDELEGATE_H
#define MYBIDASKDELEGATE_H

#include <QObject>
#include <QStyledItemDelegate>

class MyBidAskDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    MyBidAskDelegate(QObject* parent=0);

    // QAbstractItemDelegate interface
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    // QAbstractItemDelegate interface
public:
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // MYBIDASKDELEGATE_H
