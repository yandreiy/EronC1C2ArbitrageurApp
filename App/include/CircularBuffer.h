#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <QVector>

template<class T>
class CircularBuffer
{
public:
    CircularBuffer::CircularBuffer(int size)
            :m_size(size)
            ,m_head(0)
            ,m_tail(-1)
    {
        m_data.reserve(m_size);
    }

    void add(T element)
    {
        if (m_data.size() < m_size) {
            m_data.append(element);
            inc(m_tail);
        }
        else {
            m_data[m_head] = element;
            inc(m_head);
            inc(m_tail);
        }
    }

    bool isEmpty() const {
        return m_data.empty();
    }

    T head() {
        return m_data.at(m_head);
    }

    T tail() {
        return m_data.at(m_tail);
    }

private:
    inline void inc(int& index) {
        index = (index+1) % m_size;
    }

private:
    int m_size;
    QVector<T> m_data;

    int m_head, m_tail;

};

#endif // CIRCULARBUFFER_H
