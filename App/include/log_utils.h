#ifndef LOGUTILS_H
#define LOGUTILS_H

#include "log4cxx/logger.h"

class QString;

using namespace log4cxx;

namespace Log
{
    void trace(const QString& msg, LoggerPtr logger = LoggerPtr(NULL));

    void debug(const QString& msg, LoggerPtr logger = LoggerPtr(NULL));

    void info(const QString& msg, LoggerPtr logger = LoggerPtr(NULL));

    void warn(const QString& msg, LoggerPtr logger = LoggerPtr(NULL));

    void error(const QString& msg, LoggerPtr logger = LoggerPtr(NULL));

    void fatal(const QString& msg, LoggerPtr logger = LoggerPtr(NULL));
}

#endif // LOGUTILS_H
