#ifndef ACTIVEORDERS_H
#define ACTIVEORDERS_H

#include <QObject>
#include "IBClient.h"
#include "Contract.h"
#include "Order.h"
#include "OrderState.h"
#include <QMap>

struct OpenOrderData
{
    int filled, remaining, avgFillPrice;
    Order order;
    Contract contract;
};

class ActiveOrders : public QObject
{
    Q_OBJECT

public:
    ActiveOrders(IBClient* ib, QObject* parent = 0);
    ~ActiveOrders();

    const QMap<OrderId, OpenOrderData>& getOpenOrders() const;

signals:
    void openOrdersChanged();

private slots:
    void orderStatus(OrderId orderId, const IBString &status, int filled,
                        int remaining, double avgFillPrice, int permId, int parentId,
                        double lastFillPrice, int clientId, const IBString& whyHeld);

    void openOrder(OrderId orderId, const Contract&, const ::Order&, const OrderState&); 

private:
    IBClient* m_IbClient;
    QMap<OrderId, OpenOrderData> m_OpenOrders;
};

#endif // ACTIVEORDERS_H
