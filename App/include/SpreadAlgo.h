#ifndef SPREADALGO_H
#define SPREADALGO_H

#include <QObject>

#include <limits>
#include <memory>
#include <functional>
#include <QScopedPointer>
#include <QSet>
#include <Contract.h>
#include "Order.h"
#include "SpreadMonitor.h"
#include "IBClient.h"
#include "TickData.h"
#include "log4cxx/logger.h"
#include "Common.h"
#include "AlgoExecutionManager.h"
#include "CircularBuffer.h"
#include "Settings.h"

class SpreadAlgo : public QObject
{
    Q_OBJECT

    /// Possible states of the Algo
    enum class State {
        Idle,
        Started,
        LimitOrderPlacedPending,
        LimitOrderSubmitted,
        LimitOrderFilled,
        LimitOrderPartialFill,		 				/// Partial Fill, wait for rest
        LimitOrderPartialFillWhileCanceled,			/// Partial Filled received after we sent a cancel request
        MktOrderPartialFilled,
        CancellingLimitAndPlaceNew,  				/// Cancel LMT, wait for confirmation, send new order
        CancellingLimitAndRestart,	 				/// Cancel LMT, wait for confirmation, restart
        MktOrderLastPlaced,
        MktOrderLastFilled,
        StoppingTrading								/// Trading is being stopped
    };

public:

    struct TradeExecutionResult
    {
        uint qty1;
        double price1;

        uint qty2;
        double price2;

        /// If only one has traded, it's an exceptional case
        bool hasTraded() const {
           return qty1 + qty2 > 0;
        }

        double spread() const {
            return price1 - price2;
        }

        QString toString() const
        {
            return QString("%1,%2,%3,%4")
                    .arg(qty1)
                    .arg(price1)
                    .arg(qty2)
                    .arg(price2);
        }
    };

    /// subscribes to market data to initialize the feed
    explicit SpreadAlgo( IBClient* ib, Settings* settings, const Contract& c1, const Contract& c2, log4cxx::FileAppender* appender , QObject *parent = 0);

    virtual void init();

    /// start the algo using the parameters, monitoring prices and emiting signals
    virtual bool start( const AlgoParameters& parameters);

    /// stop the algo
    virtual void stop();

    virtual bool isIdle() { return m_state == State::Idle; }

    /// returns true if the parameters will generate an immediate trade & fill with current market situation
    virtual bool willTradeImmediately(AlgoSide side, double minSpread, SpreadFormula formula) const;

    /// returns current parameters
    virtual AlgoParameters getParameters() { return m_params;}

    virtual Contract getContract1() { return m_c1;}
    virtual Contract getContract2() { return m_c2;}

    /// Set the spread level that when reached, a signal will get emitted
    virtual void setSpreadLevelNotifier(QPair<double,double> spreadInterval);

    virtual void logPrice(TickerId tickerId, TickType field, double price);
    virtual bool checkSpreadDropConditions(double spread);

    static double getNormalizedPrice(double tickSize, double price);

    virtual bool hasActiveOrder() const;
    virtual QString getAlgoId() { return m_algoId;}

    void placeLimitOrder();

    void setMktDataTickerIds(std::pair<TickerId, TickerId> pairdIds);

    void updateMinSpread(double minSpread);

signals:
    void finished( SpreadAlgo::TradeExecutionResult tradesResult);

    void closeSpreadLevelReached(double spread);
    void spreadPriceDropped(double spread);

    void limitOrderPlaced( Order order, QString symbol);
    void marketOrderPlaced(Order, QString symbol);

    void limitOrderFilled(OrderId, int qty, double price);
    void mktOrderFilled(OrderId, int qty, double price);

    void orderCancelSent(OrderId);
    void orderCancelConfirmed(OrderId);

public slots:
    virtual void tickPrice(TickerId tickerId, TickType field, double price, int canAutoExecute);

protected slots:
    virtual void execDetails(int reqId, const Contract& contract, const Execution& execution);
    virtual void orderStatus(OrderId orderId, const IBString &status, int filled,
                        int remaining, double avgFillPrice, int permId, int parentId,
                        double lastFillPrice, int clientId, const IBString& whyHeld);
    virtual void onError(const int id, const int errorCode, const IBString& errorString);
    virtual void onConnected();

protected:
    virtual void subscribeToMktData();
    virtual void setupIB();
    virtual double getTickSizeC1();
    virtual double getTickSizeC2();
    virtual QString getTradingAccount();

    virtual void updateTick(TickerId tickerId, TickType field, double price);

    virtual void processNewPrice(TickerId tickerId, double spread);
    virtual void checkEntrySignal(double price);

    virtual void tryPlaceLimitOrder();
    virtual void placeMktOrder(uint qty);

    virtual void checkBeforeFillConditions(TickerId tickerId, double spread);
    virtual void handleLimitOrderExecution(const Execution &execution);

    virtual void onCancelledConfirmed();

    //inline int getEffectiveTradeQty2() const;

    /// determine the right Qty to send for C2
    virtual int getTradeQty2();

    virtual void stopAndNotifyFinished();

    virtual void evaluateCurrentPrices();
    virtual void handleLimitOrderExecutionPartialFill(const Execution &execution);
    virtual void restartAfterPartialFillAndCancel();

    /// Get the spread, if the prices are invalid, return false
    virtual bool hasSpread(TickerId tickerId, double price, double& spread);

    virtual double getLimitPrice();

    virtual OrderId sendLmtOrderToIB(Order order);
    virtual OrderId sendMktOrderToIB(Order order);
    virtual void sendCancelOrderToIB();

protected:
    AlgoParameters m_params; 				///< Algo parameters
    Contract m_c1, m_c2; 				///< Contracts traded
    Settings* m_settings;
    TickerId m_tickerId1, m_tickerId2;	///< TickerId's from IB feed
    double m_tickSizeC1;
    double m_tickSizeC2;

    IBClient* m_ibClient;

    DataFeed::TickData m_tickC1; 		///< Current tick for C1
    DataFeed::TickData m_tickC2; 		///< Current tick for C1

    State m_state;						///< Current state of the algo
    OrderId m_orderIdC1;	///< Order IDs for each contract

    QSet<OrderId> m_orderIdsC2; ///< The sent MKT orders

    log4cxx::FileAppender* m_appender;

    log4cxx::LoggerPtr m_algoLogger;
    AlgoExecutionManager m_executions;

    QPair<double,double> m_closeOutsideSpreadLevel; /// if spread goes outside this interval, notify
    bool m_hasSpreadLevelProtection;

    QScopedPointer<CircularBuffer<double>> m_spreadBuffer;
    QString m_algoId;

    int m_totalFilledC1;
    int m_totalFilledC2;

    int m_comboExecutionsC1 = 0;
    int m_comboExecutionsC2 = 0;

    double m_prevLimitPrice = std::numeric_limits<double>::quiet_NaN();
};

Q_DECLARE_METATYPE(Order);
Q_DECLARE_METATYPE(SpreadAlgo::TradeExecutionResult);

#endif // SPREADALGO_H
