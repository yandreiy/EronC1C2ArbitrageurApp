#ifndef INSTRUMENTS_H
#define INSTRUMENTS_H

#include <QObject>
#include <QDate>
#include "IBClient.h"
#include "Settings.h"

struct TickData
{
    TickData() : askPrice(0), bidPrice(0) {}
    double askPrice, bidPrice;

    double spread() const { return qAbs(askPrice - bidPrice); }

    void reset() {
        askPrice = bidPrice = 0;
    }
};

class Instruments : public QObject
{
    Q_OBJECT

public:
    Instruments(IBClient* ib, Settings* settings, QObject* parent = 0);
    ~Instruments();

public slots:
    void subscribeToMarketData();

    const TickData& getContract1TickData() const;
    const TickData& getContract2TickData() const;

    std::pair<TickerId, TickerId> getTickerIds()
    {
        return std::make_pair(m_ReqId1, m_ReqId2);
    }

signals:
    void priceChanged();
    void contract1PriceChanged();

    void tickPriceSig(TickerId tickerId, TickType field, double price, int canAutoExecute);

private slots:
    void tickPrice(TickerId tickerId, TickType field, double price, int canAutoExecute);

private:
    TickData m_TdContract1, m_TdContract2;
    IBClient* m_IbClient;
    Settings* m_Settings;

    TickerId m_ReqId1, m_ReqId2;
};

#endif // INSTRUMENTS_H
