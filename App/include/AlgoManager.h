#ifndef ALGOMANAGER_H
#define ALGOMANAGER_H

#include <QObject>
#include <QTimer>
#include <memory>
#include <QDebug>

#include "log4cxx/logger.h"

#include <SpreadAlgo.h>
#include <IBClient.h>

class AlgoManager : public QObject
{
    Q_OBJECT

public:
    explicit AlgoManager( IBClient* ib,  log4cxx::FileAppender* appender = nullptr, QObject *parent = 0);

    void set_algo( std::shared_ptr<SpreadAlgo> algo);


public slots:
    void start( const AlgoParameters& params);

    /// This will stop the algo for good
    void stop();

signals:
    void finished(SpreadAlgo::TradeExecutionResult result);
    void paused(int sec);

private slots:
    void do_start();
    void startTimer(int msec);

private:
    std::shared_ptr<SpreadAlgo> m_algo;
    AlgoParameters m_params;
    IBClient *m_ib;
    QTimer m_timer;

    QMetaObject::Connection m_connection;
    bool m_isStopped;

    log4cxx::FileAppender* m_appender;
    log4cxx::LoggerPtr m_logger;

};

#endif // ALGOMANAGER_H
