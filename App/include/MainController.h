#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QMainWindow>
#include <QVector>
#include <QSharedPointer>
#include <QListWidgetItem>
#include "IBClient.h"
#include "SpreaderMainWindow.h"
#include "SpreadersModel.h"

namespace Ui {
class MainControllerWindow;
}

class MainController: public QMainWindow
{
    Q_OBJECT

public:
    explicit MainController(IBClient* ib, QWidget *parent = 0);
    ~MainController();

    void readSettings();

    void closeEvent(QCloseEvent* event);

signals:

public slots:

protected:
    bool eventFilter(QObject *obj, QEvent *event);

private slots:

    SpreaderMainWindow* getSpreader(QString baseDir);

    void on_pushButton_StopATs_clicked();

    void on_pushButton_AddSpreader_clicked();

    void on_pushButton_TightenSpreads_clicked();
    void on_pushButton_TightenSkewDelta_clicked();
    void on_pushButton_TightenTrailDelta_clicked();

    void on_pushButton_WidenSpreads_clicked();
    void on_pushButton_WidenSkewDelta_clicked();
    void on_pushButton_WidenTrailDelta_clicked();

    std::list<std::shared_ptr<SpreadAutoTrader>> getAllAtsChecked();
    std::list<std::shared_ptr<SpreadAutoTrader>> getAllAts();
    QVector< QSharedPointer<SpreaderMainWindow>> getAllSpreadersChecked();

    void on_pushButton_StartATs_clicked();

    virtual void execDetails(int reqId, const Contract& contract, const Execution& execution);

    virtual void onTradesTimerTimeout();

    void triggerStartAtsAtTime();
    void triggerStopAtsAtTime();

private:
    void startSpreaders();
    void addSpreader(QString spreaderPath);

    void saveSettings();
    void restoreSettings();

    void configureLogger();

private:
    Ui::MainControllerWindow* ui;

    IBClient *m_ib;
    QVector< QSharedPointer<SpreaderMainWindow>> m_spreaders;
    QString m_debugLevel;
    QString m_logDirectory;
    SpreadersModel m_model;

    QMap<QString, QByteArray> m_savedGeometries; // spreader name - geometry

    QPair<int, int> m_maxTradesInInterval; //3,30 - 3 trades max in 30 sec
    QMap<QString, int> m_symbolIntervalTrades;
    QSet<OrderId> m_orderIds; // to avoid partial orders being counted

    QTimer *m_tradesTimer;

    LoggerPtr m_logger;
    QVector<QTime> m_startAllTimes;
    QVector<QTime> m_stopAllTimes;

    QTimer *m_saveSettingsTimer;
};

#endif // MAINCONTROLLER_H
