#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QObject>
#include <QTime>
#include <QStringList>
#include <QScopedPointer>
#include "CommonDefs.h"
#include "csvparser.h"
#include "Contract.h"
#include "IBClient.h"
#include "ComboContract.h"

#include "log4cxx/logger.h"

using namespace MarketLib;

class Settings : public QObject
{
    Q_OBJECT

public:
    Settings(QString configFileName, IBClient* ib, QString dirPath="", QObject* parent=NULL);
    ~Settings();

    bool requestContractDetails();

    QString getIbIp() const;
    IPs getIbPort() const;
    QTime getStartTimeTradingHours() const;
    QTime getEndTimeTradingHours() const;

    const Contract* getContract1() const;
    const Contract* getContract2() const;

	double getTickSizeContract1() const;
	double getTickSizeContract2() const;

    QString getDebugLevel() const;
    QString getLogDirectory() const;

    bool isCurrentTimeInsideTradingHours() const;

    QString tradingAccount() const;

    int getClientId()const { return m_clientId;}

    QPair<uint, uint> getProtectionPriceDropValues() { return m_protectionPriceDrop;}
    uint getProtectionMaxATExecutionsUnder1min () { return m_maxATExecutionsUnder1min;}

    bool hasError() { return m_hasError;}

    void readInstruments();

signals:
    void contract1DetailsReceived();
    void contract2DetailsReceived();
    void contractsReady();

private slots:
    void onContractDetails(int id, const ContractDetails& details);

private:
    void readSettings();
    void readInstruments(QString filename);
    void readLogSettings();

private:
    QSettings* m_Settings;
    QString m_Ip;
    IPs m_Port;
    QTime m_TradingHoursStart, m_TradingHoursEnd;
    const Contract* m_contract1;
    const Contract* m_contract2;

    QScopedPointer<ComboContract> m_combo1;
    QScopedPointer<ComboContract> m_combo2;
    bool m_combo1Ready = true;
    bool m_combo2Ready = true;

    IBClient* m_IbClient;
    TickerId reqId1, reqId2;
    QPair<uint, uint> m_protectionPriceDrop;
    uint m_maxATExecutionsUnder1min;

    double m_TickSizeContract1;
    double m_TickSizeContract2;

    QStringList m_headers;

    QString m_DebugLevel;
    QString m_LogDirectory;
    QString m_TradingAccount;
    int m_clientId;

    log4cxx::LoggerPtr m_logger;

    bool m_hasError = false;
    QString m_dirPath;
};

#endif // SETTINGS_H
