#ifndef COMBOCONTRACT_H
#define COMBOCONTRACT_H

#include <QObject>
#include <QVector>
#include <memory>
#include "CommonDefs.h"

struct Contract;
struct ComboLeg;
struct ContractDetails;
class IBClient;

class ComboContract: public QObject
{
    Q_OBJECT

public:
    ComboContract()
    {

    }
    ~ComboContract()
    {

    }

    ComboContract(const Contract& contract1, const Contract& contract2, QString exchange, QString currency, IBClient* ib, QObject* parent=nullptr);

    void start();

    double tickSize() { return m_tickSize; }

signals:
    void finished( const Contract& comboContract);

private slots:
    void onContractDetails(int reqId, const ContractDetails& contractDetails);

private:
    IBClient* m_ib;
    Contract* m_contract1;
    Contract* m_contract2;
    QString m_exchange;
    QString m_currency;

    TickerId m_tickerId1, m_tickerId2;
    ComboLeg* m_leg1;
    ComboLeg* m_leg2;

    bool m_received1 = false;
    bool m_received2 = false;

    double m_tickSize = NAN;

    Contract* m_comboContract;
};

#endif // COMBOCONTRACT_H
