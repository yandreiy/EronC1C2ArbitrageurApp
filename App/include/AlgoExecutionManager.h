#ifndef ALGOEXECUTIONMANAGER_H
#define ALGOEXECUTIONMANAGER_H

#include <QObject>
#include <QMap>

/**
 * @brief The AlgoExecutionManager class
 * Allows adding multiple trade executions, and getting the combined total quantity and average price
 */
class AlgoExecutionManager : public QObject
{
    Q_OBJECT
public:
    typedef QPair<uint, double>  QtyPricePair;
    typedef QList<QtyPricePair> QtyPricePairList;

    explicit AlgoExecutionManager(QObject *parent = 0);

    /**
     * @brief addExecution
     * @param symbol - the contract symbol
     * @param qty - the traded quantity
     * @param price - the price at which it was filled
     */
    void addExecution( const QString& symbol, uint qty, double price);

    /**
     * @brief getExecution
     * @param symbol - the contract symbol
     * @param result - the pair of quantity and average price
     * @return
     */
    QtyPricePair getExecution(const QString& symbol );

    /// Clear the data
    void reset();

signals:

public slots:

private:
    QMap< QString, QtyPricePairList> m_map;
};

#endif // ALGOEXECUTIONMANAGER_H
