QT += network

INCLUDEPATH += include

#Interactive Brokers API
#INCLUDEPATH += $${EXTERNALS_PATH}/IB_API_with_Qt/include
#LIBS += -L$${EXTERNALS_PATH}/IB_API_with_Qt/lib -lIB_API_with_Qt_9.64$${SUFFIX}
INCLUDEPATH += ../IB_API_with_Qt/include
LIBS += -L../IB_API_with_Qt/lib -lIB_API_with_Qt_9.64$${SUFFIX}

#log4cxx
INCLUDEPATH += $${EXTERNALS_PATH}/log4cxx-0.10.0/src/main/include

LIBS += -L$${EXTERNALS_PATH}/log4cxx-0.10.0/msvc13/$${BUILD_TYPE}_Static    -llog4cxx -lapr -laprutil -lxml
LIBS += -lAdvapi32 -lodbc32 -lMsWSock -lWS2_32

DEFINES += LOG4CXX_STATIC APR_DECLARE_STATIC APU_DECLARE_STATIC IB_USE_STD_STRING _AFXDLL
QMAKE_CFLAGS += /GS-
QMAKE_CXXFLAGS += /EHsc /wd4355 /wd4800

#MarketLib
INCLUDEPATH += $${EXTERNALS_PATH}/MarketLib/include
LIBS += -L$${EXTERNALS_PATH}/MarketLib/lib	-lMarketLib$${SUFFIX}


# QCustomPlot
#INCLUDEPATH += $${EXTERNALS_PATH}/qcustomplot
#QT += printsupport
#HEADERS += $${EXTERNALS_PATH}/qcustomplot/qcustomplot.h
#SOURCES += $${EXTERNALS_PATH}/qcustomplot/qcustomplot.cpp

