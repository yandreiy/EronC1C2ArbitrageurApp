#ifndef TESTUTILS_H
#define TESTUTILS_H

#include "EWrapper.h"
#include "CommonDefs.h"

struct TickPrice
{
    TickerId id;
    TickType field;
    double price;
};

#endif // TESTUTILS_H

