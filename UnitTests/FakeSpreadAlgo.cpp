#include "FakeSpreadAlgo.h"

FakeSpreadAlgo::FakeSpreadAlgo(IBClient *ib, Settings *settings, const Contract &c1, const Contract &c2, QObject *parent)
    :SpreadAlgo(ib,settings,c1,c2,nullptr,parent)
{

}


void FakeSpreadAlgo::subscribeToMktData()
{

}

void FakeSpreadAlgo::setupIB()
{

}

double FakeSpreadAlgo::getTickSizeC1()
{
    return m_tickSizeC1;
}

double FakeSpreadAlgo::getTickSizeC2()
{
    return m_tickSizeC2;
}

QString FakeSpreadAlgo::getTradingAccount()
{
    return "";
}

OrderId FakeSpreadAlgo::sendLmtOrderToIB(Order order)
{
    return ++orderId;
}

OrderId FakeSpreadAlgo::sendMktOrderToIB(Order order)
{
    return ++orderId;
}

void FakeSpreadAlgo::sendCancelOrderToIB()
{

}
