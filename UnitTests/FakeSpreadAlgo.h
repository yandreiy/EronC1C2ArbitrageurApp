#ifndef FAKESPREADALGO_H
#define FAKESPREADALGO_H

#include "SpreadAlgo.h"

class FakeSpreadAlgo: public SpreadAlgo
{
    Q_OBJECT

public:
    FakeSpreadAlgo( IBClient* ib,
                    Settings* settings,
                    const Contract& c1,
                    const Contract& c2,
                    QObject *parent = 0);

//    bool start( const AlgoParameters& parameters) override;

protected:
    void subscribeToMktData() override;
    void setupIB() override;
    double getTickSizeC1() override;
    double getTickSizeC2() override;
    QString getTradingAccount() override;

    OrderId sendLmtOrderToIB(Order order) override;
    OrderId sendMktOrderToIB(Order order) override;
    void sendCancelOrderToIB() override;

protected:
    OrderId orderId = 0;

    friend class TestSpreadAlgo;
};

#endif // FAKESPREADALGO_H
