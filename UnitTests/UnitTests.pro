EXTERNALS_PATH = ../../../externals

include( build.pri )
include( dependencies.pri )

DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
        FakeSpreadAlgo.h\
        ../App/include/Common.h \
        ../App/include/CircularBuffer.h \
        ../App/include/log_utils.h \
        ../App/include/SpreadAlgo.h \
        ../App/include/SpreadMonitor.h \
        ../App/include/TickData.h \
        ../App/include/AlgoExecutionManager.h \
        ../App/include/Settings.h \
        ../App/include/ComboContract.h \
        TestSpreadAlgo.h \
        testutils.h


SOURCES += \
        FakeSpreadAlgo.cpp\
        tst_circularbuffer.cpp \
        test_main.cpp \
        ../App/src/log_utils.cpp \
        ../App/src/SpreadAlgo.cpp \
        ../App/src/SpreadMonitor.cpp \
        ../App/src/AlgoExecutionManager.cpp \
        ../App/src/ComboContract.cpp \
        ../App/src/Settings.cpp \
        TestSpreadAlgo.cpp
