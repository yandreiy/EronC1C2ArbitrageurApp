win32: DEFINES += _MSVC_VER
DEFINES -= UNICODE
QT += network

QT -= gui

contains( DEFINES, _MSVC_VER ) {
    DEFINES += _CRT_SECURE_NO_WARNINGS #VLD
}

contains(DEFINES, VLD) {
    INCLUDEPATH += $${EXTERNALS_PATH}/VLD/include
    LIBS += -L$${EXTERNALS_PATH}/VLD/lib
}

#Interactive Brokers API
INCLUDEPATH += ../IB_API_with_Qt/include
LIBS += -L../IB_API_with_Qt/lib -lIB_API_with_Qt_9.64$${SUFFIX}

#log4cxx
INCLUDEPATH += $${EXTERNALS_PATH}/log4cxx-0.10.0/src/main/include

LIBS += -L$${EXTERNALS_PATH}/log4cxx-0.10.0/msvc13/$${BUILD_TYPE}_Static    -llog4cxx -lapr -laprutil -lxml
LIBS += -lAdvapi32 -lodbc32 -lMsWSock -lWS2_32

DEFINES += LOG4CXX_STATIC APR_DECLARE_STATIC APU_DECLARE_STATIC IB_USE_STD_STRING _AFXDLL
QMAKE_CFLAGS += /GS-
QMAKE_CXXFLAGS += /EHsc /wd4355 /wd4800

#MarketLib
INCLUDEPATH += $${EXTERNALS_PATH}/MarketLib/include
LIBS += -L$${EXTERNALS_PATH}/MarketLib/lib	-lMarketLib$${SUFFIX}

#gmock-1.7.0
INCLUDEPATH += ../gmock-1.7.0/
LIBS += -L$${EXTERNALS_PATH}/MarketLib/lib	-lMarketLib$${SUFFIX}

############################################################################################################
INCLUDEPATH += ../App/include
INCLUDEPATH += ../App/src
