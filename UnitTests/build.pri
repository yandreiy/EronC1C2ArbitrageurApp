TEMPLATE = app
CONFIG += silent warn_off
QT += testlib

CONFIG(debug, debug|release) {
    SUFFIX = d
    BUILD_TYPE="Debug"
}
else {
    DEFINES += #QT_NO_DEBUG_OUTPUT QT_NO_DEBUG_STREAM
    SUFFIX =
    BUILD_TYPE="Release"
}

#QT       += testlib
QT       -= gui
DEFINES -= UNICODE

TARGET = tst_c1c2unittest$${SUFFIX}
DESTDIR = bin
OBJECTS_DIR = build
UI_DIR = ui
MOC_DIR = moc
UI_HEADERS_DIR = ui
UI_SOURCES_DIR = ui

CONFIG += c++11

#Target version
VERSION_MAJOR = 1
VERSION_MINOR = 0
VERSION_BUILD = 0

APP_VERSION_BUILD = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}
APP_VERSION_MINOR = $${VERSION_MAJOR}.$${VERSION_MINOR}

DEFINES += APP_VERSION=\\\"$$APP_VERSION_BUILD\\\"
