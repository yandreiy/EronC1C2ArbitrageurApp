#ifndef TESTSPREADALGO_H
#define TESTSPREADALGO_H

#include <QtTest/QtTest>
#include <QObject>
#include <QScopedPointer>
#include <QDebug>
#include "FakeSpreadAlgo.h"
#include "testutils.h"

class TestSpreadAlgo : public QObject
{
    Q_OBJECT
public:
    explicit TestSpreadAlgo(QObject *parent = 0);

    void expectLimitOrderPlaced(Order order, QString action, QString type, double price, uint totalQty, uint minQty);
    void expectMktOrder(Order orderMkt, QString action, QString type, uint qty);
signals:

private slots:
    void initTestCase();

    void testIncomingPriceCorrectlySaved();
    void testNormalizedPrice();
    void testCalculateSpread();

    void testSpreadTargetReached();
    void testOrderNotPlacedWhenDeltaBidAskBigger();

    void testOrderFilledAll();

    /// Place Lmt -> new incoming price that makes the spread outside interval
    /// -> cancel order and wait for new prices
    void testCancelOrderBeforeFillAndWaitForNewPrices();

    /// Place Lmt -> new incoming price that makes the spread outside interval, but Lmt is the same as previous
    /// -> no cancel order is sent
    void testCancelOrderWhenSameLimitNotPlaced();


    /// Place Lmt
    /// -> new incoming price that makes the spread inside interval
    /// -> cancel order and place new Lmt order with updated lmt price
    void testCancelOrderBeforeFillAndPlaceNew();

    /// Place Lmt
    /// -> partial fill received
    /// -> Mkt Order is Sent
    /// -> Mkt Order is Filled
    /// -> rest of the partial Fill received
    /// -> Mkt Order is Sent
    /// -> Mkt Order is Filled
    /// -> Finished sent
    void testPartialFillSendsMktOrder();

    /// Place Lmt
    /// -> partial fill received
    /// -> new incoming price that makes the spread OUTSIDE interval
    /// -> cancel order sent
    void testPartialFillNoMktOrder();

    /// Place Lmt
    /// -> new incoming price that makes the spread OUTSIDE interval
    /// -> cancel order sent
    /// -> partial fill received
    /// -> NO Mkt Order is Sent
    /// -> cancel confirmation received
    /// -> goes back to initial state
    void testPartialFillAfterCancelSentNoMktOrder();

    /// Place Lmt
    ///  -> new incoming price that makes the spread INSIDE interval
    /// -> cancel order sent
    /// -> partial fill received
    /// -> place Mkt Order
    /// -> cancel confirmation received
    /// -> new price comes in, makes the spread outside interval
    /// -> execution Mkt received
    /// -> goes back to initial state
    void testPartialFillAfterCancelSendsMktOrder();

    /// TODO
    /// Place Lmt 9
    /// -> Execution received 3
    /// -> Execution received 3
    /// -> Placed Mkt order 1
    /// -> Received execution Mkt
    /// -> Incoming price C2 outside spread
    /// -> Cancel LMT order
    /// -> Cancelled
    /// -> Place Lmt 3
    /// -> Lmt submitted
//    void testPartialFillAfterCancelSendsMktOrder();

    /// Start with qty (20,6)
    /// Place Lmt 10
    /// -> Execution received 10
    /// -> Placed Mkt order 3
    /// -> Execution received 9
    /// -> Placed Mkt order 3
    /// -> Execution received 1
    /// -> Wait for Mkt
    void testStopAfterAllMktReceived();

private:
    QScopedPointer<FakeSpreadAlgo> m_algo;
    void resetAlgo();
    void send(TickPrice tick);
};

#endif // TESTSPREADALGO_H
