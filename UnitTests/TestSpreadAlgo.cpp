#include "TestSpreadAlgo.h"
#include "SpreadMonitor.h"
#include "Order.h"
#include "Execution.h"

#define ADD_WAIT_TIME  10000

TestSpreadAlgo::TestSpreadAlgo(QObject *parent) : QObject(parent)
{
    resetAlgo();
}

void TestSpreadAlgo::resetAlgo()
{
    Contract c1;
    c1.symbol = "C1";
    Contract c2;
    c2.symbol = "C2";
    m_algo.reset(new FakeSpreadAlgo(nullptr,nullptr,c1,c2));

    m_algo->m_tickSizeC1 = 0.01;
    m_algo->m_tickSizeC2 = 0.01;
    m_algo->m_tickerId1 = 1;
    m_algo->m_tickerId2 = 2;
}

void TestSpreadAlgo::send(TickPrice tick)
{
    m_algo->tickPrice(tick.id, tick.field, tick.price,0);
}

void TestSpreadAlgo::initTestCase()
{
    resetAlgo();
}

void TestSpreadAlgo::testIncomingPriceCorrectlySaved()
{
    send( TickPrice{ m_algo->m_tickerId1, BID, 190.5 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 91.5 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90.5 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 191.5 });

    QCOMPARE( m_algo->m_tickC1.bidPrice, 190.5);
    QCOMPARE( m_algo->m_tickC1.askPrice, 191.5);
    QCOMPARE( m_algo->m_tickC2.bidPrice, 90.5);
    QCOMPARE( m_algo->m_tickC2.askPrice, 91.5);

    send( TickPrice{ m_algo->m_tickerId2, ASK, 92.5 });
    QCOMPARE( m_algo->m_tickC2.askPrice, 92.5);
}

void TestSpreadAlgo::testNormalizedPrice()
{
    double price =  25.1249;
    double tick = 0.01;
    double normalized = SpreadAlgo::getNormalizedPrice(0.01, price);
    QVERIFY(qFuzzyCompare(normalized, 25.12));

    price = 25.1151;
    normalized = SpreadAlgo::getNormalizedPrice(0.01, price);
    QVERIFY(qFuzzyCompare(normalized, 25.12));
}

void TestSpreadAlgo::testCalculateSpread()
{
    SpreadFormula formula {3,2};
    QCOMPARE( formula.calculateSpread( 120.00, 128.00), 104.0);
}

void TestSpreadAlgo::testSpreadTargetReached()
{
    AlgoParameters params;
    params.side = AlgoSide::Buy;
    params.minSpread = 90;
    params.delta = 5;
    QCOMPARE( SpreadMonitor::isTargetReached( 96, params), false);
    QCOMPARE( SpreadMonitor::isTargetReached( 95, params), true);

    params.side = AlgoSide::Sell;
    QCOMPARE( SpreadMonitor::isTargetReached( 84, params), false);
    QCOMPARE( SpreadMonitor::isTargetReached( 85, params), true);
}

void TestSpreadAlgo::testOrderNotPlacedWhenDeltaBidAskBigger()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 0.5;
    params.delta = 1;
    params.minSpread = 101;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    /// Order is not placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spy( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spy.wait(100);
    QCOMPARE(spy.count(), 0);
}

void TestSpreadAlgo::expectLimitOrderPlaced(Order order, QString action, QString type, double price, uint totalQty, uint minQty)
{
    QCOMPARE (order.action, action);
    QCOMPARE ( order.lmtPrice, price);
    QCOMPARE(order.orderType, type);
    QCOMPARE((uint)order.totalQuantity, totalQty);
    QCOMPARE((uint)order.minQty, minQty);
}

void TestSpreadAlgo::expectMktOrder(Order orderMkt, QString action, QString type, uint qty)
{
    QCOMPARE (orderMkt.action, action);
    QCOMPARE(orderMkt.orderType, type);
    QCOMPARE((uint)orderMkt.totalQuantity, qty);
}

void TestSpreadAlgo::testOrderFilledAll()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 10;
    params.qty2 = 5;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    /// Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(100);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order order = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(order, "BUY", "LMT", 190.0, params.qty1, 1);

    QSignalSpy spyMktPlaced(m_algo.data(), &SpreadAlgo::marketOrderPlaced);

    // simulate execution received LMT
    Execution executionLmt;
    executionLmt.orderId = order.orderId;
    executionLmt.shares = params.qty1;
    executionLmt.price = order.lmtPrice;
    QTimer::singleShot(5,[&](){ m_algo->execDetails(0,Contract(), executionLmt); });

    spyMktPlaced.wait(10);
    QCOMPARE(spyMktPlaced.count(), 1);
    Order orderMkt = spyMktPlaced.takeFirst().first().value<Order>();
    expectMktOrder(orderMkt, "SELL", "MKT", params.qty2);

    // simulate execution received MKT
    QSignalSpy spyFinished(m_algo.data(), &SpreadAlgo::finished);

    Execution executionMkt;
    executionMkt.orderId = orderMkt.orderId;
    executionMkt.shares = params.qty2;
    executionMkt.price = 190;
    QTimer::singleShot(1,[&](){ m_algo->execDetails(0, Contract(), executionMkt); });

    QTimer::singleShot(5, [&](){
        m_algo->orderStatus(orderMkt.orderId, "Filled", params.qty2, 0, 190, 0, 0, 190, 0, "");
    });

    spyFinished.wait(10);
    QCOMPARE(spyFinished.count(), 1);
    SpreadAlgo::TradeExecutionResult execResult = spyFinished.takeFirst().first().value<SpreadAlgo::TradeExecutionResult>();
    QVERIFY( execResult.hasTraded());
    QCOMPARE( execResult.qty1, params.qty1);
    QCOMPARE( execResult.qty2, params.qty2);
}

void TestSpreadAlgo::testCancelOrderBeforeFillAndWaitForNewPrices()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 10;
    params.qty2 = 5;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    /// Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    // new price comes in before fill. cancel sent!
    QSignalSpy spyCancelOrder(m_algo.data(), &SpreadAlgo::orderCancelSent);
    send( TickPrice{ m_algo->m_tickerId2, BID, 85 }); // spread will be outside target

    spyCancelOrder.wait(10);
    QCOMPARE( spyCancelOrder.count(), 1);
    OrderId orderId = spyCancelOrder.takeFirst().first().value<OrderId>();
    QCOMPARE(orderId, orderLmt.orderId);

    // simulate cancelled order-status
    m_algo->orderStatus(orderLmt.orderId, "Cancelled", 0, 0, 0, 0, 0, 0, 0, "");

    // now should be in intial state
    QVERIFY(!m_algo->hasActiveOrder());
}

void TestSpreadAlgo::testCancelOrderWhenSameLimitNotPlaced()
{
    resetAlgo();
    m_algo->m_tickSizeC1 = 0.05;
    m_algo->m_tickSizeC2 = 0.01;

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,200);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 10;
    params.qty2 = 5;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    /// Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    // new price, with 0.01 difference, generates same limit price. cancel not sent!
    QSignalSpy spyCancelOrder(m_algo.data(), &SpreadAlgo::orderCancelSent);
    send( TickPrice{ m_algo->m_tickerId2, BID, 90.01 }); // spread will be outside target

    spyCancelOrder.wait(10);
    QCOMPARE( spyCancelOrder.count(), 0);

    // now should still be active
    QVERIFY(m_algo->hasActiveOrder());


    {
        // new price should cancel
        QSignalSpy spyCancelOrder(m_algo.data(), &SpreadAlgo::orderCancelSent);
        send( TickPrice{ m_algo->m_tickerId2, BID, 85 }); // spread will be outside target

        spyCancelOrder.wait(10);
        QCOMPARE( spyCancelOrder.count(), 1);

        // simulate cancelled order-status
        m_algo->orderStatus(orderLmt.orderId, "Cancelled", 0, 0, 0, 0, 0, 0, 0, "");

        // now should still be active
        QVERIFY(!m_algo->hasActiveOrder());
    }

}

void TestSpreadAlgo::testCancelOrderBeforeFillAndPlaceNew()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 10;
    params.qty2 = 5;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    /// Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    // new price comes in before fill or submitted status. cancel sent!
    QSignalSpy spyCancelOrder(m_algo.data(), &SpreadAlgo::orderCancelSent);
    send( TickPrice{ m_algo->m_tickerId2, BID, 95 }); // spread inside target

    spyCancelOrder.wait(10);
    QCOMPARE( spyCancelOrder.count(), 1);
    OrderId orderId = spyCancelOrder.takeFirst().first().value<OrderId>();
    QCOMPARE(orderId, orderLmt.orderId);

    // now should be placing new Lmt Order, when Cancel is confirmed
    QSignalSpy spyLmtPlacedNew( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->orderStatus(orderLmt.orderId, "Cancelled", 0, 0, 0, 0, 0, 0, 0, "");

    spyLmtPlacedNew.wait(10);
    QCOMPARE( spyLmtPlacedNew.count(), 1);
    Order orderLmtNew = spyLmtPlacedNew.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmtNew, "BUY", "LMT", 195.0, params.qty1, 1);
}

void TestSpreadAlgo::testPartialFillSendsMktOrder()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 10;
    params.qty2 = 5;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    /// Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    {
        // simulate partial execution received LMT
        Execution executionLmt;
        executionLmt.orderId = orderLmt.orderId;
        executionLmt.shares = 5;
        executionLmt.price = orderLmt.lmtPrice;
        QSignalSpy spyMktPlaced(m_algo.data(), &SpreadAlgo::marketOrderPlaced);
        QTimer::singleShot(5,[&](){ m_algo->execDetails(0,Contract(), executionLmt); });
        spyMktPlaced.wait(10);
        QCOMPARE(spyMktPlaced.count(), 1);
        Order orderMkt = spyMktPlaced.takeFirst().first().value<Order>();
        expectMktOrder(orderMkt, "SELL", "MKT", 3);
        m_algo->orderStatus(orderMkt.orderId, "Filled", 3, 0, 190, 0, 0, 190, 0, "");
    }

    {
        // simulate rest of the partial order
        QSignalSpy spyFinished(m_algo.data(), &SpreadAlgo::finished);

        QTimer::singleShot(5, [&](){
            QSignalSpy spyMktPlaced(m_algo.data(), &SpreadAlgo::marketOrderPlaced);

            Execution executionLmt;
            executionLmt.orderId = orderLmt.orderId;
            executionLmt.shares = 5;
            executionLmt.price = orderLmt.lmtPrice;

            QTimer::singleShot(5,[&](){ m_algo->execDetails(0,Contract(), executionLmt); });
            spyMktPlaced.wait(10);
            QCOMPARE(spyMktPlaced.count(), 1);
            Order orderMkt = spyMktPlaced.takeFirst().first().value<Order>();
            expectMktOrder(orderMkt, "SELL", "MKT", 2);

            m_algo->orderStatus(orderMkt.orderId, "Filled", 2, 0, 190, 0, 0, 190, 0, "");
        });

        spyFinished.wait(100 );
        QCOMPARE(spyFinished.count(), 1);
    }

}

void TestSpreadAlgo::testPartialFillNoMktOrder()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 9;
    params.qty2 = 1;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    /// Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    {
        // simulate partial execution received LMT
        Execution executionLmt;
        executionLmt.orderId = orderLmt.orderId;
        executionLmt.shares = 1;
        executionLmt.price = orderLmt.lmtPrice;
        m_algo->execDetails(0,Contract(), executionLmt);

        // new price comes, expect cancel
        QSignalSpy spyCancel(m_algo.data(), &SpreadAlgo::orderCancelSent);
        QTimer::singleShot(5, [&](){
            send( TickPrice{ m_algo->m_tickerId2, BID, 85 }); // spread outside target
        });

        spyCancel.wait(10);
        QCOMPARE(spyCancel.count(), 1);
    }

}

void TestSpreadAlgo::testPartialFillAfterCancelSentNoMktOrder()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 9;
    params.qty2 = 1;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    // Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    // new price comes in before fill or submitted status. cancel sent!
    QSignalSpy spyCancelOrder(m_algo.data(), &SpreadAlgo::orderCancelSent);
    send( TickPrice{ m_algo->m_tickerId2, BID, 85 }); // spread outside target
    spyCancelOrder.wait(10);
    QCOMPARE( spyCancelOrder.count(), 1);
    OrderId orderId = spyCancelOrder.takeFirst().first().value<OrderId>();
    QCOMPARE(orderId, orderLmt.orderId);

    // partial fill received
    Execution executionLmt;
    executionLmt.orderId = orderLmt.orderId;
    executionLmt.shares = 1;
    executionLmt.price = orderLmt.lmtPrice;
    m_algo->execDetails(0,Contract(), executionLmt);

    // cancel confirmation received
    m_algo->orderStatus(orderLmt.orderId, "Cancelled", 0, 0, 0, 0, 0, 0, 0, "");
    QVERIFY (!m_algo->hasActiveOrder());
}

void TestSpreadAlgo::testPartialFillAfterCancelSendsMktOrder()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 9;
    params.qty2 = 1;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    // Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    // new price comes in before fill or submitted status. cancel sent!
    QSignalSpy spyCancelOrder(m_algo.data(), &SpreadAlgo::orderCancelSent);
    send( TickPrice{ m_algo->m_tickerId2, BID, 95 }); // spread inside target
    spyCancelOrder.wait(10);
    QCOMPARE( spyCancelOrder.count(), 1);
    OrderId orderId = spyCancelOrder.takeFirst().first().value<OrderId>();
    QCOMPARE(orderId, orderLmt.orderId);

    // partial fill received, places mkt order
    Execution executionLmt;
    executionLmt.orderId = orderLmt.orderId;
    executionLmt.shares = 5;
    executionLmt.price = orderLmt.lmtPrice;

    QSignalSpy spyMktPlaced(m_algo.data(), &SpreadAlgo::marketOrderPlaced);
    QTimer::singleShot(5,[&](){
       m_algo->execDetails(0,Contract(), executionLmt);
    });
    spyMktPlaced.wait(10);
    Order orderMkt = spyMktPlaced.takeFirst().first().value<Order>();
    expectMktOrder(orderMkt, "SELL", "MKT", params.qty2);

    // cancel confirmation received
    m_algo->orderStatus(orderLmt.orderId, "Cancelled", 0, 0, 0, 0, 0, 0, 0, "");

    // price comes in outside interval
    send( TickPrice{ m_algo->m_tickerId2, BID, 85 }); // spread inside target

    // simulate execution received MKT
    Execution executionMkt;
    executionMkt.orderId = orderMkt.orderId;
    executionMkt.shares = params.qty2;
    executionMkt.price = 190;
    m_algo->execDetails(0, Contract(), executionMkt);
    m_algo->orderStatus(orderMkt.orderId, "Filled", params.qty2, 0, 190, 0, 0, 190, 0, "");

    QVERIFY(!m_algo->hasActiveOrder());
}

/// Start with qty (20,3)
/// Place Lmt 10
/// -> Execution received 10
/// -> Placed Mkt order 2
/// -> Execution received 9
/// -> Placed Mkt order 1
/// -> Execution received 1
/// -> Wait for Mkt
/// -> Stop
void TestSpreadAlgo::testStopAfterAllMktReceived()
{
    resetAlgo();

    AlgoParameters params;
    params.minDeltaBidAsk = 11;
    params.delta = 1;
    params.minSpread = 100;
    params.side = AlgoSide::Buy;
    params.protectionPriceDrop = QPair<uint,uint>(3,35);
    params.spreadFormula = SpreadFormula {1.0, 1.0};
    params.qty1 = 20;
    params.qty2 = 3;

    send( TickPrice{ m_algo->m_tickerId1, BID, 190 });
    send( TickPrice{ m_algo->m_tickerId1, ASK, 200 });
    send( TickPrice{ m_algo->m_tickerId2, BID, 90 });
    send( TickPrice{ m_algo->m_tickerId2, ASK, 100 });

    // Order is placed, the spread (=100) is less then minSpread(=101)
    QSignalSpy spyLmtPlaced( m_algo.data(), &SpreadAlgo::limitOrderPlaced);
    m_algo->start(params);

    spyLmtPlaced.wait(10);
    QCOMPARE(spyLmtPlaced.count(), 1);
    Order orderLmt = spyLmtPlaced.takeFirst().first().value<Order>();
    expectLimitOrderPlaced(orderLmt, "BUY", "LMT", 190.0, params.qty1, 1);

    Order orderMkt1;
    {
        // partial fill received, places mkt order
        Execution executionLmt;
        executionLmt.orderId = orderLmt.orderId;
        executionLmt.shares = 10;
        executionLmt.price = orderLmt.lmtPrice;

        QSignalSpy spyMktPlaced(m_algo.data(), &SpreadAlgo::marketOrderPlaced);
        QTimer::singleShot(5,[&](){
            m_algo->execDetails(0,Contract(), executionLmt);
        });
        spyMktPlaced.wait(10 + ADD_WAIT_TIME);
        QCOMPARE (spyMktPlaced.count(), 1);
        orderMkt1 = spyMktPlaced.takeFirst().first().value<Order>();
        expectMktOrder(orderMkt1, "SELL", "MKT", 2);
    }

    Order orderMkt2;
    {
        // partial fill received, places mkt order

        Execution executionLmt;
        executionLmt.orderId = orderLmt.orderId;
        executionLmt.shares = 9;
        executionLmt.price = orderLmt.lmtPrice;

        QSignalSpy spyMktPlaced(m_algo.data(), &SpreadAlgo::marketOrderPlaced);
        QTimer::singleShot(5,[&](){
            m_algo->execDetails(0,Contract(), executionLmt);
        });
        spyMktPlaced.wait(10 + ADD_WAIT_TIME);
        QCOMPARE (spyMktPlaced.count(), 1);
        orderMkt2 = spyMktPlaced.takeFirst().first().value<Order>();
        expectMktOrder(orderMkt2, "SELL", "MKT", 1);
    }

    {
        // partial fill received, No Mkt left

        Execution executionLmt;
        executionLmt.orderId = orderLmt.orderId;
        executionLmt.shares = 1;
        executionLmt.price = orderLmt.lmtPrice;

        QSignalSpy spyMktPlaced(m_algo.data(), &SpreadAlgo::marketOrderPlaced);
        QTimer::singleShot(5,[&](){
            m_algo->execDetails(0,Contract(), executionLmt);
        });
        spyMktPlaced.wait(10);
        QCOMPARE (spyMktPlaced.count(), 0);
    }

    {
        // simulate execution received MKT
        Execution executionMkt;
        executionMkt.orderId = orderMkt1.orderId;
        executionMkt.shares = 2;
        executionMkt.price = 190;
        m_algo->execDetails(0, Contract(), executionMkt);
        m_algo->orderStatus(orderMkt1.orderId, "Filled", params.qty2, 0, 190, 0, 0, 190, 0, "");
    }

    QSignalSpy spyFinished(m_algo.data(), &SpreadAlgo::finished);

    {
        // simulate execution received MKT
        Execution executionMkt;
        executionMkt.orderId = orderMkt2.orderId;
        executionMkt.shares = 1;
        executionMkt.price = 190;
        m_algo->execDetails(0, Contract(), executionMkt);
        m_algo->orderStatus(orderMkt2.orderId, "Filled", params.qty2, 0, 190, 0, 0, 190, 0, "");
    }

    spyFinished.wait(10);
    QCOMPARE(spyFinished.count(), 1);

    QVERIFY(!m_algo->hasActiveOrder());
}
